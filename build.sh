#!/bin/sh

#export CC=clang
#export CXX=clang++

if [ "$1" = "debug" ]; then
	cd build/debug
	if [ "$2" = "clean" ]; then
		cmake -DCMAKE_BUILD_TYPE=Debug ../..
	fi
	make
fi

if [ "$1" = "release" ]; then
	cd build/release
	if [ "$2" = "clean" ]; then
		cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -DCMAKE_BUILD_TYPE=Release ../..
	fi
	make
fi
