
#include "IntegratorUdpt.hpp"

namespace NIntegratorUdpt
{
	void Init(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto& path : ictx_ptr->path_buffer)
		{
			path.radiance = { 0.0f, 0.0f, 0.0f, 1.0f };
			path.throughput = { 1.0f, 1.0f, 1.0f, 1.0f };
		}

		for (auto& sample : ictx_ptr->sample_info_buffer)
		{
			sample.throughput = { 1.0f, 1.0f, 1.0f, 1.0f };
		}

		for (auto& hit_info : ictx_ptr->hit_info_buffer)
		{
			hit_info.intersection_type = INTERSECTION_TYPE_BACKGROUND;
		}
	}

	void RefreshSampleInfoBuffer(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto& sample : ictx_ptr->sample_info_buffer)
		{
			sample.throughput = { 1.0f, 1.0f, 1.0f, 1.0f };
			sample.pdf = 1.0f;
		}
	}

	void PrepareRays(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->active_path_stream)
		{
			SRay& ray = ictx_ptr->ray_buffer[i];
			SIntersectionData& hit_info = ictx_ptr->hit_info_buffer[i];
			
			SShaderGlobals& sg = ictx_ptr->sg_buffer[i];
			SSampleInfo& sample_info = ictx_ptr->sample_info_buffer[i];

			ray.origin = sg.pos;
			ray.direction = sample_info.direction;
			ray.t_min = 0.0f;
			ray.t_max = std::numeric_limits<F32>::max();

			hit_info.intersection_type = EIntersectionType::INTERSECTION_TYPE_BACKGROUND;
		}
	}

	void FilterCameraRays(SIntegratorUdptContext* ictx_ptr)
	{
		std::vector<U32> temp_active_path_stream;

		for (auto i : ictx_ptr->active_path_stream)
		{
			EIntersectionType occluder_type = ictx_ptr->hit_info_buffer[i].intersection_type;

			if (occluder_type == EIntersectionType::INTERSECTION_TYPE_BACKGROUND)
			{
				ictx_ptr->background_eval_stream.push_back(i);
			}
			else if (occluder_type == EIntersectionType::INTERSECTION_TYPE_LIGHT_SURFACE)
			{
				ictx_ptr->light_eval_stream.push_back(i);
			}
			else if (occluder_type == EIntersectionType::INTERSECTION_TYPE_MESH_SURFACE)
			{
				temp_active_path_stream.push_back(i);
			}
		}

		ictx_ptr->active_path_stream = temp_active_path_stream;
	}

	void FilterShadowRays(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->active_path_stream)
		{
			EIntersectionType occluder_type = ictx_ptr->hit_info_buffer[i].intersection_type;

			if (occluder_type == EIntersectionType::INTERSECTION_TYPE_LIGHT_SURFACE)
			{
				ictx_ptr->light_eval_stream.push_back(i);
			}
		}
	}

	void FilterIndirectRays(SIntegratorUdptContext* ictx_ptr)
	{
		std::vector<U32> temp_active_path_stream;

		for (auto i : ictx_ptr->active_path_stream)
		{
			EIntersectionType occluder_type = ictx_ptr->hit_info_buffer[i].intersection_type;

			switch (occluder_type)
			{
				case INTERSECTION_TYPE_LIGHT_SURFACE:
				{
					ictx_ptr->light_eval_stream.push_back(i);
					break;
				}
				case INTERSECTION_TYPE_MESH_SURFACE:
				{
					temp_active_path_stream.push_back(i);
					break;
				}
			}
		}

		ictx_ptr->active_path_stream = temp_active_path_stream;
	}

	void EvalBackground(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->background_eval_stream)
		{
			SPath& path = ictx_ptr->path_buffer[i];
			path.radiance = { 0.0f, 0.0f, 0.0f, 1.0f };
		}
	}

	void ExtendPaths(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->active_path_stream)
		{
			SRay ray = ictx_ptr->ray_buffer[i];
			SIntersectionData hit_info = ictx_ptr->hit_info_buffer[i];
			SShaderGlobals& sg = ictx_ptr->sg_buffer[i];

			sg.pos = hit_info.pos;
			sg.normal = hit_info.normal;
			sg.texcoord = hit_info.texcoord;
			sg.wo = -1.0f * ray.direction;

			SMatrix4x4 tangent_to_world_mat;
			tangent_to_world_mat.r0 = { hit_info.tangent.x, hit_info.tangent.y, hit_info.tangent.z, 0.0f };
			tangent_to_world_mat.r1 = { hit_info.binormal.x, hit_info.binormal.y, hit_info.binormal.z, 0.0f };
			tangent_to_world_mat.r2 = { hit_info.normal.x, hit_info.normal.y, hit_info.normal.z, 0.0f };
			tangent_to_world_mat.r3 = { 0.0f, 0.0f, 0.0f, 1.0f };
			sg.tangent_to_world_mtx = NMath::Transpose(tangent_to_world_mat);
			sg.mesh_id = hit_info.occluder_id;
			
			/* TODO: */
			sg.material_id = 0;
		}
	}

	void RussianRouletteTerminatePaths(SIntegratorUdptContext* ictx_ptr)
	{
		std::vector<U32> temp_active_path_stream;

		for (auto i : ictx_ptr->active_path_stream)
		{
			F32x4 path_throughput = ictx_ptr->path_buffer[i].throughput;
			F32 cont_prob = fmin(0.5f, Luminance(path_throughput));

			if (ictx_ptr->sampler_ptr->SampleF32() < cont_prob)
			{
				temp_active_path_stream.push_back(i);
				path_throughput = path_throughput / cont_prob;
				ictx_ptr->path_buffer[i].throughput = path_throughput;
			}
		}

		ictx_ptr->active_path_stream = temp_active_path_stream;
	}

	void UpdatePathThroughputs(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->active_path_stream)
		{
			SPath& path = ictx_ptr->path_buffer[i];
			SSampleInfo sample_info = ictx_ptr->sample_info_buffer[i];
			path.throughput = path.throughput * sample_info.throughput;
		}
	}

	void RefreshPathThroughputs(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->active_path_stream)
		{
			SPath& path = ictx_ptr->path_buffer[i];
			path.throughput = { 1.0f, 1.0f, 1.0f, 1.0f };
		}
	}

	void AccumulateRadiance(SIntegratorUdptContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->light_eval_stream)
		{
			SPath& path = ictx_ptr->path_buffer[i];
			SSampleInfo& sample_info = ictx_ptr->sample_info_buffer[i];
			path.radiance = path.radiance + path.throughput * sample_info.throughput;
		}
	}

	void AccumulateRadianceN(SIntegratorUdptContext* ictx_ptr, U32 n)
	{
		for (auto i : ictx_ptr->light_eval_stream)
		{
			SPath& path = ictx_ptr->path_buffer[i];
			SSampleInfo& sample_info = ictx_ptr->sample_info_buffer[i];
			path.radiance = path.radiance + path.throughput * sample_info.throughput * (1.0f / static_cast<F32>(n));
		}
	}

	void DirectIllumPassN(SIntegratorUdptContext* ictx_ptr, SSceneContext* sctx_ptr)
	{
		for (U32 i = 0; i < ictx_ptr->integrator_settings.di_sample_count; i++)
		{
			RefreshSampleInfoBuffer(ictx_ptr);
			/* Sample light rays */
			sctx_ptr->light_manager_ptr->Sample(ictx_ptr->active_path_stream.data(), ictx_ptr->active_path_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());

			/* Evaluate shader for direct light rays */
			sctx_ptr->shader_manager_ptr->Eval(ictx_ptr->active_path_stream, ictx_ptr->sg_buffer, ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer);

			PrepareRays(ictx_ptr);

			/* Intersect direct light rays */
			{
				SIntersectorQueryData query_data;
				query_data.ray_buffer = &ictx_ptr->ray_buffer;
				query_data.ray_stream = &ictx_ptr->active_path_stream;
				query_data.hit_info_buffer = &ictx_ptr->hit_info_buffer;
				ictx_ptr->intersector.QueryIntersection(sctx_ptr, query_data);
			}

			/* Filter direct light paths */
			FilterShadowRays(ictx_ptr);
			sctx_ptr->light_manager_ptr->Eval(ictx_ptr->light_eval_stream.data(), ictx_ptr->light_eval_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->hit_info_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());
			/* Accumulate path radiance */
			AccumulateRadianceN(ictx_ptr, ictx_ptr->integrator_settings.di_sample_count);
			ictx_ptr->light_eval_stream.clear();
		}
	}

	void DirectIllumPass(SIntegratorUdptContext* ictx_ptr, SSceneContext* sctx_ptr)
	{
		RefreshSampleInfoBuffer(ictx_ptr);
		/* Sample light rays */
		sctx_ptr->light_manager_ptr->Sample(ictx_ptr->active_path_stream.data(), ictx_ptr->active_path_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());

		/* Evaluate shader for direct light rays */
		sctx_ptr->shader_manager_ptr->Eval(ictx_ptr->active_path_stream, ictx_ptr->sg_buffer, ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer);

		PrepareRays(ictx_ptr);

		/* Intersect direct light rays */
		{
			SIntersectorQueryData query_data;
			query_data.ray_buffer = &ictx_ptr->ray_buffer;
			query_data.ray_stream = &ictx_ptr->active_path_stream;
			query_data.hit_info_buffer = &ictx_ptr->hit_info_buffer;
			ictx_ptr->intersector.QueryIntersection(sctx_ptr, query_data);
		}

		/* Filter direct light paths */
		FilterShadowRays(ictx_ptr);
		sctx_ptr->light_manager_ptr->Eval(ictx_ptr->light_eval_stream.data(), ictx_ptr->light_eval_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->hit_info_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());
		/* Accumulate path radiance */
		AccumulateRadiance(ictx_ptr);
		ictx_ptr->light_eval_stream.clear();

		/* TODO: */
		if (ictx_ptr->integrator_settings.enable_mis)
		{
			/* Sample and evaluate shader for direct bsdf rays */
			sctx_ptr->shader_manager_ptr->Sample(ictx_ptr->active_path_stream, ictx_ptr->sg_buffer, ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer);

			PrepareRays(ictx_ptr);

			/* Intersect direct bsdf rays */
			{
				SIntersectorQueryData query_data;
				query_data.ray_buffer = &ictx_ptr->ray_buffer;
				query_data.ray_stream = &ictx_ptr->active_path_stream;
				query_data.hit_info_buffer = &ictx_ptr->hit_info_buffer;
				ictx_ptr->intersector.QueryIntersection(sctx_ptr, query_data);
			}

			/* Filter direct bsdf rays */
			FilterShadowRays(ictx_ptr);
			sctx_ptr->light_manager_ptr->Eval(ictx_ptr->light_eval_stream.data(), ictx_ptr->light_eval_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->hit_info_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());
			ictx_ptr->light_eval_stream.clear();

			ictx_ptr->ray_buffer.clear();
			ictx_ptr->sample_info_buffer.clear();
			ictx_ptr->hit_info_buffer.clear();
		}
	}

	void IndirectIllumPassN(SIntegratorUdptContext* ictx_ptr, SSceneContext* sctx_ptr)
	{
		std::vector<U32> temp_active_path_stream = ictx_ptr->active_path_stream;
		std::vector<SShaderGlobals> temp_sg_buffer = ictx_ptr->sg_buffer;

		for (U32 i = 0; i < ictx_ptr->integrator_settings.ii_sample_count; i++)
		{
			U32 bounce_count = 0;
			while (ictx_ptr->active_path_stream.size() != 0 && bounce_count < ictx_ptr->integrator_settings.max_indirect_bounce_count)
			{
				/* Sample and evaluate shader for extension bsdf rays */
				RefreshSampleInfoBuffer(ictx_ptr);
				sctx_ptr->shader_manager_ptr->Sample(ictx_ptr->active_path_stream, ictx_ptr->sg_buffer, ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer);

				PrepareRays(ictx_ptr);

				/* Intersect extension bsdf rays*/
				{
					SIntersectorQueryData query_data;
					query_data.ray_buffer = &ictx_ptr->ray_buffer;
					query_data.ray_stream = &ictx_ptr->active_path_stream;
					query_data.hit_info_buffer = &ictx_ptr->hit_info_buffer;
					ictx_ptr->intersector.QueryIntersection(sctx_ptr, query_data);
				}

				/* Filter indirect illum paths */
				FilterIndirectRays(ictx_ptr);

				/* If bsdf ray hits a light source, evaluate light and terminate path */
				sctx_ptr->light_manager_ptr->Eval(ictx_ptr->light_eval_stream.data(), ictx_ptr->light_eval_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->hit_info_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());
				AccumulateRadianceN(ictx_ptr, ictx_ptr->integrator_settings.ii_sample_count);
				ictx_ptr->light_eval_stream.clear();

				/* If not, then continue extending the path  */
				ExtendPaths(ictx_ptr);
				UpdatePathThroughputs(ictx_ptr);

				/* Compute direct illumination */
				RefreshSampleInfoBuffer(ictx_ptr);
				/* Sample light rays */
				sctx_ptr->light_manager_ptr->Sample(ictx_ptr->active_path_stream.data(), ictx_ptr->active_path_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());

				/* Evaluate shader for direct light rays */
				sctx_ptr->shader_manager_ptr->Eval(ictx_ptr->active_path_stream, ictx_ptr->sg_buffer, ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer);

				PrepareRays(ictx_ptr);

				/* Intersect direct light rays */
				{
					SIntersectorQueryData query_data;
					query_data.ray_buffer = &ictx_ptr->ray_buffer;
					query_data.ray_stream = &ictx_ptr->active_path_stream;
					query_data.hit_info_buffer = &ictx_ptr->hit_info_buffer;
					ictx_ptr->intersector.QueryIntersection(sctx_ptr, query_data);
				}

				/* Filter direct light paths */
				FilterShadowRays(ictx_ptr);
				sctx_ptr->light_manager_ptr->Eval(ictx_ptr->light_eval_stream.data(), ictx_ptr->light_eval_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->hit_info_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());
				/* Accumulate path radiance */
				AccumulateRadianceN(ictx_ptr, ictx_ptr->integrator_settings.ii_sample_count);
				ictx_ptr->light_eval_stream.clear();
				
				bounce_count++;
			}

			ictx_ptr->active_path_stream = temp_active_path_stream;
			ictx_ptr->sg_buffer = temp_sg_buffer;
			RefreshPathThroughputs(ictx_ptr);
		}
	}

	void IndirectIllumPass(SIntegratorUdptContext* ictx_ptr, SSceneContext* sctx_ptr)
	{
		if (ictx_ptr->integrator_settings.enable_rr)
		{
			/* Randomly terminate paths using russian roulette sampling */
			RussianRouletteTerminatePaths(ictx_ptr);
		}

		/* Sample and evaluate shader for extension bsdf rays */
		RefreshSampleInfoBuffer(ictx_ptr);
		sctx_ptr->shader_manager_ptr->Sample(ictx_ptr->active_path_stream, ictx_ptr->sg_buffer, ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer);

		PrepareRays(ictx_ptr);

		/* Intersect extension bsdf rays*/
		{
			SIntersectorQueryData query_data;
			query_data.ray_buffer = &ictx_ptr->ray_buffer;
			query_data.ray_stream = &ictx_ptr->active_path_stream;
			query_data.hit_info_buffer = &ictx_ptr->hit_info_buffer;
			ictx_ptr->intersector.QueryIntersection(sctx_ptr, query_data);
		}

		/* Filter indirect illum paths */
		FilterIndirectRays(ictx_ptr);

		/* If bsdf ray hits a light source, evaluate light and terminate path */
		sctx_ptr->light_manager_ptr->Eval(ictx_ptr->light_eval_stream.data(), ictx_ptr->light_eval_stream.size(), ictx_ptr->sg_buffer.data(), ictx_ptr->hit_info_buffer.data(), ictx_ptr->sampler_ptr, ictx_ptr->sample_info_buffer.data());
		AccumulateRadiance(ictx_ptr);
		ictx_ptr->light_eval_stream.clear();

		/* If not, then continue extending the path  */
		ExtendPaths(ictx_ptr);
		UpdatePathThroughputs(ictx_ptr);
	}
};

void CIntegratorUdpt::Execute(SWorkQueue* work_queue_ptr, SSceneContext* scene_ctx_ptr, CFramebuffer* framebuffer_ptr)
{
	STileInfo tile_info;
	U32 tile_id;

	while (!work_queue_ptr->DequeueId(tile_id, tile_info))
	{
		U32 pixel_count = tile_info.width * tile_info.height;
		/*
		m_ctx.ray_buffer.resize(pixel_count);
		m_ctx.hit_info_buffer.resize(pixel_count);
		m_ctx.sg_buffer.resize(pixel_count);
		m_ctx.sample_info_buffer.resize(pixel_count);
		m_ctx.path_buffer.resize(pixel_count);
		*/
		m_ctx.contrib_buffer.resize(pixel_count);
		m_ctx.aux_buffer.resize(pixel_count);

		for (U32 px = 0; px < pixel_count; px++)
		{
			m_ctx.contrib_buffer[px] = { 0.0f, 0.0f, 0.0f, 1.0f };
			m_ctx.aux_buffer[px] = { 0.0f, 0.0f, 0.0f, 1.0f };
		}

		F32 error = std::numeric_limits<F32>::max();

		for (U32 i = 0; i <  m_ctx.integrator_settings.iteration_count; i++)
		{
			if (m_ctx.integrator_settings.enable_as && error < m_ctx.integrator_settings.as_termination_threshold)
			{
				break;
			}

			error = 0;

			m_ctx.sampler_ptr->InitSequence(pixel_count, i);

			NIntegratorUdpt::Init(&m_ctx);
			m_ctx.active_path_stream.clear();

			for (U32 k = 0; k < tile_info.width * tile_info.height; k++)
			{
				m_ctx.active_path_stream.push_back(k);
			}

			/* Sample camera rays */
			scene_ctx_ptr->camera_manager_ptr->GenerateRays(m_ctx.sampler_ptr, m_ctx.render_settings, tile_info, m_ctx.ray_buffer);

			/* Intersect camera rays */
			SIntersectorQueryData query_data;
			query_data.ray_buffer = &m_ctx.ray_buffer;
			query_data.ray_stream = &m_ctx.active_path_stream;
			query_data.hit_info_buffer = &m_ctx.hit_info_buffer;
			m_ctx.intersector.QueryIntersection(scene_ctx_ptr, query_data);

			NIntegratorUdpt::FilterCameraRays(&m_ctx);
			NIntegratorUdpt::EvalBackground(&m_ctx);
			m_ctx.background_eval_stream.clear();

			scene_ctx_ptr->light_manager_ptr->Eval(m_ctx.light_eval_stream.data(), m_ctx.light_eval_stream.size(), m_ctx.sg_buffer.data(), m_ctx.hit_info_buffer.data(), m_ctx.sampler_ptr, m_ctx.sample_info_buffer.data());
			NIntegratorUdpt::AccumulateRadiance(&m_ctx);
			m_ctx.light_eval_stream.clear();

			NIntegratorUdpt::ExtendPaths(&m_ctx);
			NIntegratorUdpt::DirectIllumPassN(&m_ctx, scene_ctx_ptr);
			NIntegratorUdpt::IndirectIllumPassN(&m_ctx, scene_ctx_ptr);

			framebuffer_ptr->LockW();
			for (U32 y = 0; y < tile_info.height; y++)
			for (U32 x = 0; x < tile_info.width; x++)
			{
				U32 px = y * tile_info.width + x;

				F32x4 radiance = m_ctx.path_buffer[px].radiance;
				radiance.x = NMath::Clamp(powf(radiance.x, 1.0f / 2.2f), 0.0f, 1.0f);
				radiance.y = NMath::Clamp(powf(radiance.y, 1.0f / 2.2f), 0.0f, 1.0f);
				radiance.z = NMath::Clamp(powf(radiance.z, 1.0f / 2.2f), 0.0f, 1.0f);
				m_ctx.contrib_buffer[px] = m_ctx.contrib_buffer[px] + radiance;

				framebuffer_ptr->SetColorBufferPx(x + tile_info.offset_x, y + tile_info.offset_y, m_ctx.contrib_buffer[px] / F32(i + 1));
			}
			framebuffer_ptr->UnlockW();

			if (i % 2 != 0)
			{
				for (U32 y = 0; y < tile_info.height; y++)
				for (U32 x = 0; x < tile_info.width; x++)
				{
					U32 px = y * tile_info.width + x;

					F32x4 radiance = m_ctx.path_buffer[px].radiance;
					radiance.x = NMath::Clamp(powf(radiance.x, 1.0f / 2.2f), 0.0f, 1.0f);
					radiance.y = NMath::Clamp(powf(radiance.y, 1.0f / 2.2f), 0.0f, 1.0f);
					radiance.z = NMath::Clamp(powf(radiance.z, 1.0f / 2.2f), 0.0f, 1.0f);
					m_ctx.aux_buffer[px] = m_ctx.aux_buffer[px] + radiance;
				}
			}

			for (U32 y = 0; y < tile_info.height; y++)
			for (U32 x = 0; x < tile_info.width; x++)
			{
				U32 px = y * tile_info.width + x;
				F32x4 c0 = m_ctx.contrib_buffer[px] / static_cast<F32>(i + 1);
				F32x4 c1 = m_ctx.aux_buffer[px] / static_cast<F32>(((i - 1) / 2) + 1);

				error += (abs(c0.x - c1.y) + abs(c0.y - c1.y) + abs(c0.z - c1.z)) / (sqrt(c0.x + c0.y + c0.z) + 1e-3f);
			}

			error /= tile_info.width * tile_info.height;
			error *= 2.0f;
		}

		/* Accumulate radiance to framebuffer */
		/*
		framebuffer_ptr->LockW();
		for (U32 y = 0; y < tile_info.height; y++)
		for (U32 x = 0; x < tile_info.width; x++)
		{
			U32 result_idx = y * tile_info.width + x;
			framebuffer_ptr->SetColorBufferPx(x + tile_info.offset_x, y + tile_info.offset_y, m_ctx.contrib_buffer[result_idx] / F32(m_ctx.integrator_settings.iteration_count));
		}
		framebuffer_ptr->UnlockW();
		*/
		m_ctx.contrib_buffer.clear();
		m_ctx.aux_buffer.clear();

		work_queue_ptr->SetTileStateFinished(tile_id);
	}
}

IIntegrator* CIntegratorUdptFactory::Create(SRenderSettings* render_settings, void* integrator_settings, CSceneIntersector& intersector) const
{
	return new CIntegratorUdpt(*render_settings, *static_cast<SIntegratorUdptSettings*>(integrator_settings), intersector);
}
