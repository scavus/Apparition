
#include <iostream>
#include <chrono>
#include <limits>

#include "IntegratorAo.hpp"

#include "IntegratorAoKernel_ispc.h"


struct SIntegratorContext_K
{
	SRay* ray_buffer;
	SIntersectionData* hit_info_buffer;
	SShaderGlobals* sg_buffer;
	F32x4* ao_buffer;

	U32* active_path_stream;
	U32 active_path_stream_length;

	U32* eval_background_stream;
	U32 eval_background_stream_length;
};

/*
extern void ComputeAo_K(struct SIntegratorContext_K &ictx);
extern void EvalBackground_K(struct SIntegratorContext_K &ictx);
extern void ExtendRays_K(struct SIntegratorContext_K &ictx);
*/

/*
extern void ExtendRays_K();
extern void EvalBackground_K();
extern void SampleAoRays_K();
extern void ComputeAo_K();
*/



namespace NIntegratorAo
{
	void FilterCameraRays(SIntegratorAoContext* ictx_ptr)
	{
		std::vector<U32> temp_active_path_stream;

		for (auto path_id : ictx_ptr->active_path_stream)
		{
			EIntersectionType occluder_type = ictx_ptr->hit_info_buffer[path_id].intersection_type;

			switch (occluder_type)
			{
				case INTERSECTION_TYPE_BACKGROUND:
				{
					ictx_ptr->eval_background_stream.push_back(path_id);
					break;
				}
				case INTERSECTION_TYPE_MESH_SURFACE:
				{
					temp_active_path_stream.push_back(path_id);
					break;
				}
			}
		}

		ictx_ptr->active_path_stream = temp_active_path_stream;
	}

	void ExtendRays_V(SIntegratorAoContext* ictx_ptr)
	{
		ispc::SIntegratorContext_K ictx_k;
		ictx_k.active_path_stream = &ictx_ptr->active_path_stream[0];
		ictx_k.active_path_stream_length = ictx_ptr->active_path_stream.size();
		ictx_k.ao_buffer = &ictx_ptr->ao_buffer[0];
		ictx_k.eval_background_stream = &ictx_ptr->eval_background_stream[0];
		ictx_k.eval_background_stream_length = ictx_ptr->eval_background_stream.size();
		ictx_k.hit_info_buffer = &ictx_ptr->hit_info_buffer[0];
		ictx_k.ray_buffer = &ictx_ptr->ray_buffer[0];
		ictx_k.sg_buffer = &ictx_ptr->sg_buffer[0];
		ispc::ExtendRays_K(ictx_k);
	}

	void ExtendRays(SIntegratorAoContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->active_path_stream)
		{
			SRay ray = ictx_ptr->ray_buffer[i];
			SIntersectionData hit_info = ictx_ptr->hit_info_buffer[i];
			SShaderGlobals* sg_ptr = &ictx_ptr->sg_buffer[i];

			sg_ptr->pos = hit_info.pos;
			sg_ptr->normal = hit_info.normal;
			sg_ptr->wo = -1.0f * ray.direction;
			
			SMatrix4x4 tangent_to_world_mat;
			tangent_to_world_mat.r0 = { hit_info.tangent.x, hit_info.tangent.y, hit_info.tangent.z, 0.0f };
			tangent_to_world_mat.r1 = { hit_info.binormal.x, hit_info.binormal.y, hit_info.binormal.z, 0.0f };
			tangent_to_world_mat.r2 = { hit_info.normal.x, hit_info.normal.y, hit_info.normal.z, 0.0f };
			tangent_to_world_mat.r3 = { 0.0f, 0.0f, 0.0f, 1.0f };

			sg_ptr->tangent_to_world_mtx = NMath::Transpose(tangent_to_world_mat);
		}
	}

	void EvalBackground_V(SIntegratorAoContext* ictx_ptr)
	{
		ispc::SIntegratorContext_K ictx_k;
		ictx_k.active_path_stream = &ictx_ptr->active_path_stream[0];
		ictx_k.active_path_stream_length = ictx_ptr->active_path_stream.size();
		ictx_k.ao_buffer = &ictx_ptr->ao_buffer[0];
		ictx_k.eval_background_stream = &ictx_ptr->eval_background_stream[0];
		ictx_k.eval_background_stream_length = ictx_ptr->eval_background_stream.size();
		ictx_k.hit_info_buffer = &ictx_ptr->hit_info_buffer[0];
		ictx_k.ray_buffer = &ictx_ptr->ray_buffer[0];
		ictx_k.sg_buffer = &ictx_ptr->sg_buffer[0];
		ispc::EvalBackground_K(ictx_k);
	}

	void EvalBackground(SIntegratorAoContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->eval_background_stream)
		{
			ictx_ptr->ao_buffer[i] = ictx_ptr->ao_buffer[i] + F32x4{ 1.0f, 1.0f, 1.0f, 1.0f };
		}
	}

	void SampleAoRays_V(SIntegratorAoContext* ictx_ptr)
	{
		std::vector<F32x2> uv_buffer;
		uv_buffer.resize(ictx_ptr->render_settings.tile_width * ictx_ptr->render_settings.tile_height);

		for (auto i : ictx_ptr->active_path_stream)
		{
			F32x2 uv = ictx_ptr->sampler_ptr->SampleF32x2();
			uv_buffer[i] = uv;
		}

		ispc::SIntegratorContext_K ictx_k;
		ictx_k.active_path_stream = &ictx_ptr->active_path_stream[0];
		ictx_k.active_path_stream_length = ictx_ptr->active_path_stream.size();
		ictx_k.ao_buffer = &ictx_ptr->ao_buffer[0];
		ictx_k.eval_background_stream = &ictx_ptr->eval_background_stream[0];
		ictx_k.eval_background_stream_length = ictx_ptr->eval_background_stream.size();
		ictx_k.hit_info_buffer = &ictx_ptr->hit_info_buffer[0];
		ictx_k.ray_buffer = &ictx_ptr->ray_buffer[0];
		ictx_k.sg_buffer = &ictx_ptr->sg_buffer[0];
		ispc::SampleAoRays_K((ispc::SVector2f*)&uv_buffer[0], ictx_k);
	}

	void SampleAoRays(SIntegratorAoContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->active_path_stream)
		{
			SShaderGlobals sg = ictx_ptr->sg_buffer[i];
			F32x2 uv = ictx_ptr->sampler_ptr->SampleF32x2();
			F32x3 dir = SampleHemisphereCosineWeighted(uv.x, uv.y);
			dir = NMath::TransformDir(sg.tangent_to_world_mtx, dir);

			SRay ray = { sg.pos, dir, 0.0f, std::numeric_limits<F32>::max() };
			ictx_ptr->ray_buffer[i] = ray;

			SIntersectionData hit_info;
			hit_info.intersection_type = INTERSECTION_TYPE_BACKGROUND;
			ictx_ptr->hit_info_buffer[i] = hit_info;
		}
	}

	void ComputeAo_V(SIntegratorAoContext* ictx_ptr)
	{
		ispc::SIntegratorContext_K ictx_k;
		ictx_k.active_path_stream = &ictx_ptr->active_path_stream[0];
		ictx_k.active_path_stream_length = ictx_ptr->active_path_stream.size();
		ictx_k.ao_buffer = &ictx_ptr->ao_buffer[0];
		ictx_k.eval_background_stream = &ictx_ptr->eval_background_stream[0];
		ictx_k.eval_background_stream_length = ictx_ptr->eval_background_stream.size();
		ictx_k.hit_info_buffer = &ictx_ptr->hit_info_buffer[0];
		ictx_k.ray_buffer = &ictx_ptr->ray_buffer[0];
		ictx_k.sg_buffer = &ictx_ptr->sg_buffer[0];
		ispc::ComputeAo_K(ictx_k);
	}
		
	void ComputeAo(SIntegratorAoContext* ictx_ptr)
	{
		for (auto i : ictx_ptr->active_path_stream)
		{
			SIntersectionData hit_info = ictx_ptr->hit_info_buffer[i];
			F32x4 ao;

			if (hit_info.intersection_type == INTERSECTION_TYPE_BACKGROUND)
			{
				ao = { 1.0f, 1.0f, 1.0f, 1.0f };
			}
			else if (hit_info.intersection_type == INTERSECTION_TYPE_MESH_SURFACE)
			{
				SShaderGlobals sg = ictx_ptr->sg_buffer[i];
				F32 d = NMath::Distance(sg.pos, hit_info.pos);
				F32 ao_factor = d / ictx_ptr->integrator_settings.max_ao_distance;
				ao_factor = 0.0f;
				ao = NMath::INV_PI * F32x4{ ao_factor, ao_factor, ao_factor, 1.0f };
			}

			ictx_ptr->ao_buffer[i] = ictx_ptr->ao_buffer[i] + ao;
		}
	}
	
}

void CIntegratorAo::Execute(SWorkQueue* work_queue_ptr, SSceneContext* scene_ctx_ptr, CFramebuffer* framebuffer_ptr)
{
	STileInfo tile_info;
	U32 tile_id = 0;

	while (!work_queue_ptr->DequeueId(tile_id, tile_info))
	{
		m_ctx.ao_buffer.resize(tile_info.width * tile_info.height);
		m_ctx.aux_buffer.resize(tile_info.width * tile_info.height);
		
		for (U32 px = 0; px < tile_info.width * tile_info.height; px++)
		{
			m_ctx.ao_buffer[px] = { 0.0f, 0.0f, 0.0f, 1.0f };
			m_ctx.aux_buffer[px] = { 0.0f, 0.0f, 0.0f, 1.0f };
		}

		F32 error = std::numeric_limits<F32>::max();

		U32 tile_it = 0;

		for (U32 i = 0; i < m_ctx.integrator_settings.iteration_count; i++)
		{
			if (error < 0.001f) break;

			error = 0;

			m_ctx.active_path_stream.clear();

			for (U32 k = 0; k < m_ctx.render_settings.tile_width * m_ctx.render_settings.tile_height; k++)
			{
				m_ctx.active_path_stream.push_back(k);
			}

			/* Sample camera rays */
			scene_ctx_ptr->camera_manager_ptr->GenerateRays(m_ctx.sampler_ptr, m_ctx.render_settings, tile_info, m_ctx.ray_buffer);

			/* Intersect extension rays */
			SIntersectorQueryData query_data;
			query_data.ray_buffer = &m_ctx.ray_buffer;
			query_data.ray_stream = &m_ctx.active_path_stream;
			query_data.hit_info_buffer = &m_ctx.hit_info_buffer;
			m_ctx.intersector.QueryIntersection(scene_ctx_ptr, query_data);

			NIntegratorAo::FilterCameraRays(&m_ctx);
			
			NIntegratorAo::EvalBackground_V(&m_ctx);
			m_ctx.eval_background_stream.clear();

			NIntegratorAo::ExtendRays_V(&m_ctx);
			
			NIntegratorAo::SampleAoRays_V(&m_ctx);
			m_ctx.intersector.QueryIntersection(scene_ctx_ptr, query_data);
			NIntegratorAo::ComputeAo_V(&m_ctx);

			if (i % 2 != 0)
			{
				for (U32 y = 0; y < tile_info.height; y++)
				for (U32 x = 0; x < tile_info.width; x++)
				{
					U32 px = y * tile_info.width + x;

					/*
					F32x4 radiance = m_ctx.ao_buffer[px] / F32(i + 1);
					radiance.x = NMath::Clamp(powf(radiance.x, 1.0f / 2.2f), 0.0f, 1.0f);
					radiance.y = NMath::Clamp(powf(radiance.y, 1.0f / 2.2f), 0.0f, 1.0f);
					radiance.z = NMath::Clamp(powf(radiance.z, 1.0f / 2.2f), 0.0f, 1.0f);
					m_ctx.aux_buffer[px] = m_ctx.aux_buffer[px] + radiance;
					*/

					m_ctx.aux_buffer[px] = m_ctx.aux_buffer[px] + m_ctx.ao_buffer[px];
				}
			}

			for (U32 y = 0; y < tile_info.height; y++)
			for (U32 x = 0; x < tile_info.width; x++)
			{
				U32 px = y * tile_info.width + x;
				F32x4 c0 = m_ctx.ao_buffer[px] / static_cast<F32>(i + 1);
				c0.x = NMath::Clamp(powf(c0.x, 1.0f / 2.2f), 0.0f, 1.0f);
				c0.y = NMath::Clamp(powf(c0.y, 1.0f / 2.2f), 0.0f, 1.0f);
				c0.z = NMath::Clamp(powf(c0.z, 1.0f / 2.2f), 0.0f, 1.0f);

				F32x4 c1 = m_ctx.aux_buffer[px] / static_cast<F32>(((i - 1) / 2) + 1);
				c1.x = NMath::Clamp(powf(c1.x, 1.0f / 2.2f), 0.0f, 1.0f);
				c1.y = NMath::Clamp(powf(c1.y, 1.0f / 2.2f), 0.0f, 1.0f);
				c1.z = NMath::Clamp(powf(c1.z, 1.0f / 2.2f), 0.0f, 1.0f);

				error += (abs(c0.x - c1.y) + abs(c0.y - c1.y) + abs(c0.z - c1.z)) / (sqrt(c0.x + c0.y + c0.z) + 1e-3f);
			}

			error /= tile_info.width * tile_info.height;
			// error *= 2.0f;
			tile_it++;
		}

		/* Accumulate radiance to framebuffer */
		framebuffer_ptr->LockW();
		for (U32 y = 0; y < tile_info.height; y++)
		for (U32 x = 0; x < tile_info.width; x++)
		{
			U32 result_idx = y * tile_info.width + x;
			framebuffer_ptr->SetColorBufferPx(x + tile_info.offset_x, y + tile_info.offset_y, m_ctx.ao_buffer[result_idx] / F32(tile_it));
		}
		framebuffer_ptr->UnlockW();

		m_ctx.ao_buffer.clear();

		work_queue_ptr->SetTileStateFinished(tile_id);
	}
}

IIntegrator* CIntegratorAoFactory::Create(SRenderSettings* render_settings, void* integrator_settings, CSceneIntersector& intersector) const
{
	return new CIntegratorAo(*render_settings, *static_cast<SIntegratorAoSettings*>(integrator_settings), intersector);
}