
#pragma once

#include <vector>

#include "Foundation/Common.hpp"
#include "Foundation/Geometry.hpp"
#include "Foundation/ISampler.hpp"
#include "Foundation/Sampling.hpp"
#include "Foundation/Shading.hpp"
#include "Foundation/WorkManager.hpp"
#include "Foundation/Framebuffer.hpp"
#include "Foundation/IIntegrator.hpp"
#include "Foundation/IIntersector.hpp"
#include "Sampler/SamplerUniform.hpp"
#include "Scene/SceneContext.hpp"
#include "Scene/SceneIntersector.hpp"

struct SIntegratorAoSettings
{
	U32 iteration_count;
	F32 max_ao_distance;
};

struct SIntegratorAoContext
{
	std::vector<SRay> ray_buffer;
	std::vector<SIntersectionData> hit_info_buffer;
	std::vector<SShaderGlobals> sg_buffer;
	std::vector<F32x4> ao_buffer;
	std::vector<F32x4> aux_buffer;

	std::vector<U32> active_path_stream;
	std::vector<U32> eval_background_stream;

	CSceneIntersector intersector;
	ISampler* sampler_ptr;
	SRenderSettings render_settings;
	SIntegratorAoSettings integrator_settings;
};

class CIntegratorAo : public IIntegrator
{
	public:
		CIntegratorAo(SRenderSettings& render_settings, SIntegratorAoSettings& integrator_settings, CSceneIntersector& intersector) 
		{
			m_ctx.render_settings = render_settings;
			m_ctx.integrator_settings = integrator_settings;
			m_ctx.intersector = intersector;

			U32 pixel_count = render_settings.tile_width * render_settings.tile_height;

			m_ctx.ray_buffer.resize(pixel_count);
			m_ctx.hit_info_buffer.resize(pixel_count);
			m_ctx.sg_buffer.resize(pixel_count);
			// m_ctx.ao_buffer.resize(pixel_count);

			m_ctx.hit_info_buffer.resize(pixel_count);
			// m_ctx.active_path_stream.resize(pixel_count);
			// m_ctx.eval_background_stream.resize(pixel_count);

			m_ctx.sampler_ptr = new CSamplerUniform{ 666 };
		};

		~CIntegratorAo() {};

		void Init() override {};
		void Update() override {};
		void Execute(SWorkQueue* work_queue_ptr, SSceneContext* scene_ctx_ptr, CFramebuffer* framebuffer_ptr) override;

	protected:

	private:
		SIntegratorAoContext m_ctx;
};

class CIntegratorAoFactory : public IIntegratorFactory
{
	public:
		CIntegratorAoFactory() {};
		~CIntegratorAoFactory() {};

		virtual IIntegrator* Create(SRenderSettings* render_settings, void* integrator_settings, CSceneIntersector& intersector) const override;

	protected:

	private:

};
