
#pragma once

#include <vector>

#include "Foundation/Common.hpp"
#include "Foundation/IIntegrator.hpp"
#include "Foundation/IIntersector.hpp"
#include "Foundation/ISampler.hpp"
#include "Foundation/Sampling.hpp"
#include "Foundation/Shading.hpp"
#include "Foundation/WorkManager.hpp"
#include "Foundation/Framebuffer.hpp"
#include "Sampler/SamplerUniform.hpp"
#include "Sampler/SamplerSobol.hpp"
#include "Scene/SceneContext.hpp"
#include "Scene/SceneIntersector.hpp"

struct SPath
{
	F32x4 throughput;
	F32x4 radiance;
};

struct SIntegratorUdptSettings
{
	U32 iteration_count;
	U32 di_sample_count;
	U32 ii_sample_count;
	U32 max_indirect_bounce_count;
	bool enable_mis;
	bool enable_rr;
	bool enable_as;
	F32 as_termination_threshold;
};

struct SIntegratorUdptContext
{
	/* Persistent */
	std::vector<SPath> path_buffer;
	std::vector<SShaderGlobals> sg_buffer;
	std::vector<F32x4> contrib_buffer;
	std::vector<F32x4> aux_buffer;

	/* Temp */
	std::vector<SSampleInfo> sample_info_buffer;
	std::vector<SSampleInfo> bsdf_sample_info_buffer;
	std::vector<SSampleInfo> light_sample_info_buffer;
	std::vector<SRay> ray_buffer;
	std::vector<SIntersectionData> hit_info_buffer;

	/* Stream */
	std::vector<U32> active_path_stream;
	std::vector<U32> light_eval_stream;
	std::vector<U32> background_eval_stream;

	SRenderSettings render_settings;
	SIntegratorUdptSettings integrator_settings;

	CSceneIntersector intersector;
	ISampler* sampler_ptr;
};

class CIntegratorUdpt : public IIntegrator
{
	public:
		CIntegratorUdpt(SRenderSettings& render_settings, SIntegratorUdptSettings& integrator_settings, CSceneIntersector& intersector) 
		{
			m_ctx.render_settings = render_settings;
			m_ctx.integrator_settings = integrator_settings;
			m_ctx.intersector = intersector;

			U32 pixel_count = render_settings.tile_width * render_settings.tile_height;

			m_ctx.ray_buffer.resize(pixel_count);
			m_ctx.hit_info_buffer.resize(pixel_count);
			m_ctx.sg_buffer.resize(pixel_count);
			m_ctx.sample_info_buffer.resize(pixel_count);
			m_ctx.path_buffer.resize(pixel_count);
			
			// m_ctx.sampler_ptr = new CSamplerUniform{ 666 };
			m_ctx.sampler_ptr = new CSamplerSobol{ 666 };

			// m_ctx.active_path_stream.resize(pixel_count);
			// m_ctx.eval_background_stream.resize(pixel_count);
		};

		~CIntegratorUdpt() {};

		virtual void Init() override {};
		virtual void Update() override {};
		virtual void Execute(SWorkQueue* work_queue_ptr, SSceneContext* scene_ctx_ptr, CFramebuffer* framebuffer_ptr) override;

	protected:

	private:
		SIntegratorUdptContext m_ctx;
};

class CIntegratorUdptFactory : public IIntegratorFactory
{
	public:
		virtual IIntegrator* Create(SRenderSettings* render_settings, void* integrator_settings, CSceneIntersector& intersector) const override;

	protected:

	private:
};