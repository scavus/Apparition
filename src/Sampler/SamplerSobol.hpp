
#pragma once

#include <vector>

#include "Foundation/Random.hpp"
#include "Foundation/ISampler.hpp"
#include "Sampler/SamplerUniform.hpp"

class CSamplerSobol : public ISampler
{
	public:
		CSamplerSobol(U32 seed)
			: m_aux_sampler(CSamplerUniform{ seed })
		{
			m_seed = seed;
			m_scramble = 0;
			m_index = 0;
			m_dimension = 0;
		};

		CSamplerSobol(U32 seed, U32 scramble, U32 index, U32 dimension)
			: m_aux_sampler(CSamplerUniform{ seed })
		{
			m_seed = seed;
			m_scramble = scramble;
			m_index = index;
			m_dimension = dimension;
		};

		virtual ISampler* GetSubsampler(U32 idx) override { return &m_subsamplers[idx]; };
		virtual void InitSequence(U32 px, U32 sample_idx) override;

		virtual U32 SampleU32() override;
		virtual F32 SampleF32() override;
		virtual F32x2 SampleF32x2() override;

		// void GenerateF32N();
		// void GenerateF32x2N();

	private:
		static const U32 SOBOL_MAX_DIM_COUNT = 1024;

		std::vector<CSamplerSobol> m_subsamplers;

		CSamplerUniform m_aux_sampler;

		U32 m_seed;
		U32 m_scramble;
		U32 m_index;
		U32 m_dimension;
};
