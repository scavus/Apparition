
#include "SamplerUniform.hpp"

void CSamplerUniform::InitSequence(U32 px, U32 sample_idx)
{

}

U32 CSamplerUniform::SampleU32()
{
	return GenerateU32(&m_rng);
};

F32 CSamplerUniform::SampleF32()
{
	return GenerateF32(&m_rng);
};

// void GenerateF32N();

F32x2 CSamplerUniform::SampleF32x2()
{
	F32x2 uv;
	uv.x = GenerateF32(&m_rng);
	uv.y = GenerateF32(&m_rng);
	return uv;
};

// void GenerateF32x2N();
