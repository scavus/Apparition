
#pragma once

#include "Foundation/Random.hpp"
#include "Foundation/ISampler.hpp"

class CSamplerUniform : public ISampler
{
	public:
		CSamplerUniform(U32 seed)
		{
			SeedRng(&m_rng, seed);
		};

		virtual ISampler* GetSubsampler(U32 idx) override { return this; };
		virtual void InitSequence(U32 px, U32 sample_idx) override;

		virtual U32 SampleU32() override;

		virtual F32 SampleF32() override;

		// void GenerateF32N();

		virtual F32x2 SampleF32x2() override;

		// void GenerateF32x2N();

	private:
		SRandomNumberGenerator m_rng;
};
