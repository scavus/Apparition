
#include "SamplerSobol.hpp"

#include "sobol.h"

namespace NSamplerSobol
{
	inline U32 PermuteIndex(U32 index, U32 scramble)
	{
		return (index & ~0xFF) | ((index + scramble) & 0xFF);
	}
}

void CSamplerSobol::InitSequence(U32 px, U32 sample_idx)
{
	m_subsamplers.clear();

	for (U32 i = 0; i < px; i++)
	{
		U32 scramble = m_seed ^ HashFnWang(i);
		U32 index = sample_idx;
		U32 dimension = 0;

		m_subsamplers.push_back(CSamplerSobol{ m_seed, scramble, index, dimension });
	}
}

U32 CSamplerSobol::SampleU32()
{
	return static_cast<U32>(SampleF32() * std::numeric_limits<U32>::max());
}

F32 CSamplerSobol::SampleF32()
{
	if (m_dimension >= SOBOL_MAX_DIM_COUNT)
	{
		return m_aux_sampler.SampleF32();
	}

	return sobol::sample(NSamplerSobol::PermuteIndex(m_index, m_scramble), m_dimension++, m_scramble);
}

F32x2 CSamplerSobol::SampleF32x2()
{
	return F32x2{ SampleF32(), SampleF32() };
}
