
/* #include "ArCore.h" */
#include "Foundation/Math.hpp"
#include "Foundation/Geometry.hpp"
#include "Intersector/IntersectorEmbree.hpp"
#include "Intersector/IntersectorLightLinear.hpp"
#include "Scene/SceneContext.hpp"
#include "Scene/SceneIo.hpp"
#include "Scene/CameraManager.hpp"
#include "Scene/LightManager.hpp"
#include "Scene/MeshManager.hpp"
#include "Scene/SceneIntersector.hpp"
#include "Scene/ShaderManager.hpp"

#include <vector>

#ifdef _MSC_VER
#ifdef AR_BUILDING
#define AR_API extern "C" __declspec(dllexport)
#else
#define AR_API extern "C" __declspec(dllimport)
#endif
#else
#ifdef AR_BUILDING
#define AR_API extern "C"
#else
#define AR_API extern "C"
#endif
#endif

AR_API typedef bool ArBool;
AR_API typedef U32 ArU32;

AR_API typedef F32 ArF32;
AR_API typedef F32x2 ArF32x2;
AR_API typedef F32x3 ArF32x3;
AR_API typedef F32x4 ArF32x4;
AR_API typedef SMatrix4x4 ArMatrix4x4;

AR_API typedef SQuad ArQuad;
AR_API typedef SDisk ArDisk;
AR_API typedef SSphere ArSphere;
AR_API typedef STriangle ArTriangle;
AR_API typedef SAabb ArAabb;

AR_API typedef SRay ArRay;
AR_API typedef SIntersectionData ArHitInfo;

std::vector<SSceneContext*> g_scene_ctx_ptrs;
std::vector<CSceneIntersector*> g_scene_intersector_ptrs;


// std::cout << "[BREAK] " << __FILE__ << "::" << __LINE__ << std::endl;

AR_API U32 arLoadScene(const char* filename)
{
	std::cout << "[INFO] " << "scene = " << filename << std::endl;
	U32 scene_ctx_id;

	auto scene_ctx_ptr = new SSceneContext;

	SSceneStorage scene_storage;
	scene_storage.camera_storage = new CCameraManager::SStorage;
	scene_storage.mesh_storage = new CMeshManager::SStorage;
	scene_storage.mesh_geom_storage = new CMeshGeomManager::SStorage;
	scene_storage.area_light_storage = new CLightManager::SStorage;
	// scene_storage.shader_storage = new CShaderManager::SStorage;


	NSceneIo::Import(filename, scene_storage);
	scene_ctx_ptr->camera_manager_ptr = new CCameraManager{ scene_storage.camera_storage, scene_ctx_ptr };
	scene_ctx_ptr->mesh_manager_ptr = new CMeshManager{ scene_storage.mesh_storage, scene_ctx_ptr };
	scene_ctx_ptr->mesh_geom_manager_ptr = new CMeshGeomManager{ scene_storage.mesh_geom_storage, scene_ctx_ptr };
	scene_ctx_ptr->light_manager_ptr = new CLightManager{ scene_storage.area_light_storage, scene_ctx_ptr };
	// scene_ctx.shader_manager_ptr = new CShaderManager{ &shader_storage, &scene_ctx };
	std::cout << "[INFO] " << "prim-count = " << scene_storage.mesh_geom_storage->mesh_geom.size() << std::endl;
	// scene_ctx_ptr->shader_manager_ptr = new CShadingEngineOsl;

	g_scene_ctx_ptrs.push_back(scene_ctx_ptr);
	scene_ctx_id = g_scene_ctx_ptrs.size() - 1;
	NSceneContext::Update(scene_ctx_ptr);

	return scene_ctx_id;
}

AR_API U32 arCreateIntersector(U32 scene_ctx_id, const char* intersector_type)
{
	auto scene_ctx_ptr = g_scene_ctx_ptrs[scene_ctx_id];

	auto intersector_ptr = new CSceneIntersector;

	// CAcceleratorBvh2 mesh_accel(&scene_ctx);
	// CIntersectorBvh2 mesh_intersector(&mesh_accel);
	auto mesh_intersector_ptr = new CIntersectorEmbree{ scene_ctx_ptr };

	auto light_intersector_ptr = new CIntersectorLightLinear;

	intersector_ptr->SetMeshIntersector(mesh_intersector_ptr);
	intersector_ptr->SetLightIntersector(light_intersector_ptr);

	// mesh_accel.Update();
	std::cout << "[INFO] " << "building acc ... ";
	mesh_intersector_ptr->Update();
	std::cout << "done" << std::endl;

	g_scene_intersector_ptrs.push_back(intersector_ptr);
	return g_scene_intersector_ptrs.size() - 1;
}

AR_API ArHitInfo arQueryIntersection1(U32 scene_ctx_id, U32 intersector_id, ArRay ray_ptr)
{
	ArHitInfo hit_info;
	auto scene_ctx_ptr = g_scene_ctx_ptrs[scene_ctx_id];
	auto intersector_ptr = g_scene_intersector_ptrs[intersector_id];
	intersector_ptr->QueryIntersection1(scene_ctx_ptr, ray_ptr, hit_info);

	return hit_info;
}

AR_API ArRay arCameraGenerateRay(U32 scene_ctx_id, ArF32x2 uv, ArF32x2 pixel_coord, ArU32 render_width, ArU32 render_height)
{
	ArRay ray;
	auto scene_ctx_ptr = g_scene_ctx_ptrs[scene_ctx_id];
	ray = scene_ctx_ptr->camera_manager_ptr->GenerateRay(uv, pixel_coord, render_width, render_height);
	return ray;
}

AR_API ArMatrix4x4 arTranspose(ArMatrix4x4 m)
{
	return NMath::Transpose(m);
}

AR_API ArF32x3 arTransformDir(ArMatrix4x4 m, ArF32x3 v)
{
	return NMath::TransformDir(m, v);
}

AR_API ArF32x3 arSampleHemisphereCosineWeighted(ArF32 u, ArF32 v)
{
	return SampleHemisphereCosineWeighted(u, v);
}
