
#include "IntersectorBvh2.hpp"

#include "Foundation/Math.hpp"
#include "Foundation/Geometry.hpp"
#include "Accelerator/AcceleratorBvh2.hpp"

#include "IntersectorBvh2Kernel_ispc.h"

namespace NIntersectorBvh2
{
	void QueryIntersection_V(SIntersectorQueryData& query_data, const std::vector<STriangle>& geom_buffer, const std::vector<SLinearBvh2Node>& node_buffer)
	{
		ispc::SIntersectorQueryData query_data_k;
		query_data_k.ray_buffer = &(*query_data.ray_buffer)[0];
		query_data_k.ray_stream = &(*query_data.ray_stream)[0];
		query_data_k.ray_stream_length = query_data.ray_stream->size();
		query_data_k.hit_info_buffer = &(*query_data.hit_info_buffer)[0];
		QueryIntersection_K(query_data_k, (ispc::STriangle*)&geom_buffer[0], (ispc::SLinearBvh2Node*)&node_buffer[0]);

		// QueryIntersectionLinear_K(query_data_k, (ispc::STriangle*)&geom_buffer[0], (U32)geom_buffer.size());
	}

	void QueryIntersection(SIntersectorQueryData& query_data, const std::vector<STriangle>& geom_buffer, const std::vector<SLinearBvh2Node>& node_buffer)
	{
		for (auto i : *query_data.ray_stream)
		{
			SRay* ray_ptr = &(*query_data.ray_buffer)[i];
			SIntersectionData* intersection_data_ptr = &(*query_data.hit_info_buffer)[i];
			U32 traversal_stack[64];
			const S32 stack_head_idx_null = -1;
			S32 stack_head_idx = -1;

			SIntersectionData intersection_data;

			F32x3 inv_dir = { 1.0f / ray_ptr->direction.x, 1.0f / ray_ptr->direction.y, 1.0f / ray_ptr->direction.z };
			// bool DirIsNeg[3] = {InvDir.x < 0, InvDir.y < 0, InvDir.z < 0};
			// FIXME: MSVC gives "Constant too big" error

			bool terminated_flag = false;
			U32 current_node_id = 0;
			SLinearBvh2Node current_node = node_buffer[current_node_id];

			while (!terminated_flag)
			{
				while (current_node.prim_count == 0)
				{
					SAabb aabb0;
					SAabb aabb1;

					aabb0 = node_buffer[current_node_id + 1].aabb;
					aabb1 = node_buffer[current_node.offset].aabb;

					bool bIntersectsChildNode0 = IntersectAabbFast2(ray_ptr, &aabb0, inv_dir);
					bool bIntersectsChildNode1 = IntersectAabbFast2(ray_ptr, &aabb1, inv_dir);

					if (!bIntersectsChildNode0 && !bIntersectsChildNode1)
					{
						if (stack_head_idx != stack_head_idx_null)
						{
							current_node_id = traversal_stack[stack_head_idx];
							current_node = node_buffer[current_node_id];
							stack_head_idx--;
						}
						else
						{
							terminated_flag = true;
							break;
						}
					}
					else if (bIntersectsChildNode0 && bIntersectsChildNode1)
					{
						// Push the farthest node
						/*
						if (DirIsNeg[current_node.axis])
						{
							stack_head_idx++;
							traversal_stack[stack_head_idx] = current_node_id + 1;

							current_node_id = current_node.offset;
							current_node = nodes[current_node_id];
						}
						else
						{
							stack_head_idx++;
							traversal_stack[stack_head_idx] = current_node.offset;

							current_node_id = current_node_id + 1;
							current_node = nodes[current_node_id];
						}
						*/

						stack_head_idx++;
						traversal_stack[stack_head_idx] = current_node.offset;
						current_node_id = current_node_id + 1;
						current_node = node_buffer[current_node_id];
					}
					else if (bIntersectsChildNode0 && !bIntersectsChildNode1)
					{
						current_node_id = current_node_id + 1;
						current_node = node_buffer[current_node_id];
					}
					else
					{
						current_node_id = current_node.offset;
						current_node = node_buffer[current_node_id];
					}
				}

				while (current_node.prim_count > 0)
				{
					for (U32 prim_idx = 0; prim_idx < current_node.prim_count; prim_idx++)
					{
						STriangle triangle = geom_buffer[current_node.offset + prim_idx];

						bool bIntersects;
						bIntersects = IntersectTriangle(ray_ptr, &triangle, &intersection_data);

						if (bIntersects)
						{
							F32 hit_distance = NMath::Distance(ray_ptr->origin, intersection_data.pos);

							if (hit_distance < ray_ptr->t_max)
							{
								intersection_data_ptr->intersection_type = INTERSECTION_TYPE_MESH_SURFACE;
								intersection_data_ptr->pos = intersection_data.pos;
								intersection_data_ptr->normal = intersection_data.normal;
								intersection_data_ptr->binormal = intersection_data.binormal;
								intersection_data_ptr->tangent = intersection_data.tangent;
								intersection_data_ptr->texcoord = intersection_data.texcoord;
								intersection_data_ptr->occluder_id = triangle.mesh_id;

								ray_ptr->t_max = hit_distance;
							}
						}
					}

					if (stack_head_idx == stack_head_idx_null)
					{
						terminated_flag = true;
						break;
					}
					else
					{
						current_node_id = traversal_stack[stack_head_idx];

						current_node = node_buffer[current_node_id];
						stack_head_idx--;
					}
				}
			}
		}
	}
}

void CIntersectorBvh2::Update()
{

}

void CIntersectorBvh2::QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data)
{
	const SAcceleratorBvh2Storage& acc_storage  = m_accelerator_ptr->GetStorageConstRef();
	NIntersectorBvh2::QueryIntersection_V(query_data, acc_storage.temp_mesh_geom, acc_storage.mesh_bvh2_nodes);
}

void CIntersectorBvh2::QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info)
{

}

void CIntersectorBvh2::QueryOcclusion()
{
}
