
#include "IntersectorEmbree.hpp"

#include "Scene/SceneContext.hpp"

#include "embree2/rtcore.h"
#include "embree2/rtcore_ray.h"

namespace NIntersectorEmbree
{
	void ConvertRays(const std::vector<SRay>& in_ray_buffer, std::vector<RTCRay>& out_ray_buffer)
	{
		size_t ray_count = in_ray_buffer.size();
		out_ray_buffer.reserve(ray_count);

		for (size_t i = 0; i < ray_count; i++)
		{
			SRay in_ray = in_ray_buffer[i];

			RTCRay out_ray;
			out_ray.org[0] = in_ray.origin.x;
			out_ray.org[1] = in_ray.origin.y;
			out_ray.org[2] = in_ray.origin.z;

			out_ray.dir[0] = in_ray.direction.x;
			out_ray.dir[1] = in_ray.direction.y;
			out_ray.dir[2] = in_ray.direction.z;

			out_ray.tnear = 0.001f;
			out_ray.tfar = in_ray.t_max;

			out_ray.geomID = RTC_INVALID_GEOMETRY_ID;
			out_ray.primID = RTC_INVALID_GEOMETRY_ID;
			out_ray.mask = -1;
			out_ray.time = 0;

			out_ray_buffer.push_back(out_ray);
		}
	}

	void ProcessPostIntersectionData(const std::vector<RTCRay>& in_hit_info_buffer, const std::vector<STriangle>& tri_buffer, std::vector<SRay>& out_ray_buffer, std::vector<SIntersectionData>& out_hit_info_buffer)
	{
		for (size_t i = 0; i < in_hit_info_buffer.size(); i++)
		{
			RTCRay in_hit_info = in_hit_info_buffer[i];
			SIntersectionData out_hit_info;

			out_ray_buffer[i].t_max = in_hit_info.tfar;

			if (in_hit_info.geomID == RTC_INVALID_GEOMETRY_ID)
			{
				out_hit_info.intersection_type = EIntersectionType::INTERSECTION_TYPE_BACKGROUND;
			}
			else
			{
				F32 u = in_hit_info.u;
				F32 v = in_hit_info.v;
				F32 w = 1.0f - u - v;

				STriangle tri = tri_buffer[in_hit_info.primID];

				out_hit_info.pos =  w * tri.vtx[0].pos + u * tri.vtx[1].pos + v * tri.vtx[2].pos;
				out_hit_info.normal = w * tri.vtx[0].normal + u * tri.vtx[1].normal + v * tri.vtx[2].normal;
				out_hit_info.tangent = w * tri.vtx[0].tangent + u * tri.vtx[1].tangent + v * tri.vtx[2].tangent;
				out_hit_info.binormal = w * tri.vtx[0].binormal + u * tri.vtx[1].binormal + v * tri.vtx[2].binormal;
				out_hit_info.texcoord = w * tri.vtx[0].texcoord + u * tri.vtx[1].texcoord + v * tri.vtx[2].texcoord;
				out_hit_info.intersection_type = EIntersectionType::INTERSECTION_TYPE_MESH_SURFACE;
				out_hit_info.occluder_id = tri.mesh_id;
			}

			out_hit_info_buffer.push_back(out_hit_info);
		}

	}
}

CIntersectorEmbree::CIntersectorEmbree(SSceneContext* scene_ctx_ptr)
{
	m_scene_ctx_ptr = scene_ctx_ptr;

	m_device = rtcNewDevice();
	m_scene = rtcDeviceNewScene(m_device, RTCSceneFlags::RTC_SCENE_HIGH_QUALITY, RTCAlgorithmFlags::RTC_INTERSECT1);
}

CIntersectorEmbree::~CIntersectorEmbree()
{
	rtcDeleteScene(m_scene);
	rtcDeleteDevice(m_device);
}

void CIntersectorEmbree::Update()
{
	const std::vector<STriangle>& tri_buffer = m_scene_ctx_ptr->mesh_geom_manager_ptr->GetTransformedMeshGeomConstRef();
	U32 mesh_id = rtcNewTriangleMesh(m_scene, RTCGeometryFlags::RTC_GEOMETRY_STATIC, tri_buffer.size(), tri_buffer.size() * 3);

	U32 vidx = 0;

	for (auto& tri : tri_buffer)
	{
		F32x3 vtx0 = tri.vtx[0].pos;
		F32x3 vtx1 = tri.vtx[1].pos;
		F32x3 vtx2 = tri.vtx[2].pos;

		m_vertex_buffer.push_back(vtx0);
		m_vertex_buffer.push_back(vtx1);
		m_vertex_buffer.push_back(vtx2);

		STriangleIndex index;
		index.v0 = vidx++;
		index.v1 = vidx++;
		index.v2 = vidx++;

		m_index_buffer.push_back(index);
	}

	rtcSetBuffer(m_scene, mesh_id, RTCBufferType::RTC_VERTEX_BUFFER, &m_vertex_buffer[0], 0, sizeof(F32x3));
	rtcSetBuffer(m_scene, mesh_id, RTCBufferType::RTC_INDEX_BUFFER, &m_index_buffer[0], 0, sizeof(STriangleIndex));

	rtcCommit(m_scene);
	auto err = rtcDeviceGetError(m_device);
}

void CIntersectorEmbree::QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data)
{
	if (query_data.ray_stream->size() == 0)
	{
		return;
	}

	std::vector<SRay> in_ray_buffer;
	std::vector<RTCRay> out_ray_buffer;

	for (auto i : *(query_data.ray_stream))
	{
		in_ray_buffer.push_back((*(query_data.ray_buffer))[i]);
	}

	NIntersectorEmbree::ConvertRays(in_ray_buffer, out_ray_buffer);

	RTCIntersectContext intersect_ctx;
	intersect_ctx.flags = RTCIntersectFlags::RTC_INTERSECT_INCOHERENT;
	rtcIntersect1M(m_scene, &intersect_ctx, &out_ray_buffer[0], out_ray_buffer.size(), sizeof(RTCRay));

	std::vector<SIntersectionData> hit_info_buffer;

	NIntersectorEmbree::ProcessPostIntersectionData(out_ray_buffer, scene_ctx_ptr->mesh_geom_manager_ptr->GetTransformedMeshGeomConstRef(), in_ray_buffer, hit_info_buffer);

	for (U32 i = 0; i < (*query_data.ray_stream).size(); i++)
	{
		U32 ray_id = (*query_data.ray_stream)[i];
		(*query_data.ray_buffer)[ray_id] = in_ray_buffer[i];
		(*query_data.hit_info_buffer)[ray_id] = hit_info_buffer[i];
	}

}

void CIntersectorEmbree::QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info)
{
	RTCRay rtc_ray;
	rtc_ray.org[0] = ray.origin.x;
	rtc_ray.org[1] = ray.origin.y;
	rtc_ray.org[2] = ray.origin.z;
	rtc_ray.dir[0] = ray.direction.x;
	rtc_ray.dir[1] = ray.direction.y;
	rtc_ray.dir[2] = ray.direction.z;

	rtc_ray.tnear = 0.001f;
	rtc_ray.tfar = ray.t_max;

	rtc_ray.geomID = RTC_INVALID_GEOMETRY_ID;
	rtc_ray.primID = RTC_INVALID_GEOMETRY_ID;
	rtc_ray.mask = -1;
	rtc_ray.time = 0;

	rtcIntersect(m_scene, rtc_ray);

	ray.t_max = rtc_ray.tfar;

	if (rtc_ray.geomID == RTC_INVALID_GEOMETRY_ID)
	{
		hit_info.intersection_type = EIntersectionType::INTERSECTION_TYPE_BACKGROUND;
	}
	else
	{
		F32 u = rtc_ray.u;
		F32 v = rtc_ray.v;
		F32 w = 1.0f - u - v;

		auto tri_buffer = scene_ctx_ptr->mesh_geom_manager_ptr->GetTransformedMeshGeomConstRef();

		STriangle tri = tri_buffer[rtc_ray.primID];

		hit_info.pos =  w * tri.vtx[0].pos + u * tri.vtx[1].pos + v * tri.vtx[2].pos;
		hit_info.normal = w * tri.vtx[0].normal + u * tri.vtx[1].normal + v * tri.vtx[2].normal;
		hit_info.tangent = w * tri.vtx[0].tangent + u * tri.vtx[1].tangent + v * tri.vtx[2].tangent;
		hit_info.binormal = w * tri.vtx[0].binormal + u * tri.vtx[1].binormal + v * tri.vtx[2].binormal;
		hit_info.texcoord = w * tri.vtx[0].texcoord + u * tri.vtx[1].texcoord + v * tri.vtx[2].texcoord;
		hit_info.intersection_type = EIntersectionType::INTERSECTION_TYPE_MESH_SURFACE;
		hit_info.occluder_id = tri.mesh_id;
	}
}

void CIntersectorEmbree::QueryOcclusion()
{

}
