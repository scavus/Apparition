
#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/IIntersector.hpp"
#include "Accelerator/AcceleratorBvh2.hpp"

class CIntersectorBvh2 : public IIntersector
{
	public:
		CIntersectorBvh2(CAcceleratorBvh2* accelerator_ptr) { m_accelerator_ptr = accelerator_ptr; };

		virtual void Update() override;
		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data) override;
		virtual void QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info) override;
		virtual void QueryOcclusion() override;

	protected:

	private:
		CAcceleratorBvh2* m_accelerator_ptr;

};
