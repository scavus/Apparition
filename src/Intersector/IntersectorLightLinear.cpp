
#include "IntersectorLightLinear.hpp"

#include "Foundation/Light.hpp"

namespace NIntersectorLightLinear
{
	void QueryIntersection(const std::vector<SAreaLight>& light_buffer, SIntersectorQueryData& query_data)
	{
		for (auto i : *(query_data.ray_stream))
		{
			SRay& ray = (*(query_data.ray_buffer))[i];
			SIntersectionData& hit_info = (*(query_data.hit_info_buffer))[i];
			SIntersectionData intersection_data;

			for (U32 area_light_idx = 0; area_light_idx < light_buffer.size(); area_light_idx++)
			{
				SAreaLight area_light = light_buffer[area_light_idx];
				bool b_intersects = false;

				switch (area_light.type)
				{
					case LIGHT_TYPE_QUAD:
					{
						b_intersects = IntersectQuad(&ray, &area_light.shape.quad, &intersection_data);
						break;
					}
					case LIGHT_TYPE_DISK:
					{
						b_intersects = IntersectDisk(&ray, &area_light.shape.disk, &intersection_data);
						break;
					}
					case LIGHT_TYPE_SPHERE:
					{
						b_intersects = IntersectSphere(&ray, &area_light.shape.sphere, &intersection_data);
						break;
					}
					case LIGHT_TYPE_TUBE:
					{
						break;
					}
				}

				if (b_intersects)
				{
					F32 hit_distance = NMath::Distance(ray.origin, intersection_data.pos);

					if (hit_distance < ray.t_max)
					{
						hit_info.intersection_type = INTERSECTION_TYPE_LIGHT_SURFACE;
						hit_info.pos = intersection_data.pos;
						hit_info.occluder_id = area_light_idx;

						ray.t_max = hit_distance;
					}
				}
			}
		}
	}
}

void CIntersectorLightLinear::Update()
{

}

void CIntersectorLightLinear::QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data)
{
	NIntersectorLightLinear::QueryIntersection(scene_ctx_ptr->light_manager_ptr->GetAreaLightsArrayConstRef(), query_data);
}

void CIntersectorLightLinear::QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info)
{

}

void CIntersectorLightLinear::QueryOcclusion()
{

}
