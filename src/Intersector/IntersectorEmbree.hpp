
#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/IIntersector.hpp"

#include "embree2/rtcore.h"

struct STriangleIndex
{
	int v0;
	int v1;
	int v2;
};

class CIntersectorEmbree : public IIntersector
{
	public:
		CIntersectorEmbree(SSceneContext* scene_ctx_ptr);
		~CIntersectorEmbree();

		virtual void Update() override;
		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data) override;
		virtual void QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info) override;
		virtual void QueryOcclusion() override;


	protected:

	private:
		SSceneContext* m_scene_ctx_ptr;
		RTCDevice m_device;
		RTCScene m_scene;

		std::vector<F32x3> m_vertex_buffer;
		std::vector<STriangleIndex> m_index_buffer;
};
