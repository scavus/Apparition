
#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/IIntersector.hpp"
#include "Scene/SceneContext.hpp"

class CIntersectorLightLinear : public IIntersector
{
	public:
		CIntersectorLightLinear() {};
		~CIntersectorLightLinear() {};

		virtual void Update() override;
		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data) override;
		virtual void QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info) override;
		virtual void QueryOcclusion() override;

	protected:

	private:

};
