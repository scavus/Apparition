
#pragma once

class IAccelerator
{
	public:
		virtual void Update() = 0;

	protected:

	private:
};
