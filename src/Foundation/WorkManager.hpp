
#pragma once

#include <queue>
#include <mutex>

#include "Foundation/Common.hpp"
#include "Foundation/Settings.hpp"

struct STileInfo
{
	U32 width;
	U32 height;
	U32 offset_x;
	U32 offset_y;
	bool finished;
};

struct SWorkQueue
{
	std::queue<STileInfo> tiles;
	std::mutex mut;

	std::vector<STileInfo> tile_cache;

	bool Dequeue(STileInfo& tile_info)
	{
		bool empty_flag = true;

		mut.lock();
		
		empty_flag = tiles.empty();

		if (!empty_flag)
		{
			tile_info = tiles.front();
			tiles.pop();
		}

		mut.unlock();

		return empty_flag;
	}

	bool DequeueId(U32& id, STileInfo& tile_info)
	{
		bool empty_flag = true;

		mut.lock();

		empty_flag = tiles.empty();

		if (!empty_flag)
		{
			tile_info = tiles.front();
			tile_cache.push_back(tile_info);
			tiles.pop();
			id = tile_cache.size() - 1;
		}

		mut.unlock();

		return empty_flag;
	}

	/*
	void SetTileStateProcessing(U32 id)
	{

	}
	*/

	void SetTileStateFinished(U32 id)
	{
		mut.lock();
		tile_cache[id].finished = true;
		mut.unlock();
	}

};

namespace NWorkManager
{
	void DistributeWork(SRenderSettings& settings, SWorkQueue& work_queue);
	void DistributeWorkLowGranularity(SRenderSettings& render_settings, SWorkQueue& work_queue);
}
