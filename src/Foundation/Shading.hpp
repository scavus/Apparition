
#pragma once

#include <Foundation/Common.hpp>
#include <Foundation/Math.hpp>
#include <Foundation/Geometry.hpp>

typedef struct SShaderGlobals
{
	F32x3 pos;
	F32x3 normal;
	F32x2 texcoord;

	F32x3 wo;

	SMatrix4x4 tangent_to_world_mtx;

	U32 mesh_id;
	U32 material_id;

} SShaderGlobals;
