
#include "Geometry.hpp"

F32 ComputeAreaDisk(const SDisk* disk_ptr)
{
	F32 radius = disk_ptr->radius;

	return NMath::PI * radius * radius;
}

F32 ComputeAreaSphere(const SSphere* sphere_ptr)
{
	F32 radius = sphere_ptr->radius;

	return (4.0f / 3.0f) * NMath::PI * radius * radius;
}

F32 ComputeAreaTriangle(const STriangle* triangle_ptr)
{
	F32x3 edge0 = triangle_ptr->vtx[1].pos - triangle_ptr->vtx[0].pos;
	F32x3 edge1 = triangle_ptr->vtx[2].pos - triangle_ptr->vtx[0].pos;

	return NMath::Length(NMath::Cross(edge0, edge1)) * 0.5f;
}

bool IntersectAabbFast2(SRay* ray_ptr, SAabb* aabb_ptr, F32x3 inv_dir)
{
	F32 t_min;
	F32 t_max;

	F32 Tx1 = (aabb_ptr->bound_min.x - ray_ptr->origin.x) * inv_dir.x;
	F32 Tx2 = (aabb_ptr->bound_max.x - ray_ptr->origin.x) * inv_dir.x;

	t_min = fmin(Tx1, Tx2);
	t_max = fmax(Tx1, Tx2);

	F32 Ty1 = (aabb_ptr->bound_min.y - ray_ptr->origin.y) * inv_dir.y;
	F32 Ty2 = (aabb_ptr->bound_max.y - ray_ptr->origin.y) * inv_dir.y;

	t_min = fmax(t_min, fmin(Ty1, Ty2));
	t_max = fmin(t_max, fmax(Ty1, Ty2));

	F32 Tz1 = (aabb_ptr->bound_min.z - ray_ptr->origin.z) * inv_dir.z;
	F32 Tz2 = (aabb_ptr->bound_max.z - ray_ptr->origin.z) * inv_dir.z;

	t_min = fmax(t_min, fmin(Tz1, Tz2));
	t_max = fmin(t_max, fmax(Tz1, Tz2));

	return t_max >= fmax(0.0f, t_min);
}

// CAUTION: Self-Intersection Problems due to Epsilon
// Moller-Trumbore triangle intersection
bool IntersectTriangle(SRay* ray_ptr, STriangle* triangle_ptr, SIntersectionData* intersection_data_ptr)
{
	F32x3 e1 = triangle_ptr->vtx[1].pos - triangle_ptr->vtx[0].pos;
	F32x3 e2 = triangle_ptr->vtx[2].pos - triangle_ptr->vtx[0].pos;
	F32x3 vt = ray_ptr->origin - triangle_ptr->vtx[0].pos;
	F32x3 P = NMath::Cross(ray_ptr->direction, e2);

	F32 det = NMath::Dot(e1, P);

	if (det > -1e-6f && det < 1e-6f) return false;

	F32 inv_det = 1.0f / det;
	F32 u = NMath::Dot(vt, P) * inv_det;

	if (u < 0.0f || u > 1.0f) return false;

	F32x3 q = NMath::Cross(vt, e1);
	F32 v = NMath::Dot(ray_ptr->direction, q) * inv_det;

	if (v < 0.0f || u + v > 1.0f) return false;

	F32 w = (1.0f - u - v);
	F32 t = NMath::Dot(e2, q) * inv_det;

	if (t < 1e-3f) return false;

	ray_ptr->t_min = t;

	// Barycentric interpolation
	intersection_data_ptr->pos = ray_ptr->origin + (ray_ptr->direction * t);
	// intersection_data_ptr->pos =  w * triangle_ptr->vtx[0].pos + u * triangle_ptr->vtx[1].pos + v * triangle_ptr->vtx[2].pos;

	intersection_data_ptr->normal = w * triangle_ptr->vtx[0].normal + u * triangle_ptr->vtx[1].normal + v * triangle_ptr->vtx[2].normal;
	intersection_data_ptr->tangent = w * triangle_ptr->vtx[0].tangent + u * triangle_ptr->vtx[1].tangent + v * triangle_ptr->vtx[2].tangent;
	intersection_data_ptr->binormal = w * triangle_ptr->vtx[0].binormal + u * triangle_ptr->vtx[1].binormal + v * triangle_ptr->vtx[2].binormal;
	intersection_data_ptr->texcoord = w * triangle_ptr->vtx[0].texcoord + u * triangle_ptr->vtx[1].texcoord + v * triangle_ptr->vtx[2].texcoord;

	return true;
}

/*
bool IntersectTriangleWoop(SRay* ray_ptr, STriangle* triangle_ptr, SIntersectionData* intersection_data_ptr)
{

}
*/

bool IntersectQuad(SRay* ray_ptr, SQuad* quad_ptr, SIntersectionData* intersection_data_ptr)
{
	F32 n_dot_d = NMath::Dot(quad_ptr->normal, ray_ptr->direction);

	if (n_dot_d < 1e-6f)
	{
		return false;
	}

	F32 t = NMath::Dot(quad_ptr->normal, quad_ptr->base - ray_ptr->origin) / n_dot_d;

	if (t <= 0.0f)
	{
		return false;
	}

	F32x3 point_in_plane = ray_ptr->origin + ray_ptr->direction * t;
	F32x3 Vec = point_in_plane - quad_ptr->base;
	F32 u = NMath::Dot(Vec, quad_ptr->u) * quad_ptr->inv_length_sqr_u;
	F32 v = NMath::Dot(Vec, quad_ptr->v) * quad_ptr->inv_length_sqr_v;

	if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
	{
		return false;
	}

	intersection_data_ptr->pos = point_in_plane;
	intersection_data_ptr->normal = quad_ptr->normal;
	intersection_data_ptr->texcoord = { u, 1.0f - v };

	return true;
}

bool IntersectDisk(SRay* ray_ptr, SDisk* disk_ptr, SIntersectionData* intersection_data_ptr)
{
	F32 n_dot_d = NMath::Dot(disk_ptr->normal, ray_ptr->direction);

	if (n_dot_d >= 0.0f)
	{
		return false;
	}

	F32 t = NMath::Dot(disk_ptr->normal, disk_ptr->center - ray_ptr->origin) / n_dot_d;

	if (t < 0.0f)
	{
		return false;
	}

	F32x3 point_in_plane = ray_ptr->origin + ray_ptr->direction * t;
	F32 distance_sqr = NMath::DistanceSq(point_in_plane, disk_ptr->center);

	if (distance_sqr > disk_ptr->radius * disk_ptr->radius)
	{
		return false;
	}

	intersection_data_ptr->pos = point_in_plane;
	intersection_data_ptr->normal = disk_ptr->normal;

	return true;
}

bool IntersectSphere(SRay* ray_ptr, SSphere* sphere_ptr, SIntersectionData* intersection_data_ptr)
{

	return true;
}

SAabb ComputeBounds(const STriangle& triangle)
{
	SAabb aabb;

	F32x3 vtx0_pos = triangle.vtx[0].pos;
	F32x3 vtx1_pos = triangle.vtx[1].pos;
	F32x3 vtx2_pos = triangle.vtx[2].pos;

	aabb.bound_min.x = std::min({ vtx0_pos.x, vtx1_pos.x, vtx2_pos.x });
	aabb.bound_min.y = std::min({ vtx0_pos.y, vtx1_pos.y, vtx2_pos.y });
	aabb.bound_min.z = std::min({ vtx0_pos.z, vtx1_pos.z, vtx2_pos.z });

	aabb.bound_max.x = std::max({ vtx0_pos.x, vtx1_pos.x, vtx2_pos.x });
	aabb.bound_max.y = std::max({ vtx0_pos.y, vtx1_pos.y, vtx2_pos.y });
	aabb.bound_max.z = std::max({ vtx0_pos.z, vtx1_pos.z, vtx2_pos.z });

	return aabb;
}

SAabb Combine(const SAabb& aabb0, const SAabb& aabb1)
{
	SAabb combined_aabb;

	combined_aabb.bound_min.x = std::min(aabb0.bound_min.x, aabb1.bound_min.x);
	combined_aabb.bound_min.y = std::min(aabb0.bound_min.y, aabb1.bound_min.y);
	combined_aabb.bound_min.z = std::min(aabb0.bound_min.z, aabb1.bound_min.z);

	combined_aabb.bound_max.x = std::max(aabb0.bound_max.x, aabb1.bound_max.x);
	combined_aabb.bound_max.y = std::max(aabb0.bound_max.y, aabb1.bound_max.y);
	combined_aabb.bound_max.z = std::max(aabb0.bound_max.z, aabb1.bound_max.z);

	return combined_aabb;
}

SAabb Combine(const SAabb& aabb, const F32x3& point)
{
	SAabb combined_aabb;

	combined_aabb.bound_min.x = std::min(aabb.bound_min.x, point.x);
	combined_aabb.bound_min.y = std::min(aabb.bound_min.y, point.y);
	combined_aabb.bound_min.z = std::min(aabb.bound_min.z, point.z);

	combined_aabb.bound_max.x = std::max(aabb.bound_max.x, point.x);
	combined_aabb.bound_max.y = std::max(aabb.bound_max.y, point.y);
	combined_aabb.bound_max.z = std::max(aabb.bound_max.z, point.z);

	return combined_aabb;
}


F32 ComputeSurfaceArea(const SAabb& aabb)
{
	F32x3 v = aabb.bound_max - aabb.bound_min;

	return 2.0f * (v.x * v.y + v.x * v.z + v.y * v.z);
}
