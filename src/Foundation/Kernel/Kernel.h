#pragma once

typedef unsigned int8  U8;
typedef unsigned int16 U16;
typedef unsigned int32 U32;
typedef unsigned int64 U64;

typedef int8  S8;
typedef int16 S16;
typedef int32 S32;
typedef int64 S64;

typedef float F32;
typedef double F64;




struct SVector2f
{
	F32 x, y;
};

typedef SVector2f F32x2;
F32x2 F32x2Create(F32 x, F32 y);

struct SVector3f
{
	F32 x, y, z;
};

typedef SVector3f F32x3;
F32x3 F32x3Create(F32 x, F32 y, F32 z);

struct SVector4f
{
	F32 x, y, z, w;
};

typedef SVector4f F32x4;
F32x4 F32x4Create(F32 x, F32 y, F32 z, F32 w);

struct SMatrix4x4
{
	SVector4f r0;
	SVector4f r1;
	SVector4f r2;
	SVector4f r3;
};

/*
const F32 Math_PI = 3.14159265f;
const F32 Math_INV_PI = 1.0f / Math_PI;
*/

#define Math_PI 3.14159265f
#define Math_INV_PI 1.0f / Math_PI

inline F32 DegToRad(F32 a)
{
	return a * (Math_PI / 360.0f);
}

inline F32 RadToDeg(F32 a)
{
	return a * (360.0f / Math_PI);
}

inline F32 Clamp(F32 n, F32 minRange, F32 maxRange)
{
	if (n < minRange) return minRange;
	else if (n > maxRange) return maxRange;
	else return n;
}

inline F32 Saturate(F32 n)
{
	if (n < 0.0f) return 0.0f;
	else if (n > 1.0f) return 1.0f;
	else return n;
}

SVector2f operator+(const SVector2f& v0, const SVector2f& v1);
SVector2f operator-(const SVector2f& v0, const SVector2f& v1);
SVector2f operator*(const SVector2f& v0, const F32 f);
SVector2f operator*(const F32 f, const SVector2f& v0);
SVector2f operator/(const SVector2f& v0, const F32 f);

SVector3f operator+(const SVector3f& v0, const SVector3f& v1);
SVector3f operator-(const SVector3f& v0, const SVector3f& v1);
SVector3f operator*(const SVector3f& v0, const F32 f);
SVector3f operator*(const F32 f, const SVector3f& v0);
SVector3f operator/(const SVector3f& v0, const F32 f);

SVector4f operator+(const SVector4f& v0, const SVector4f& v1);
SVector4f operator-(const SVector4f& v0, const SVector4f& v1);
SVector4f operator*(const SVector4f& v0, const F32 f);
SVector4f operator*(const F32 f, const SVector4f& v0);
SVector4f operator/(const SVector4f& v0, const F32 f);

SVector4f operator*(const SMatrix4x4& mat0, const SVector4f& v0);

F32 Dot(const SVector3f& v0, const SVector3f& v1);
SVector3f Cross(const SVector3f& v0, const SVector3f& v1);
SVector3f Normalize(const SVector3f& v0);
F32 Length(const SVector3f& v0);
F32 Distance(const SVector3f& v0, const SVector3f& v1);
F32 DistanceSq(const SVector3f& v0, const SVector3f& v1);
F32 Dot(const SVector4f& v0, const SVector4f& v1);
SVector4f Normalize(const SVector4f& v0);
F32 Length(const SVector4f& v0);
SMatrix4x4 Transpose(const SMatrix4x4& mat0);
SVector3f TransformPos(const SMatrix4x4& m, const SVector3f& v);
SVector3f TransformDir(const SMatrix4x4& m, const SVector3f& v);




enum EIntersectionType
{
	INTERSECTION_TYPE_NULL,
	INTERSECTION_TYPE_BACKGROUND,
	INTERSECTION_TYPE_MESH_SURFACE,
	INTERSECTION_TYPE_LIGHT_SURFACE,
	INTERSECTION_TYPE_MESH_VOLUME,
	INTERSECTION_TYPE_LIGHT_VOLUME
};

struct SIntersectionData
{
	F32x3 pos;
	F32x3 normal;
	F32x3 tangent;
	F32x3 binormal;
	F32x2 texcoord;

	EIntersectionType intersection_type;
	U32 occluder_id;

};

struct SRay
{
	F32x3 origin;
	F32x3 direction;

	F32 t_min;
	F32 t_max;
};

struct SVertex
{
	F32x3 pos;
	F32x3 normal;
	F32x3 tangent;
	F32x3 binormal;
	F32x2 texcoord;
};

struct STriangle
{
	SVertex vtx[3];

	U32 mesh_id;
};

struct SQuad
{
	F32x3 base;
	F32x3 normal;
	F32x3 u;
	F32x3 v;
	F32 inv_length_sqr_u;
	F32 inv_length_sqr_v;
};

struct SDisk
{
	F32x3 center;
	F32x3 normal;
	F32 radius;
};

struct SSphere
{
	F32x3 center;
	F32 radius;
};

struct SAabb
{
	F32x3 bound_min;
	F32x3 bound_max;
};

F32 ComputeAreaDisk(const SDisk& disk);
F32 ComputeAreaSphere(const SSphere& sphere);
F32 ComputeAreaTriangle(const STriangle& triangle);
bool IntersectAabbFast2(const SRay& ray, const SAabb& aabb, const F32x3 inv_dir);
bool IntersectTriangle(SRay& ray, const STriangle& triangle, SIntersectionData& intersection_data);
bool IntersectQuad(const SRay& ray, const SQuad& quad, SIntersectionData& intersection_data);
bool IntersectDisk(const SRay& ray, const SDisk& disk, SIntersectionData& intersection_data);




struct SSampleInfo
{
	F32x3 direction;
	F32x4 throughput;
	F32 pdf;
};

F32x2 SampleDiskUniform(F32 u0, F32 u1);
F32x2 SampleDiskConcentric(F32 u0, F32 u1);
F32x3 SampleHemisphereUniform(F32 u0, F32 u1);
F32 PdfHemisphereUniform();
F32x3 SampleHemisphereCosineWeighted(F32 u0, F32 u1);
F32 PdfHemisphereCosineWeighted(F32 cos_theta);
F32x2 SampleTriangleUniform(F32 u0, F32 u1);
F32 PdfTriangleUniform(const STriangle& triangle);

F32 ComputeMisWeightBalanceHeuristic(U32 sample_count0, F32 pdf0, U32 sample_count1, F32 pdf1);
F32 ComputeMisWeightPowerHeuristic(U32 sample_count0, F32 pdf0, U32 sample_count1, F32 pdf1);




struct SShaderGlobals
{
	F32x3 pos;
	F32x3 normal;
	F32x2 texcoord;

	F32x3 wo;

	SMatrix4x4 tangent_to_world_mtx;

	U32 mesh_id;
	U32 material_id;
};




struct SMesh
{
	SMatrix4x4 obj_to_world_mtx;
	SMatrix4x4 world_to_obj_mtx;

	U32 material_id;
	U32 polygon_count;
};
