
#pragma once

#include <Foundation/Common.hpp>
#include <Foundation/Math.hpp>

typedef struct SMesh
{
	SMatrix4x4 obj_to_world_mtx;
	SMatrix4x4 world_to_obj_mtx;

	U32 material_id;
	U32 polygon_count;

} SMesh;
