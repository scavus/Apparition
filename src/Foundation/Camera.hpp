
#pragma once

#include <Foundation/Common.hpp>
#include <Foundation/Math.hpp>

typedef enum ECameraType
{
	PINHOLE,
	THINLENS

} ECameraType;

typedef struct SPinholeCameraProperties
{
	F32 fov;

} SPinholeCameraProperties;

typedef struct SThinLensCameraProperties
{
	F32 aperture_size;
	F32 focal_length;
	F32 fov;

} SThinLensCameraProperties;

typedef struct SCamera
{
	ECameraType type;

	union UProperties
	{
		SPinholeCameraProperties pinhole;
		SThinLensCameraProperties thinlens;

	} properties;

	SMatrix4x4 camera_to_world_mtx;

} SCamera;
