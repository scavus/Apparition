
#pragma once

namespace NIntegratorPluginRegistry
{
	void RegisterPluginFactory();
	void UnregisterPluginFactory();
	IIntegratorPluginFactory* GetPluginFactory();
};

namespace NIntersectorPluginRegistry
{
	void RegisterPluginFactory();
	void UnregisterPluginFactory();
	IIntersectorPluginFactory* GetPluginFactory();
};

namespace NAcceleratorPluginRegistry
{
	void RegisterPluginFactory();
	void UnregisterPluginFactory();
	IAcceleratorPluginFactory* GetPluginFactory();
}
