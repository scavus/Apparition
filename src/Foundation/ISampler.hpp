
#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/Math.hpp"

class ISampler
{
	public:
		virtual ISampler* GetSubsampler(U32 idx) = 0;
		virtual void InitSequence(U32 px, U32 sample_idx) = 0;
		virtual U32 SampleU32() = 0;
		virtual F32 SampleF32() = 0;
		virtual F32x2 SampleF32x2() = 0;

	protected:

	private:
};
