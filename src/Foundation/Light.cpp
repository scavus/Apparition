
#include "Light.hpp"

F32 Luminance(F32x4 color)
{
	return 0.2126f * color.x + 0.7152f * color.y + 0.0722f * color.z;
}

void SampleLightSphere(const SAreaLight& light, const SShaderGlobals& sg, const F32 u0, const F32 u1, SSampleInfo& light_sample)
{


}

void SampleLightQuad(const SAreaLight& light, const SShaderGlobals& sg, const F32 u0, const F32 u1, SSampleInfo& light_sample)
{
	F32x3 sample_pos;
	F32x3 sample_dir;

	SQuad quad = light.shape.quad;

	sample_pos = { 2 * u0 - 1.0f, 2 * u1 - 1.0f, 0.0f };

	sample_pos = NMath::TransformPos(light.obj_to_world_mtx, sample_pos);
	sample_dir = NMath::Normalize(sample_pos - sg.pos);

	F32 distance_sqr = NMath::DistanceSq(sg.pos, sample_pos);

	F32 cos_theta_prime = abs(NMath::Dot(quad.normal, -1.0f * sample_dir));

	F32 sx = NMath::Length(quad.u);
	F32 sy = NMath::Length(quad.v);

	light_sample.direction = sample_dir;
	light_sample.pdf = distance_sqr / (sx * sy * cos_theta_prime);
}

void SampleLightDisk(const SAreaLight& light, const SShaderGlobals& sg, const F32 u0, const F32 u1, SSampleInfo& light_sample)
{
	F32x3 sample_pos;
	F32x3 sample_dir;

	SDisk disk = light.shape.disk;
		
	F32x2 v = SampleDiskConcentric(u0, u1);
	sample_pos = { v.x, v.y, 0.0f };

	sample_pos = NMath::TransformPos(light.obj_to_world_mtx, sample_pos);
	sample_dir = NMath::Normalize(sample_pos - sg.pos);

	F32 distance_sqr = NMath::DistanceSq(sg.pos, sample_pos);

	F32 cos_theta_prime = abs(NMath::Dot(disk.normal, -1.0f * sample_dir));

	light_sample.direction = sample_dir;
	light_sample.pdf = distance_sqr / (ComputeAreaDisk(&disk) * cos_theta_prime);
}

/*
void SampleSelectedLight(
	U32 light_id,
	F32x2 uv,
	SShaderGlobals* shading_point_info_ptr,
	SAreaLight* area_lights,
	SSampleInfo* light_sample_ptr)
{

	if (scene_metadata_ptr->env_light_count > 0
		&&
		light_id == (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count - 1))
	{
		SEnvLight env_light = *g_env_light;
		SampleEnvLight(&env_light, g_env_light_conditional_cdfs, g_env_light_marginal_cdf, uv.x, uv.y, light_sample_ptr);
		return;
	}

	SAreaLight light = area_lights[light_id];

	switch (light.type)
	{
		case LIGHT_TYPE_QUAD:
		{
			SampleLightQuad(&light, shading_point_info_ptr, uv.x, uv.y, light_sample_ptr);
			break;
		}
		case LIGHT_TYPE_DISK:
		{
			SampleLightDisk(&light, shading_point_info_ptr, uv.x, uv.y, light_sample_ptr);
			break;
		}
		case LIGHT_TYPE_SPHERE:
		{
			break;
		}
		case LIGHT_TYPE_TUBE:
		{
			break;
		}
	}
}
void SampleLight(SRandomNumberGenerator* rng_ptr, SShaderGlobals* shading_point_info_ptr, SSceneMetadata* scene_metadata_ptr,
	WR_GMEM_PARAM SAreaLight* area_lights,
	WR_GMEM_PARAM SEnvLight* g_env_light,
	WR_READONLY_TEXTURE2D g_env_map,
	WR_GMEM_PARAM F32* g_env_light_conditional_cdfs,
	WR_GMEM_PARAM F32* g_env_light_marginal_cdf,
	SSampleInfo* light_sample_ptr)
{
	U32 selected_light_id = SampleLightIdUniform(rng_ptr, scene_metadata_ptr);

	if (scene_metadata_ptr->env_light_count > 0
		&&
		selected_light_id == (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count - 1))
	{
		SEnvLight env_light = *g_env_light;
		F32 u0 = GenerateF32(rng_ptr);
		F32 u1 = GenerateF32(rng_ptr);
		SampleEnvLight(&env_light, g_env_light_conditional_cdfs, g_env_light_marginal_cdf, u0, u1, light_sample_ptr);
		light_sample_ptr->pdf *= 1.0f / (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count);
		return;
	}

	SAreaLight light = area_lights[selected_light_id];

	switch (light.type)
	{
		case LIGHT_TYPE_QUAD:
		{
			F32 u0 = GenerateF32(rng_ptr);
			F32 u1 = GenerateF32(rng_ptr);
			SampleLightQuad(&light, shading_point_info_ptr, u0, u1, light_sample_ptr);
			light_sample_ptr->pdf *= 1.0f / (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count);
			break;
		}

		case LIGHT_TYPE_DISK:
		{
			F32 u0 = GenerateF32(rng_ptr);
			F32 u1 = GenerateF32(rng_ptr);
			SampleLightDisk(&light, shading_point_info_ptr, u0, u1, light_sample_ptr);
			light_sample_ptr->pdf *= 1.0f / (scene_metadata_ptr->area_light_count + scene_metadata_ptr->env_light_count);
			break;
		}

		case LIGHT_TYPE_SPHERE:
		{

			break;
		}

		case LIGHT_TYPE_TUBE:
		{

			break;
		}
	}
}
*/
