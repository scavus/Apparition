
#include <Foundation/Sampling.hpp>

// NOTE: From paper: http://graphics.pixar.com/library/MultiJitteredSampling/
/*
void SampleCorrelatedMultiJittered2d(SRandomNumberGenerator* rng_ptr, U32 m, U32 n, WR_GMEM_PARAM F32x2* samples)
{
	U32x2 idx;

	for (idx.y = 0; idx.y < n; idx.y++)
	{
		for (idx.x = 0; idx.x < m; idx.x++)
		{
			samples[idx.y * m + idx.x].x = ((F32)(idx.x) + ((F32)(idx.y) + GenerateF32(rng_ptr)) / (F32)(n)) / (F32)(m);
			samples[idx.y * m + idx.x].y = ((F32)(idx.y) + ((F32)(idx.x) + GenerateF32(rng_ptr)) / (F32)(m)) / (F32)(n);
		}
	}

	// Shuffle x coords along columns
	for (idx.y = 0; idx.y < n; idx.y++)
	{
		U32 offset = (U32)((F32)(idx.y) + GenerateF32(rng_ptr) * (F32)(n - idx.y));

		for (idx.x = 0; idx.x < m; idx.x++)
		{
			F32 temp = samples[idx.y * m + idx.x].x;
			samples[idx.y * m + idx.x].x = samples[offset * m + idx.x].x;
			samples[offset * m + idx.x].x = temp;
		}
	}

	// Shuffle y coords along rows
	for (idx.x = 0; idx.x < m; idx.x++)
	{
		U32 offset = (U32)((F32)(idx.x) + GenerateF32(rng_ptr) * (F32)(m - idx.x));

		for (idx.y = 0; idx.y < n; idx.y++)
		{
			F32 temp = samples[idx.y * m + idx.x].y;
			samples[idx.y * m + idx.x].y = samples[idx.y * m + offset].y;
			samples[idx.y * m + offset].y = temp;
		}
	}
}
*/

F32x2 SampleDiskUniform(F32 u0, F32 u1)
{
	F32 a = sqrt(u0);
	F32 b = 2.0f * NMath::PI * u1;

	F32x2 sample_coord;
	sample_coord.x = a * cos(b);
	sample_coord.y = a * sin(b);

	return sample_coord;
}

F32x2 SampleDiskConcentric(F32 u0, F32 u1)
{
	F32 a;
	F32 b;

	F32x2 p;
	p.x = 2.0f * u0 - 1.0f;
	p.y = 2.0f * u1 - 1.0f;

	if (p.x > -p.y)
	{
		if (p.x > p.y)
		{
			a = p.x;
			b = (NMath::PI / 4.0f) * (p.y / p.x);
		}
		else
		{
			a = p.y;
			b = (NMath::PI / 4.0f) * (2.0f - (p.x / p.y));
		}
	}
	else
	{
		if (p.x < p.y)
		{
			a = -p.x;
			b = (NMath::PI / 4.0f) * (4.0f + (p.y / p.x));
		}
		else
		{
			a = -p.y;

			if (p.y != 0.0f)
			{
				b = (NMath::PI / 4.0f) * (6.0f - (p.x / p.y));
			}
			else
			{
				b = 0.0f;
			}
		}
	}

	F32x2 sample_coord;
	sample_coord.x = a * cos(b);
	sample_coord.y = a * sin(b);

	return sample_coord;
}

F32x3 SampleHemisphereUniform(F32 u0, F32 u1)
{
	F32 m = std::max(0.0f, 1.0f - (u0 * u0));
	F32 a = sqrt(m);
	F32 b = 2.0f * NMath::PI * u1;

	F32x3 sample_coord;
	sample_coord.x = a * cos(b);
	sample_coord.y = a * sin(b);
	sample_coord.z = u0;

	return sample_coord;
}

F32 PdfHemisphereUniform()
{
	return 1.0f / (2.0f * NMath::PI);
}

F32x3 SampleHemisphereCosineWeighted(F32 u0, F32 u1)
{
	F32x2 disk_sample_coord;
	disk_sample_coord = SampleDiskConcentric(u0, u1);

	F32x3 sample_coord;
	sample_coord.x = disk_sample_coord.x;
	sample_coord.y = disk_sample_coord.y;
	sample_coord.z = sqrt(1.0f - (disk_sample_coord.x * disk_sample_coord.x) - (disk_sample_coord.y * disk_sample_coord.y));

	return sample_coord;
}

F32 PdfHemisphereCosineWeighted(F32 cos_theta)
{
	return cos_theta * NMath::INV_PI;
}

F32x2 SampleTriangleUniform(F32 u0, F32 u1)
{
	F32 a = sqrt(u0);

	F32x2 sample_coord;
	sample_coord.x = 1.0f - a;
	sample_coord.y = u1 * a;

	return sample_coord;
}

// TODO: multiply with uv --> solidangle conversion factor
F32 PdfTriangleUniform(STriangle* triangle_ptr)
{
	return 1.0f / ComputeAreaTriangle(triangle_ptr);
}

F32 ComputeMisWeightBalanceHeuristic(U32 sample_count0, F32 pdf0, U32 sample_count1, F32 pdf1)
{
	F32 f = (F32)(sample_count0) * pdf0;
	F32 g = (F32)(sample_count1) * pdf1;

	return f / (f + g);
}

F32 ComputeMisWeightPowerHeuristic(U32 sample_count0, F32 pdf0, U32 sample_count1, F32 pdf1)
{
	F32 f = (F32)(sample_count0) * pdf0;
	F32 g = (F32)(sample_count1) * pdf1;

	return f * f / (f * f + g * g);
}
