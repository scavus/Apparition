
#pragma once

#include "Foundation/Common.hpp"

struct SRenderSettings
{
	U32 render_width;
	U32 render_height;
	U32 tile_width;
	U32 tile_height;
	U32 thread_count;
};
