#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/Shading.hpp"
#include "Foundation/Random.hpp"
#include "Foundation/Sampling.hpp"

class IShaderNode
{
	public:
		virtual void Eval(const SShaderGlobals& sg, SRandomNumberGenerator* rng_ptr, SSampleInfo& sample_info) = 0;
		virtual void Sample(const SShaderGlobals& sg, SRandomNumberGenerator* rng_ptr, SSampleInfo& sample_info) = 0;

	protected:

	private:
};
