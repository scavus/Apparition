
#pragma once

#include <algorithm>

#include "Foundation/Common.hpp"

struct SVector2f
{
	F32 x, y;
};

using F32x2 = SVector2f;

SVector2f operator+(const SVector2f& v0, const SVector2f& v1);
SVector2f operator-(const SVector2f& v0, const SVector2f& v1);
SVector2f operator*(const SVector2f& v0, const F32 f);
SVector2f operator*(const F32 f, const SVector2f& v0);
SVector2f operator/(const SVector2f& v0, const F32 f);


struct SVector3f
{
	F32 x, y, z;
};

using F32x3 = SVector3f;

SVector3f operator+(const SVector3f& v0, const SVector3f& v1);
SVector3f operator-(const SVector3f& v0, const SVector3f& v1);
SVector3f operator*(const SVector3f& v0, const F32 f);
SVector3f operator*(const F32 f, const SVector3f& v0);
SVector3f operator/(const SVector3f& v0, const F32 f);

struct SVector4f
{
	F32 x, y, z, w;
};

using F32x4 = SVector4f;

SVector4f operator+(const SVector4f& v0, const SVector4f& v1);
SVector4f operator-(const SVector4f& v0, const SVector4f& v1);
SVector4f operator*(const SVector4f& v0, const SVector4f& v1);
SVector4f operator*(const SVector4f& v0, const F32 f);
SVector4f operator*(const F32 f, const SVector4f& v0);
SVector4f operator/(const SVector4f& v0, const F32 f);

struct SMatrix3x3
{
	union
	{
		struct
		{
			SVector3f r0;
			SVector3f r1;
			SVector3f r2;
		};

		struct
		{
			F32 m00, m01, m02;
			F32 m10, m11, m12;
			F32 m20, m21, m22;
		};
	};
};

struct SMatrix4x4
{
	union
	{
		struct
		{
			SVector4f r0;
			SVector4f r1;
			SVector4f r2;
			SVector4f r3;
		};

		struct
		{
			F32 m00, m01, m02, m03;
			F32 m10, m11, m12, m13;
			F32 m20, m21, m22, m23;
			F32 m30, m31, m32, m33;
		};
	};
};

SMatrix4x4 operator*(const SMatrix4x4& mat0, const SMatrix4x4& mat1);
SVector4f operator*(const SMatrix4x4& mat0, const SVector4f& v0);

namespace NMath
{
	const F32 PI = 3.14159265f;
	const F32 INV_PI = 1.0f / PI;

	inline F32 DegToRad(F32 a)
	{
		return a * (PI / 360.0f);
	}

	inline F32 RadToDeg(F32 a)
	{
		return a * (360.0f / PI);
	}

	inline F32 Clamp(F32 n, F32 minRange, F32 maxRange)
	{
		if (n < minRange) return minRange;
		else if (n > maxRange) return maxRange;
		else return n;
	}

	inline F32 Saturate(F32 n)
	{
		if (n < 0.0f) return 0.0f;
		else if (n > 1.0f) return 1.0f;
		else return n;
	}

	inline F32 Sqr(F32 n)
	{
		return n * n;
	}

	inline F32 Rsqrt(F32 n)
	{
		return powf(n, -0.5f);
	}

	inline F32x3 SphericalToCartesian(F32 r, F32 theta, F32 phi)
	{
		F32 sin_theta = sin(theta);
		F32 sin_phi = sin(phi);
		F32 cos_theta = cos(theta);
		F32 cos_phi = cos(phi);

		F32x3 v;
		v.x = r * sin_theta * cos_phi;
		v.y = r * sin_theta * sin_phi;
		v.z = r * cos_theta;
		return v;
	}

	inline F32x3 SphericalToCartesianUnit(F32 theta, F32 phi)
	{
		F32 sin_theta = sin(theta);
		F32 sin_phi = sin(phi);
		F32 cos_theta = cos(theta);
		F32 cos_phi = cos(phi);

		F32x3 v;
		v.x = sin_theta * cos_phi;
		v.y = sin_theta * sin_phi;
		v.z = cos_theta;
		return v;
	}

	inline F32x2 CartesianToSphericalUnit(F32x3 v)
	{
		F32x2 sc;
		sc.x = acos(v.z);
		sc.y = atan(v.y / v.x);

		return sc;
	}

	inline F32 CartesianToSphericalThetaUnit(F32x3 v)
	{
		return acos(NMath::Clamp(v.z, -1.0f, 1.0f));
	}

	inline F32 CartesianToSphericalPhiUnit(F32x3 v)
	{
		F32 p = atan2(v.y, v.x);
		return (p < 0.0f) ? p + 2.0f * PI : p;
	}

	SVector2f MakeVec2(F32 x, F32 y);
	SVector3f MakeVec3(F32 x, F32 y, F32 z);
	F32 GetVec3Elem(const SVector3f v, S32 i);

	F32 Dot(const SVector3f& v0, const SVector3f& v1);
	SVector3f Cross(const SVector3f& v0, const SVector3f& v1);
	SVector3f Normalize(const SVector3f& v0);
	F32 Length(const SVector3f& v0);
	F32 Distance(const SVector3f& v0, const SVector3f& v1);
	F32 DistanceSq(const SVector3f& v0, const SVector3f& v1);

	inline F32x3 Reflect(F32x3 i, F32x3 n)
	{
		return 2.0f * NMath::Dot(n, i) * n - i;
	}

	SVector4f MakeVec4(F32 x, F32 y, F32 z, F32 w);

	typedef SVector4f ColorRGBA;

	F32 Dot(const SVector4f& v0, const SVector4f& v1);

	SVector4f Normalize(const SVector4f& v0);

	F32 Length(const SVector4f& v0);

	SMatrix4x4 MakeMatrix4x4(const SVector4f& row0, const SVector4f& row1, const SVector4f& row2, const SVector4f& row3);

	// SVector4f Transform();
	SVector3f TransformPos(const SMatrix4x4& m, const SVector3f& v);;
	SVector3f TransformDir(const SMatrix4x4& m, const SVector3f& v);;

	SMatrix4x4 Transpose(const SMatrix4x4& mat0);
	SMatrix4x4 Translation(F32 Tx, F32 Ty, F32 Tz);
	SMatrix4x4 Scale(F32 Sx, F32 Sy, F32 Sz);
	SMatrix4x4 RotationX(F32 deg);
	SMatrix4x4 RotationY(F32 deg);
	SMatrix4x4 RotationZ(F32 deg);
	SMatrix4x4 Rotation(const SVector3f& v0, F32 deg);
	SMatrix4x4 Perspective_GL(F32 fovDeg, F32 aspect, F32 zNear, F32 zFar);
	SMatrix4x4 PerspectiveFovMatrix(F32 FovYDeg, F32 AspectRatio, F32 ZNear, F32 ZFar);
	SMatrix4x4 PerspectiveFovMatrixLH(F32 FovYDeg, F32 AspectRatio, F32 ZNear, F32 ZFar);
	SMatrix4x4 LookAtMatrix(const SVector3f& Eye, const SVector3f &Target, const SVector3f& Up);
	SMatrix4x4 LookAtMatrixLH(const SVector3f& Eye, const SVector3f &Target, const SVector3f& Up);
};