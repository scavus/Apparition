
#include "WorkManager.hpp"

namespace NWorkManager
{
	void DistributeWork(SRenderSettings& render_settings, SWorkQueue& work_queue)
	{
		U32 x = render_settings.render_width / render_settings.tile_width;
		U32 y = render_settings.render_height / render_settings.tile_height;

		U32 m = render_settings.render_width % render_settings.tile_width;
		U32 n = render_settings.render_height % render_settings.tile_height;

		U32 total_tile_count = x * y;

		if (m > 0 && n > 0)
		{
			total_tile_count += x + y + 1;
		}
		else if (m == 0 && n > 0)
		{
			total_tile_count += x;
		}
		else if (m > 0 && n == 0)
		{
			total_tile_count += y;
		}

		U32 tile_offset_x = 0;
		U32 tile_offset_y = 0;

		if (x * y > 0)
		{
			for (U32 tile_idx_y = 0; tile_idx_y < y; tile_idx_y++)
			{
				for (U32 tile_idx_x = 0; tile_idx_x < x; tile_idx_x++)
				{
					STileInfo tile_info;
					tile_info.width = render_settings.tile_width;
					tile_info.height = render_settings.tile_height;
					tile_info.offset_x = tile_offset_x;
					tile_info.offset_y = tile_offset_y;
					tile_info.finished = false;
					work_queue.tiles.push(tile_info);

					tile_offset_x += render_settings.tile_width;
				}

				tile_offset_x = 0;
				tile_offset_y += render_settings.tile_height;
			}
		}

		tile_offset_x = 0;
		tile_offset_y = render_settings.render_height - n;

		if (n > 0)
		{
			for (U32 tile_idx = 0; tile_idx < x; tile_idx++)
			{
				STileInfo tile_info;
				tile_info.width = render_settings.tile_width;
				tile_info.height = n;
				tile_info.offset_x = tile_offset_x;
				tile_info.offset_y = tile_offset_y;
				tile_info.finished = false;
				work_queue.tiles.push(tile_info);

				tile_offset_x += render_settings.tile_width;
			}
		}

		tile_offset_x = render_settings.render_width - m;
		tile_offset_y = 0;

		if (m > 0)
		{
			for (U32 tile_idx = 0; tile_idx < y; tile_idx++)
			{
				STileInfo tile_info;
				tile_info.width = m;
				tile_info.height = render_settings.tile_height;
				tile_info.offset_x = tile_offset_x;
				tile_info.offset_y = tile_offset_y;
				tile_info.finished = false;
				work_queue.tiles.push(tile_info);

				tile_offset_y += render_settings.tile_height;
			}
		}

		tile_offset_x = render_settings.render_width - m;
		tile_offset_y = render_settings.render_height - n;

		if (m > 0 && n > 0)
		{
			STileInfo tile_info;
			tile_info.width = m;
			tile_info.height = n;
			tile_info.offset_x = tile_offset_x;
			tile_info.offset_y = tile_offset_y;
			tile_info.finished = false;
			work_queue.tiles.push(tile_info);
		}
	}

	void DistributeWorkLowGranularity(SRenderSettings& render_settings, SWorkQueue& work_queue)
	{
		U32 thread_count = render_settings.thread_count;
		U32 tile_step = render_settings.render_width / thread_count;

		for (U32 i = 0, offset = 0; i < thread_count; i++, offset += tile_step)
		{
			STileInfo tile_info;

			/* Handle last tile in case of tile_step with remainder */
			if (i == thread_count - 1)
			{
				tile_info.width = render_settings.render_width - offset;
			}
			else
			{
				tile_info.width = tile_step;
			}

			tile_info.height = render_settings.render_height;
			tile_info.offset_x = offset;
			tile_info.offset_y = 0;
			work_queue.tiles.push(tile_info);
		}
	}
}
