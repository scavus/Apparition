
#pragma once

#include <Foundation/Common.hpp>
#include <Foundation/Math.hpp>

typedef enum EIntersectionType
{
	INTERSECTION_TYPE_NULL,
	INTERSECTION_TYPE_BACKGROUND,
	INTERSECTION_TYPE_MESH_SURFACE,
	INTERSECTION_TYPE_LIGHT_SURFACE,
	INTERSECTION_TYPE_MESH_VOLUME,
	INTERSECTION_TYPE_LIGHT_VOLUME

} EIntersectionType;

typedef struct SIntersectionData
{
	F32x3 pos;
	F32x3 normal;
	F32x3 tangent;
	F32x3 binormal;
	F32x2 texcoord;

	EIntersectionType intersection_type;
	U32 occluder_id;

} SIntersectionData;

typedef struct SRay
{
	F32x3 origin;
	F32x3 direction;

	F32 t_min;
	F32 t_max;

} SRay;

typedef struct SVertex
{
	F32x3 pos;
	F32x3 normal;
	F32x3 tangent;
	F32x3 binormal;
	F32x2 texcoord;

} SVertex;

typedef struct STriangle
{
	SVertex vtx[3];

	U32 mesh_id;

} STriangle;

typedef struct SQuad
{
	F32x3 base;
	F32x3 normal;
	F32x3 u;
	F32x3 v;
	F32 inv_length_sqr_u;
	F32 inv_length_sqr_v;

} SQuad;

typedef struct SDisk
{
	F32x3 center;
	F32x3 normal;
	F32 radius;

} SDisk;

typedef struct SSphere
{
	F32x3 center;
	F32 radius;

} SSphere;

typedef struct SAabb
{
	F32x3 bound_min;
	F32x3 bound_max;

} SAabb;

F32 ComputeAreaDisk(const SDisk* disk_ptr);
F32 ComputeAreaSphere(const SSphere* sphere_ptr);
F32 ComputeAreaTriangle(const STriangle* triangle_ptr);

bool IntersectAabbFast2(SRay* ray_ptr, SAabb* aabb_ptr, F32x3 inv_dir);
bool IntersectTriangle(SRay* ray_ptr, STriangle* triangle_ptr, SIntersectionData* intersection_data_ptr);
bool IntersectQuad(SRay* ray_ptr, SQuad* quad_ptr, SIntersectionData* intersection_data_ptr);
bool IntersectDisk(SRay* ray_ptr, SDisk* disk_ptr, SIntersectionData* intersection_data_ptr);
bool IntersectSphere(SRay* ray_ptr, SSphere* sphere_ptr, SIntersectionData* intersection_data_ptr);

SAabb ComputeBounds(const STriangle& triangle);
F32 ComputeSurfaceArea(const SAabb& aabb);
SAabb Combine(const SAabb& aabb0, const SAabb& aabb1);
SAabb Combine(const SAabb& aabb, const F32x3& point);
