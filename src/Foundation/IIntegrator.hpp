
#pragma once

struct SWorkQueue;
struct SSceneContext;
struct SRenderSettings;
class CFramebuffer;
class CSceneIntersector;


class IIntegrator
{
	public:
		virtual void Init() = 0;
		virtual void Update() = 0;
		virtual void Execute(SWorkQueue* work_queue_ptr, SSceneContext* scene_ctx_ptr, CFramebuffer* framebuffer_ptr) = 0;

	protected:

	private:

};

class IIntegratorFactory
{
	public:
		virtual IIntegrator* Create(SRenderSettings* render_settings, void* integrator_settings, CSceneIntersector& intersector) const = 0;

	protected:

	private:
};