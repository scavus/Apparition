
#pragma once

#include <atomic>

#include "Foundation/Common.hpp"
#include "Foundation/Math.hpp"

class CFramebuffer
{
	public:
		CFramebuffer(U32 width, U32 height)
		{
			m_width = width;
			m_height = height;
			m_color_buffer = new F32x4[m_width * m_height]();
			m_rw = 0;
		};

		~CFramebuffer()
		{
			delete[] m_color_buffer;
		};

		inline U32 GetWidth() { return m_width; };
		inline U32 GetHeight() { return m_height; };

		inline void SetColorBufferPx(U32 x, U32 y, const F32x4& color)
		{
			m_color_buffer[m_width * y + x] = color;
		};

		inline F32x4 GetColorBufferPx(U32 x, U32 y)
		{
			return m_color_buffer[m_height * y + x];
		};

		inline F32x4* GetColorBuffer()
		{
			return m_color_buffer;
		};

		/**
		* Lock the buffer for writing
		* \brief Locks the buffer so that only writers can access it
		*/
		inline void LockW()
		{
			while (m_rw > 0) {}
			m_rw--;
		};

		/**
		* Unlock the buffer after writing
		* \brief
		*/
		inline void UnlockW()
		{
			m_rw++;
		};

		/**
		* Lock the buffer for reading
		*/
		inline void LockR()
		{
			while (m_rw < 0) {}
			m_rw++;
		}

		/**
		* Unlock the buffer after reading
		*/
		inline void UnlockR()
		{
			m_rw--;
		}

	protected:

	private:
		U32 m_width;
		U32 m_height;

		F32x4* m_color_buffer;

		std::atomic_int m_rw;
};
