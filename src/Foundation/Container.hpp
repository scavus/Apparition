
#pragma once

#include <vector>
#include <unordered_map>

struct SResourceId
{
	U32 idx;
};

template<typename T>
struct TTrackedArray
{
	SResourceId Append(const T& resource);
	void Delete(const SResourceId resource_id);
	void Clear();

	T& operator[](const SResourceId resource_id);
	const T& operator[](const SResourceId resource_id) const;

	std::vector<T> data;
	std::vector<std::unordered_map<SResourceId*, SResourceId*>> trackers;

	bool update_flag;
};

template<typename T>
void TTrackedArray<T>::Clear()
{
	data.clear();
	// TODO: Broadcast to trackers
	trackers.clear();
}

template<typename T>
inline T& TTrackedArray<T>::operator[](const SResourceId resource_id)
{
	return data[resource_id.idx];
}

template<typename T>
inline const T& TTrackedArray<T>::operator[](const SResourceId resource_id) const
{
	return data[resource_id.idx];
}

template<typename T>
SResourceId TTrackedArray<T>::Append(const T& resource)
{
	SResourceId id;
	data.push_back(resource);
	// gens.push_back(0);

	// std::unordered_map<&TResourceId<Tag>, &TResourceId<Tag> tracker_map;
	trackers.push_back(std::unordered_map<SResourceId*, SResourceId*>());
	// trackers.reserve(data.size());

	id.idx = data.size() - 1;
	return id;
}

template<typename T>
void TTrackedArray<T>::Delete(const SResourceId resource_id)
{
	std::swap(data[resource_id.idx], data.back());
	std::swap(trackers[resource_id.idx], trackers.back());

	for (auto tracker_map : trackers[resource_id.idx])
	{
		*tracker_map.second = resource_id;
	}

	for (auto tracker_map : trackers.back())
	{
		*tracker_map.second = { 0 };
	}

	data.pop_back();
	trackers.pop_back();
}
