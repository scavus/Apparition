
#pragma once

#include <Foundation/Common.hpp>
#include <Foundation/Math.hpp>
#include <Foundation/Geometry.hpp>

struct SSampleInfo
{
	F32x3 direction;
	F32x4 throughput;
	F32 pdf;
};

F32x2 SampleDiskUniform(F32 u0, F32 u1);
F32x2 SampleDiskConcentric(F32 u0, F32 u1);

F32x3 SampleHemisphereUniform(F32 u0, F32 u1);
F32x3 SampleHemisphereCosineWeighted(F32 u0, F32 u1);
F32 PdfHemisphereCosineWeighted(F32 cos_theta);

F32x2 SampleTriangleUniform(F32 u0, F32 u1);
F32 PdfTriangleUniform(STriangle* triangle_ptr);
