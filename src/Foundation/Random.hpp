
#pragma once

#include <limits>

#include <Foundation/Common.hpp>

typedef struct SRngXorShift
{
	U32 state;
} SRngXorShift;

typedef struct SRngLcg
{
	U32 state;
} SRngLcg;

typedef SRngXorShift SRandomNumberGenerator;

inline U32 HashFnWang(U32 seed)
{
	seed = (seed ^ 61) ^ (seed >> 16);
	seed *= 9;
	seed = seed ^ (seed >> 4);
	seed *= 0x27d4eb2d;
	seed = seed ^ (seed >> 15);
	return seed;
}

inline void SeedRng(SRngXorShift* rng_ptr, U32 seed)
{
	rng_ptr->state = HashFnWang(seed);
}

inline U32 GenerateU32(SRngXorShift* rng_ptr)
{
	U32 state = rng_ptr->state;
	state ^= (state << 13);
	state ^= (state >> 17);
	state ^= (state << 5);
	rng_ptr->state = state;
	return state;
}

inline F32 GenerateF32(SRngXorShift* rng_ptr)
{
	U32 state = rng_ptr->state;
	state ^= (state << 13);
	state ^= (state >> 17);
	state ^= (state << 5);
	rng_ptr->state = state;
	return (float)state / (float)(std::numeric_limits<U32>::max());
}
