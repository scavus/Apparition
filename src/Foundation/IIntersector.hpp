
#pragma once

#include <vector>

#include "Foundation/Common.hpp"
#include "Foundation/Geometry.hpp"

struct SSceneContext;

struct SIntersectorQueryData
{
	std::vector<SRay>* ray_buffer;
	std::vector<U32>* ray_stream;
	std::vector<SIntersectionData>* hit_info_buffer;
};

class IIntersector
{
	public:
		virtual void Update() = 0;
		virtual void QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data) = 0;
		virtual void QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info) = 0;
		virtual void QueryOcclusion() = 0;

	protected:

	private:

};
