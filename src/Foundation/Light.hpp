
#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/Math.hpp"
#include "Foundation/Geometry.hpp"
#include "Foundation/Shading.hpp"
#include "Foundation/Sampling.hpp"
#include "Foundation/Random.hpp"

enum ELightType
{
	LIGHT_TYPE_QUAD,
	LIGHT_TYPE_DISK,
	LIGHT_TYPE_SPHERE,
	LIGHT_TYPE_TUBE

};

struct SAreaLight
{
	ELightType type;

	SMatrix4x4 obj_to_world_mtx;
	SMatrix4x4 world_to_obj_mtx;

	F32 intensity;
	F32 exposure;
	F32x4 color;

	union UShape
	{
		SQuad quad;
		SDisk disk;
		SSphere sphere;

	} shape;

};

struct SDirectionalLight
{
	F32x3 direction;

	F32x4 color;
	F32 intensity;
	F32 angle;

};

struct SEnvLight
{
	SMatrix4x4 obj_to_world_mtx;
	SMatrix4x4 world_to_obj_mtx;

	U32 width;
	U32 height;

};

F32 Luminance(F32x4 color);
void SampleLightSphere(const SAreaLight& light, const SShaderGlobals& sg, const F32 u0, const F32 u1, SSampleInfo& light_sample);
void SampleLightQuad(const SAreaLight& light, const SShaderGlobals& sg, const F32 u0, const F32 u1, SSampleInfo& light_sample);
void SampleLightDisk(const SAreaLight& light, const SShaderGlobals& sg, const F32 u0, const F32 u1, SSampleInfo& light_sample);
