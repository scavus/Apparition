
#pragma once

#include <thread>

#include "Foundation/Settings.hpp"
#include "Foundation/Framebuffer.hpp"
#include "Foundation/IIntegrator.hpp"
#include "Foundation/WorkManager.hpp"
#include "Scene/SceneContext.hpp"
#include "Scene/SceneIntersector.hpp"

class CRenderEngine
{
	public:
		CRenderEngine(SRenderSettings& settings, void* integrator_settings, CSceneIntersector& intersector, const IIntegratorFactory* integrator_factory_ptr)
		{
			m_framebuffer_ptr = new CFramebuffer(settings.render_width, settings.render_height);
			m_settings = settings;
			m_integrator_settings = integrator_settings;
			m_integrator_factory_ptr = integrator_factory_ptr;
			m_intersector = intersector;
		}

		~CRenderEngine()
		{
			for (IIntegrator* integrator_ptr : m_integrators)
			{
				delete integrator_ptr;
			}

			m_integrators.clear();

			delete m_framebuffer_ptr;
		};

		void Init();
		void Update();
		void Render(SSceneContext* scene_ctx_ptr);
		CFramebuffer* GetFramebufferPtr() { return m_framebuffer_ptr; };
		void ReadTileCache(std::vector<STileInfo>& tile_cache) 
		{
			m_work_queue.mut.lock();

			// tile_cache = m_work_queue.tile_cache;
			for (auto tile : m_work_queue.tile_cache)
			{
				tile_cache.push_back(tile);
			}
			// std::copy(std::begin(m_work_queue.tile_cache), std::end(m_work_queue.tile_cache), std::begin(tile_cache));
			m_work_queue.mut.unlock();
		};

		void SetIntegratorFactory(IIntegratorFactory* integrator_factory_ptr)
		{
			m_integrators.clear();
			m_integrator_factory_ptr = integrator_factory_ptr;
		};

	private:
		SRenderSettings m_settings;
		void* m_integrator_settings;
		const IIntegratorFactory* m_integrator_factory_ptr;
		CSceneIntersector m_intersector;

		SWorkQueue m_work_queue;
		std::vector<IIntegrator*> m_integrators;
		CFramebuffer* m_framebuffer_ptr;
};
