
#include "RenderEngine.hpp"

void CRenderEngine::Init()
{

}

void CRenderEngine::Update()
{

}

void CRenderEngine::Render(SSceneContext* scene_ctx_ptr)
{
	NWorkManager::DistributeWork(m_settings, m_work_queue);
	// NWorkManager::DistributeWorkLowGranularity(m_settings, m_work_queue);

	U32 thread_count;

	if (m_settings.thread_count == 0)
	{
		thread_count = std::thread::hardware_concurrency();
	}
	else
	{
		thread_count = m_settings.thread_count;
	}

	m_integrators.resize(thread_count);

	for (U32 i = 0; i < thread_count; i++)
	{
		m_integrators[i] = m_integrator_factory_ptr->Create(&m_settings, m_integrator_settings, m_intersector);
	}

	std::vector<std::thread> render_threads;
	render_threads.reserve(thread_count);

	for (auto integrator : m_integrators)
	{
		render_threads.push_back(std::thread{ &IIntegrator::Execute, integrator, &m_work_queue, scene_ctx_ptr, m_framebuffer_ptr });
	}

	for (auto& render_thread : render_threads)
	{
		render_thread.join();
	}
}
