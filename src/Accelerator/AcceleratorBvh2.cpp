
#include "AcceleratorBvh2.hpp"

#include "Scene/SceneContext.hpp"

namespace NAcceleratorBvh2
{
	struct SBvh2Node
	{
		SAabb aabb;

		SBvh2Node* child_nodes[2];

		U32 split_axis;
		U32 first_prim_id;
		U32 prim_count;

		void InitLeafNode(U32 first_prim_id, U32 triangle_count, const SAabb& aabb);

		void InitInternalNode(U32 split_axis, SBvh2Node* child0_ptr, SBvh2Node* child1_ptr);
	};

	struct SBvh2PrimitiveInfo
	{
		U32 prim_id;
		SAabb aabb;
		F32x3 center;
	};

	void SBvh2Node::InitLeafNode(U32 in_first_prim_id, U32 in_triangle_count, const SAabb& in_aabb)
	{
		first_prim_id = in_first_prim_id;
		prim_count = in_triangle_count;
		aabb = in_aabb;
	}

	void SBvh2Node::InitInternalNode(U32 in_split_axis, SBvh2Node* child0_ptr, SBvh2Node* child1_ptr)
	{
		split_axis = in_split_axis;
		child_nodes[0] = child0_ptr;
		child_nodes[1] = child1_ptr;
		prim_count = 0;
		aabb = Combine(child0_ptr->aabb, child1_ptr->aabb);
	}

	struct CompareToBucket
	{
		CompareToBucket(int split, int num, int d, const SAabb &b)
			: centroidBounds(b)
		{
			splitBucket = split; bucket_count = num; dim = d;
		}

		bool operator()(const SBvh2PrimitiveInfo &p) const;

		int splitBucket, bucket_count, dim;
		const SAabb &centroidBounds;
	};

	bool CompareToBucket::operator()(const SBvh2PrimitiveInfo &p) const {
		int b = bucket_count * ( ( NMath::GetVec3Elem(p.center, dim) - NMath::GetVec3Elem(centroidBounds.bound_min, dim) ) /
			( NMath::GetVec3Elem(centroidBounds.bound_max, dim) - NMath::GetVec3Elem(centroidBounds.bound_min, dim) ) );
		if (b == bucket_count) b = bucket_count - 1;
		// Assert(b >= 0 && b < bucket_count);
		return b <= splitBucket;
	}

	struct ComparePoints {
		ComparePoints(int d) { dim = d; }
		int dim;
		bool operator()(const SBvh2PrimitiveInfo &a,
			const SBvh2PrimitiveInfo &b) const {
			return NMath::GetVec3Elem(a.center, dim) < NMath::GetVec3Elem(b.center, dim);
		}
	};

	const U32 max_prims_in_node = 8;

	SBvh2Node* BuildBVH2_R(std::vector<SBvh2PrimitiveInfo>& build_data, const std::vector<STriangle>& triangles,
		U32 start_idx, U32 end_idx, U32* total_node_count, std::vector<STriangle>& ordered_triangles)
	{
		(*total_node_count)++;

		SBvh2Node* node_ptr = new SBvh2Node;

		SAabb aabb;
		aabb.bound_min = SVector3f{ std::numeric_limits<F32>::max(), std::numeric_limits<F32>::max(), std::numeric_limits<F32>::max() };
		aabb.bound_max = SVector3f{ -std::numeric_limits<F32>::max(), -std::numeric_limits<F32>::max(), -std::numeric_limits<F32>::max() };

		for (U32 idx = start_idx; idx < end_idx; idx++)
		{
			aabb = Combine(aabb, build_data[idx].aabb);
		}

		U32 prim_count = end_idx - start_idx;

		if (prim_count == 1)
		{
			// Create Leaf node

			U32 first_prim_id = ordered_triangles.size();

			for (U32 idx = start_idx; idx < end_idx; idx++)
			{
				U32 prim_id = build_data[idx].prim_id;
				ordered_triangles.push_back(triangles[prim_id]);
			}

			node_ptr->InitLeafNode(first_prim_id, prim_count, aabb);

		}
		else
		{
			// Create Internal node
			U32 split_axis;

			SAabb center_aabb;
			center_aabb.bound_min = SVector3f{ std::numeric_limits<F32>::max(), std::numeric_limits<F32>::max(), std::numeric_limits<F32>::max() };
			center_aabb.bound_max = SVector3f{ -std::numeric_limits<F32>::max(), -std::numeric_limits<F32>::max(), -std::numeric_limits<F32>::max() };

			for (U32 idx = start_idx; idx < end_idx; idx++)
			{
				center_aabb = Combine(center_aabb, build_data[idx].center);
			}

			SVector3f diagonal = center_aabb.bound_max - center_aabb.bound_min;

			if (diagonal.x > diagonal.y && diagonal.x > diagonal.z)
			{
				split_axis = 0;
			}
			else if (diagonal.y > diagonal.z)
			{
				split_axis = 1;
			}
			else
			{
				split_axis = 2;
			}

			U32 mid_idx = (start_idx + end_idx) / 2;

			if (NMath::GetVec3Elem(center_aabb.bound_min, split_axis) == NMath::GetVec3Elem(center_aabb.bound_max, split_axis))
			{
				if (prim_count <= max_prims_in_node)
				{
					U32 first_prim_id = ordered_triangles.size();

					for (U32 idx = start_idx; idx < end_idx; idx++)
					{
						U32 prim_id = build_data[idx].prim_id;
						ordered_triangles.push_back(triangles[prim_id]);
					}

					node_ptr->InitLeafNode(first_prim_id, prim_count, aabb);

					return node_ptr;
				}
				else
				{
					SBvh2Node* child0_ptr = BuildBVH2_R(build_data, triangles, start_idx, mid_idx, total_node_count, ordered_triangles);
					SBvh2Node* child1_ptr = BuildBVH2_R(build_data, triangles, mid_idx, end_idx, total_node_count, ordered_triangles);

					node_ptr->InitInternalNode(split_axis, child0_ptr, child1_ptr);

					return node_ptr;
				}
			}


			/*
			// else if (Technique == EPartitionTechnique::MIDPOINT)
			{
				F32 SplitPos = 0.5f * (center_aabb.bound_min.elements[split_axis] + center_aabb.bound_max.elements[split_axis]);

				uint32_t mid_idx = start_idx;

				for (U32 idx = start_idx; idx < end_idx; idx++)
				{
					if (build_data[idx].center.elements[split_axis] < SplitPos)
					{
						std::swap(build_data[idx], build_data[mid_idx]);

						mid_idx++;
					}
				}

				if (mid_idx == start_idx || mid_idx == end_idx)
				{
					mid_idx = start_idx + (start_idx + end_idx) / 2;
				}

			}
			*/

			// if (Technique == EPartitionTechnique::SAH)
			/**/
			{
				// Partition primitives using approximate SAH
				if (prim_count <= 4)
				{
					// Partition primitives into equally-sized subsets
					mid_idx = (start_idx + end_idx) / 2;
					std::nth_element(&build_data[start_idx], &build_data[mid_idx],
						&build_data[end_idx - 1] + 1, ComparePoints(split_axis));
				}
				else
				{
					// Allocate _BucketInfo_ for SAH partition buckets
					const int bucket_count = 12;
					struct BucketInfo
					{
						BucketInfo() { count = 0; }
						int count;
						SAabb bounds;
					};
					BucketInfo buckets[bucket_count];

					// Initialize _BucketInfo_ for SAH partition buckets
					for (uint32_t i = start_idx; i < end_idx; ++i)
					{
						int b =
							bucket_count
							*
							(
								( NMath::GetVec3Elem(build_data[i].center, split_axis) - NMath::GetVec3Elem(center_aabb.bound_min, split_axis) )
								/
								( NMath::GetVec3Elem(center_aabb.bound_max, split_axis) - NMath::GetVec3Elem(center_aabb.bound_min, split_axis) )
							);
						if (b == bucket_count) b = bucket_count - 1;
						// Assert(b >= 0 && b < bucket_count);
						buckets[b].count++;
						buckets[b].bounds = Combine(buckets[b].bounds, build_data[i].aabb);
					}

					// Compute costs for splitting after each bucket
					float cost[bucket_count - 1];
					for (int i = 0; i < bucket_count - 1; ++i)
					{
						SAabb b0, b1;
						int count0 = 0, count1 = 0;
						for (int j = 0; j <= i; ++j)
						{
							b0 = Combine(b0, buckets[j].bounds);
							count0 += buckets[j].count;
						}
						for (int j = i + 1; j < bucket_count; ++j)
						{
							b1 = Combine(b1, buckets[j].bounds);
							count1 += buckets[j].count;
						}
						cost[i] = .125f + (count0*ComputeSurfaceArea(b0) + count1*ComputeSurfaceArea(b1)) /
							ComputeSurfaceArea(aabb);
					}

					// Find bucket to split at that minimizes SAH metric
					float min_cost = cost[0];
					uint32_t min_cost_split = 0;
					for (int i = 1; i < bucket_count - 1; ++i)
					{
						if (cost[i] < min_cost)
						{
							min_cost = cost[i];
							min_cost_split = i;
						}
					}

					// Either create leaf or split primitives at selected SAH bucket
					if (prim_count > max_prims_in_node || min_cost < prim_count)
					{
						SBvh2PrimitiveInfo *pmid = std::partition(&build_data[start_idx],
							&build_data[end_idx- 1] + 1,
							CompareToBucket(min_cost_split, bucket_count, split_axis, center_aabb));
						mid_idx = pmid - &build_data[0];
					}
					else
					{
						// Create leaf _BVHBuildNode_
						uint32_t firstPrimOffset = ordered_triangles.size();
						for (uint32_t i = start_idx; i < end_idx; ++i) {
							uint32_t primNum = build_data[i].prim_id;
							ordered_triangles.push_back(triangles[primNum]);
						}
						node_ptr->InitLeafNode(firstPrimOffset, prim_count, aabb);
						return node_ptr;
					}
				}
			}

			SBvh2Node* child0_ptr = BuildBVH2_R(build_data, triangles, start_idx, mid_idx, total_node_count, ordered_triangles);
			SBvh2Node* child1_ptr = BuildBVH2_R(build_data, triangles, mid_idx, end_idx, total_node_count, ordered_triangles);

			node_ptr->InitInternalNode(split_axis, child0_ptr, child1_ptr);
		}

		return node_ptr;

	}

	U32 LinearizeBvh2_R(SBvh2Node* root_ptr, U32& node_offset, std::vector<SLinearBvh2Node>& linear_nodes)
	{
		SLinearBvh2Node& node = linear_nodes[node_offset];
		node.aabb = root_ptr->aabb;
		node.prim_count = root_ptr->prim_count;

		U32 offset = node_offset++;

		if (node.prim_count > 0)
		{
			node.offset = root_ptr->first_prim_id;
		}
		else
		{
			node.axis = root_ptr->split_axis;
			LinearizeBvh2_R(root_ptr->child_nodes[0], node_offset, linear_nodes);
			node.offset = LinearizeBvh2_R(root_ptr->child_nodes[1], node_offset, linear_nodes);
		}

		return offset;
	}

	void Build(std::vector<STriangle>& triangles, std::vector<SLinearBvh2Node>& linear_nodes)
	{
		std::vector<SBvh2PrimitiveInfo> BuildData;

		BuildData.reserve(triangles.size());

		for (U32 idx = 0; idx < triangles.size(); idx++)
		{
			SAabb prim_aabb = ComputeBounds(triangles[idx]);

			SBvh2PrimitiveInfo prim_info;
			prim_info.center = prim_aabb.bound_min * 0.5f + prim_aabb.bound_max * 0.5f;
			prim_info.aabb = prim_aabb;
			prim_info.prim_id = idx;

			BuildData.push_back(prim_info);
		}

		U32 total_node_count = 0;

		std::vector<STriangle> ordered_triangles;
		ordered_triangles.reserve(triangles.size());

		SBvh2Node* root_node_ptr = BuildBVH2_R(BuildData, triangles, 0, triangles.size(), &total_node_count, ordered_triangles);

		triangles.swap(ordered_triangles);

		linear_nodes.resize(total_node_count);

		U32 offset = 0;
		LinearizeBvh2_R(root_node_ptr, offset, linear_nodes);
	}

};

void CAcceleratorBvh2::Update()
{
	if (m_update_flag)
	{
		m_storage.mesh_bvh2_nodes.clear();
		m_storage.temp_mesh_geom = m_scene_ctx_ptr->mesh_geom_manager_ptr->GetTransformedMeshGeomConstRef();
		NAcceleratorBvh2::Build(m_storage.temp_mesh_geom, m_storage.mesh_bvh2_nodes);

		// m_storage.temp_mesh_geom.clear();
		m_update_flag = false;
	}
}
