#pragma once

#include <vector>

#include "Foundation/Common.hpp"
#include "Foundation/Container.hpp"
#include "Foundation/Geometry.hpp"
#include "Foundation/Mesh.hpp"
#include "Foundation/IAccelerator.hpp"
#include "Foundation/IIntersector.hpp"

struct SSceneContext;

enum EPartitionTechnique
{
	MIDPOINT,
	SAH
};

struct SLinearBvh2Node
{
	SAabb aabb;
	U32 offset;
	U32 prim_count;
	U32 axis;
};

struct SAcceleratorBvh2Storage
{
	std::vector<STriangle> temp_mesh_geom;
	std::vector<SLinearBvh2Node> mesh_bvh2_nodes;
};

class CAcceleratorBvh2 : public IAccelerator
{
	using SStorage = SAcceleratorBvh2Storage;

	public:
		CAcceleratorBvh2(SSceneContext* scene_ctx_ptr) { m_scene_ctx_ptr = scene_ctx_ptr; m_update_flag = true; };
		~CAcceleratorBvh2() {};

		virtual void Update() override;

		const SAcceleratorBvh2Storage& GetStorageConstRef() const { return m_storage; };

	protected:

	private:
		SStorage m_storage;
		bool m_update_flag;

		SSceneContext* m_scene_ctx_ptr;
};
