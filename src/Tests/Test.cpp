
#include <iostream>
#include <chrono>

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#define STB_IMAGE_IMPLEMENTATION
#include "../thirdparty/stb/stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../thirdparty/stb/stb_image_write.h"

#include "Engine/RenderEngine.hpp"
#include "Integrator/IntegratorAo.hpp"
#include "Accelerator/AcceleratorBvh2.hpp"
#include "Intersector/IntersectorBvh2.hpp"
#include "Intersector/IntersectorEmbree.hpp"
#include "Intersector/IntersectorLightLinear.hpp"
#include "Scene/SceneIo.hpp"
#include "Integrator/IntegratorAo.hpp"
#include "Integrator/IntegratorUdpt.hpp"

const CIntegratorAoFactory G_INTEGRATOR_AO_FACTORY;
const CIntegratorUdptFactory G_INTEGRATOR_UDPT_FACTORY;

int main(int argc, char const *argv[])
{
	SSceneContext scene_ctx;
	SSceneStorage scene_storage;

	CCameraManager::SStorage camera_storage;
	CMeshManager::SStorage mesh_storage;
	CMeshGeomManager::SStorage mesh_geom_storage;
	CLightManager::SStorage light_storage;
	CShaderManager::SStorage shader_storage;

	scene_storage.camera_storage = &camera_storage;
	scene_storage.mesh_storage = &mesh_storage;
	scene_storage.mesh_geom_storage = &mesh_geom_storage;
	scene_storage.area_light_storage = &light_storage;
	scene_storage.shader_storage = &shader_storage;

	NSceneIo::Import("data/scene.wsf", scene_storage);

	scene_ctx.camera_manager_ptr = new CCameraManager{ &camera_storage, &scene_ctx };
	scene_ctx.mesh_manager_ptr = new CMeshManager{ &mesh_storage, &scene_ctx };
	scene_ctx.mesh_geom_manager_ptr = new CMeshGeomManager{ &mesh_geom_storage, &scene_ctx };
	scene_ctx.light_manager_ptr = new CLightManager{ &light_storage, &scene_ctx };
	// scene_ctx.shader_manager_ptr = new CShaderManager{ &shader_storage, &scene_ctx };
	scene_ctx.shader_manager_ptr = new CShadingEngineOsl;

	SRenderSettings render_settings;
	render_settings.render_width = 640;
	render_settings.render_height = 480;
	render_settings.tile_width = 32;
	render_settings.tile_height = 32;
	render_settings.thread_count = 0;

	/*
	SIntegratorAoSettings integrator_settings;
	integrator_settings.iteration_count = 128;
	integrator_settings.max_ao_distance = 10.0f;
	*/

	SIntegratorUdptSettings integrator_settings;
	integrator_settings.iteration_count = 128;
	integrator_settings.di_sample_count = 1;
	integrator_settings.ii_sample_count = 1;
	integrator_settings.max_indirect_bounce_count = 4;
	integrator_settings.enable_mis = false;
	integrator_settings.enable_rr = false;
	integrator_settings.enable_as = true;

	CSceneIntersector intersector;

	// CAcceleratorBvh2 mesh_accel(&scene_ctx);
	// CIntersectorBvh2 mesh_intersector(&mesh_accel);
	CIntersectorEmbree mesh_intersector(&scene_ctx);

	CIntersectorLightLinear light_intersector;

	intersector.SetMeshIntersector(&mesh_intersector);
	intersector.SetLightIntersector(&light_intersector);

	NSceneContext::Update(&scene_ctx);
	// mesh_accel.Update();
	mesh_intersector.Update();


	// CRenderEngine engine(render_settings, &integrator_settings, intersector, &G_INTEGRATOR_AO_FACTORY);
	CRenderEngine engine(render_settings, &integrator_settings, intersector, &G_INTEGRATOR_UDPT_FACTORY);
	std::chrono::high_resolution_clock::time_point render_begin = std::chrono::high_resolution_clock::now();
	engine.Render(&scene_ctx);
	std::chrono::high_resolution_clock::time_point render_end = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::seconds>(render_end - render_begin).count() << std::endl;

	auto framebuffer = engine.GetFramebufferPtr();
	U8* raw_image = new U8[render_settings.render_width * render_settings.render_height * 3];
	auto color_buffer = framebuffer->GetColorBuffer();

	for (U32 j = 0; j < framebuffer->GetHeight(); j++)
	{
		for (U32 i = 0; i < framebuffer->GetWidth(); i++)
		{
			U32 k = j * render_settings.render_width + i;
			U32 p = k * 3;

			raw_image[p + 0] = (U8)(std::min(std::fabs(color_buffer[k].x) * 255 + (0.5f), (255.0f)));
			raw_image[p + 1] = (U8)(std::min(std::fabs(color_buffer[k].y) * 255 + (0.5f), (255.0f)));
			raw_image[p + 2] = (U8)(std::min(std::fabs(color_buffer[k].z) * 255 + (0.5f), (255.0f)));
		}
	}

	stbi_write_bmp("data/output.bmp", render_settings.render_width, render_settings.render_height, 3, raw_image);
	return 0;
}
