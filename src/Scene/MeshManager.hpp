
#pragma once

#include <vector>

#include "Foundation/Common.hpp"
#include "Foundation/Geometry.hpp"
#include "Foundation/Mesh.hpp"
#include "Foundation/Container.hpp"

struct SSceneContext;

using SMeshId = SResourceId;

struct SMeshContainer
{
	TTrackedArray<SMesh> mesh_arr;
};

/* TODO: Use triangle buffers to hold mesh geometry */
struct SMeshGeomContainer
{
	std::vector<STriangle> mesh_geom;
	std::vector<STriangle> transformed_geom;
	bool transformed_mesh_geom_update_flag;
};

class CMeshGeomManager
{
	public:
		using SStorage = SMeshGeomContainer;

		CMeshGeomManager(){};

		CMeshGeomManager(SStorage* storage_ptr, SSceneContext* scene_ctx_ptr)
		{
			m_storage_ptr = storage_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
			m_storage_ptr->transformed_mesh_geom_update_flag = true;
		};

		virtual ~CMeshGeomManager()
		{
			m_storage_ptr->mesh_geom.clear();
			m_storage_ptr->transformed_geom.clear();
		};

		void Update();

		const std::vector<STriangle>& GetTransformedMeshGeomConstRef() const { return m_storage_ptr->transformed_geom; };

	protected:

	private:
		SStorage* m_storage_ptr;
		SSceneContext* _scene_ctx_ptr;
};


class CMeshManager
{
	public:
		using SStorage = SMeshContainer;

		CMeshManager(){};

		CMeshManager(SStorage* storage_ptr, SSceneContext* scene_ctx_ptr)
		{
			m_storage_ptr = storage_ptr;
			_scene_ctx_ptr = scene_ctx_ptr;
			m_storage_ptr->mesh_arr.update_flag = true;
		};

		virtual ~CMeshManager()
		{
			m_storage_ptr->mesh_arr.Clear();
		};

		const TTrackedArray<SMesh>& GetMeshArrayConstRef() const { return m_storage_ptr->mesh_arr; };

		void Update();

		void SetMesh(SMeshId resource_id, SMesh& resource);
		// void SetMeshMaterial(SMeshId mesh_id, SMaterialId mtl_id);

	protected:

	private:
		SStorage* m_storage_ptr;
		SSceneContext* _scene_ctx_ptr;
};
