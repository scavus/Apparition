
#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/IShaderNode.hpp"

class CShaderNodeLambert : public IShaderNode
{
	public:
		CShaderNodeLambert() { m_albedo_color = { 1.0f, 1.0f, 1.0f, 1.0f }; };
		~CShaderNodeLambert() {};

		virtual void Eval(const SShaderGlobals& sg, SRandomNumberGenerator* rng_ptr, SSampleInfo& sample_info) override;
		virtual void Sample(const SShaderGlobals& sg, SRandomNumberGenerator* rng_ptr, SSampleInfo& sample_info) override;

	protected:

	private:
		F32x4 m_albedo_color;
		F32x4 m_output_color;
};
