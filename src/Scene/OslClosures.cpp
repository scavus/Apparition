
#include "OslClosures.hpp"

#include "Foundation/Math.hpp"
#include "Foundation/Sampling.hpp"
#include "Foundation/Shading.hpp"

F32 CBsdfDiffuse::Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf)
{
	wi = SampleHemisphereCosineWeighted(uv.x, uv.y);
	wi = NMath::Normalize(NMath::TransformDir(sg.tangent_to_world_mtx, wi));

	F32 cos_theta = NMath::Clamp(NMath::Dot({ m_params.normal.x, m_params.normal.y, m_params.normal.z }, wi), 0.0f, 1.0f);
	pdf = PdfHemisphereCosineWeighted(cos_theta);

	return NMath::INV_PI * cos_theta;
}

F32 CBsdfDiffuse::Eval(const SShaderGlobals& sg, const F32x3& wi)
{
	return NMath::INV_PI * NMath::Clamp(NMath::Dot({ m_params.normal.x, m_params.normal.y, m_params.normal.z }, wi), 0.0f, 1.0f);
}

F32 CBsdfReflection::Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf)
{
	return 1.0f;
}

F32 CBsdfReflection::Eval(const SShaderGlobals& sg, const F32x3& wi)
{
	return 1.0f;
}

F32 CBsdfRefraction::Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf)
{
	return 1.0f;
}

F32 CBsdfRefraction::Eval(const SShaderGlobals& sg, const F32x3& wi)
{
	return 1.0f;
}

F32 FresnelSchlick(F32 ior, F32 u)
{
	F32 f0 = pow((ior - 1.0f) / (ior + 1.0f), 2);
	return f0 + (1.0f - f0) * pow(1.0f - u, 5);
}

F32 DistributionBeckmann(F32 alpha, F32 cos_theta_m)
{
	F32 cos_theta_m_2 = NMath::Sqr(cos_theta_m);
	F32 cos_theta_m_4 = NMath::Sqr(cos_theta_m_2);
	F32 alpha_2 = NMath::Sqr(alpha);

	return (1.0f / (NMath::PI * alpha_2 * cos_theta_m_4)) * exp((cos_theta_m_2 - 1.0f) / (alpha_2 * cos_theta_m_2));
}

F32 GeometryBeckmann(F32 alpha, F32 cos_theta)
{
	F32 cos_theta_2 = NMath::Sqr(cos_theta);
	F32 c = cos_theta * (1.0f / alpha) * NMath::Rsqrt(1.0f - cos_theta_2);
	F32 c_2 = NMath::Sqr(c);

	if (c < 1.6f)
	{
		return ((3.535f * c) + (2.181f * c_2)) / (1.0f + (2.276f * c) + (2.577f * c_2));
	}
	else if (c >= 1.6f)
	{
		return 1.0f;
	}
}

F32 GeometrySmithBeckmann(F32 alpha, F32 n_dot_l, F32 n_dot_v)
{
	return GeometryBeckmann(alpha, n_dot_l) * GeometryBeckmann(alpha, n_dot_v);
}

F32 PdfDistributionBeckmann(F32 alpha, F32 n_dot_h, F32 v_dot_h)
{
	F32 pdf_microfacet = DistributionBeckmann(alpha, n_dot_h) * n_dot_h;
	F32 jacobian = 1.0f / (4.0f * v_dot_h);

	return pdf_microfacet * jacobian;
}

F32x3 SampleDistributionBeckmann(F32 u0, F32 u1, F32 alpha)
{
	F32 theta = atan(sqrt(-alpha*alpha * log(1.0f - u0)));
	F32 phi = (2.0f * NMath::PI) * u1;

	return NMath::SphericalToCartesianUnit(theta, phi);
}

F32 CBsdfMicrofacetBeckmann::Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf)
{
	F32x3 m = SampleDistributionBeckmann(uv.x, uv.y, m_params.alpha);
	m = NMath::TransformDir(sg.tangent_to_world_mtx, m);
	F32x3 light_dir = NMath::Reflect(sg.wo, m);

	F32 n_dot_l = fmax(NMath::Dot(sg.normal, light_dir), 1e-3f);
	F32 n_dot_v = fmax(NMath::Dot(sg.normal, sg.wo), 1e-3f);
	F32 n_dot_h = fmax(NMath::Dot(sg.normal, m), 1e-3f);
	F32 l_dot_h = fmax(NMath::Dot(light_dir, m), 1e-3f);
	F32 v_dot_h = fmax(NMath::Dot(sg.wo, m), 1e-3f);

	wi = light_dir;
	pdf = PdfDistributionBeckmann(m_params.alpha, n_dot_h, v_dot_h);

	F32 f = FresnelSchlick(m_params.eta, l_dot_h);
	F32 d = DistributionBeckmann(m_params.alpha, n_dot_h);
	F32 g = GeometrySmithBeckmann(m_params.alpha, n_dot_l, n_dot_v);
	F32 denominator = 4.0f * n_dot_l * n_dot_v;

	return ((f * d * g) / denominator);
}

F32 CBsdfMicrofacetBeckmann::Eval(const SShaderGlobals& sg, const F32x3& wi)
{
	F32x3 half_dir = NMath::Normalize(sg.wo + wi);

	F32 n_dot_l = fmax(NMath::Dot(sg.normal, wi), 1e-3f);
	F32 n_dot_v = fmax(NMath::Dot(sg.normal, sg.wo), 1e-3f);
	F32 n_dot_h = fmax(NMath::Dot(sg.normal, half_dir), 1e-3f);
	F32 l_dot_h = fmax(NMath::Dot(wi, half_dir), 1e-3f);
	F32 v_dot_h = fmax(NMath::Dot(sg.wo, half_dir), 1e-3f);

	F32 f = FresnelSchlick(m_params.eta, l_dot_h);
	F32 d = DistributionBeckmann(m_params.alpha, n_dot_h);
	F32 g = GeometrySmithBeckmann(m_params.alpha, n_dot_l, n_dot_v);
	F32 denominator = 4.0f * n_dot_l * n_dot_v;

	return ((f * d * g) / denominator);
}