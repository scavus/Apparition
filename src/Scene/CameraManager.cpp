
#include <limits>

#include "CameraManager.hpp"

#include "Foundation/Sampling.hpp"

void CCameraManager::Update()
{
}

void CCameraManager::GenerateRays(ISampler* sampler_ptr, const SRenderSettings& render_settings, const STileInfo& tile, std::vector<SRay>& ray_buffer)
{
	SCamera camera = m_storage_ptr->camera;

	// generate new path from camera
	SMatrix4x4 camera_to_world_mtx = camera.camera_to_world_mtx;

	for (U32 j = 0; j < tile.height; j++)
	for (U32 i = 0; i < tile.width; i++)
	{
		F32x2 pixel_coord = { (F32)(i + tile.offset_x), (F32)(j + tile.offset_y) };

		F32x2 uv = sampler_ptr->GetSubsampler(tile.width * j + i)->SampleF32x2();

		F32x2 camera_sample;
		camera_sample = pixel_coord + uv;

		F32 fov;

		if (camera.type == PINHOLE)
		{
			fov = tan(camera.properties.pinhole.fov * 0.5f * NMath::PI / 180.0f);
		}
		else if (camera.type == THINLENS)
		{
			fov = tan(camera.properties.thinlens.fov * 0.5f * NMath::PI / 180.0f);
		}

		F32 aspect_width = (F32)(render_settings.render_width);
		F32 aspect_height = (F32)(render_settings.render_height);

		camera_sample.x = (2.0f * (camera_sample.x) / aspect_width - 1.0f) * fov * (aspect_width / aspect_height);
		camera_sample.y = (1.0f - 2.0f * (camera_sample.y) / aspect_height) * fov;

		SRay ray;

		if (camera.type == PINHOLE)
		{
			ray.origin = NMath::TransformPos(camera_to_world_mtx, { 0.0f, 0.0f, 0.0f });
			ray.direction = NMath::Normalize(NMath::TransformDir(camera_to_world_mtx, { camera_sample.x, camera_sample.y, -1.0f }));
			ray.t_max = std::numeric_limits<F32>::max();
		}
		else if (camera.type == THINLENS)
		{
			F32x2 lens_uv = SampleDiskConcentric(uv.x, uv.y);
			lens_uv = lens_uv * camera.properties.thinlens.aperture_size;
			ray.origin = NMath::TransformPos(camera_to_world_mtx, { 0.0f, 0.0f, 0.0f });
			ray.direction = NMath::Normalize(NMath::TransformDir(camera_to_world_mtx, { camera_sample.x, camera_sample.y, -1.0f }));
			F32x3 focal_point = ray.origin + (ray.direction * camera.properties.thinlens.focal_length);

			ray.origin = NMath::TransformPos(camera_to_world_mtx, { lens_uv.x, lens_uv.y, 0.0f });
			ray.direction = NMath::Normalize(focal_point - ray.origin);
			ray.t_max = std::numeric_limits<F32>::max();
		}

		ray_buffer[tile.width * j + i] = ray;
	}
}

SRay CCameraManager::GenerateRay(const F32x2& uv, const F32x2& pixel_coord, const U32 render_width, const U32 render_height)
{
	SCamera camera = m_storage_ptr->camera;

	SMatrix4x4 camera_to_world_mtx = camera.camera_to_world_mtx;

	F32x2 camera_sample;
	camera_sample = pixel_coord + uv;

	F32 fov;

	if (camera.type == PINHOLE)
	{
		fov = tan(camera.properties.pinhole.fov * 0.5f * NMath::PI / 180.0f);
	}
	else if (camera.type == THINLENS)
	{
		fov = tan(camera.properties.thinlens.fov * 0.5f * NMath::PI / 180.0f);
	}

	F32 aspect_width = (F32)(render_width);
	F32 aspect_height = (F32)(render_height);

	camera_sample.x = (2.0f * (camera_sample.x) / aspect_width - 1.0f) * fov * (aspect_width / aspect_height);
	camera_sample.y = (1.0f - 2.0f * (camera_sample.y) / aspect_height) * fov;

	SRay ray;

	if (camera.type == PINHOLE)
	{
		ray.origin = NMath::TransformPos(camera_to_world_mtx, { 0.0f, 0.0f, 0.0f });
		ray.direction = NMath::Normalize(NMath::TransformDir(camera_to_world_mtx, { camera_sample.x, camera_sample.y, -1.0f }));
		ray.t_max = std::numeric_limits<F32>::max();
	}
	else if (camera.type == THINLENS)
	{
		F32x2 lens_uv = SampleDiskConcentric(uv.x, uv.y);
		lens_uv = lens_uv * camera.properties.thinlens.aperture_size;
		ray.origin = NMath::TransformPos(camera_to_world_mtx, { 0.0f, 0.0f, 0.0f });
		ray.direction = NMath::Normalize(NMath::TransformDir(camera_to_world_mtx, { camera_sample.x, camera_sample.y, -1.0f }));
		F32x3 focal_point = ray.origin + (ray.direction * camera.properties.thinlens.focal_length);

		ray.origin = NMath::TransformPos(camera_to_world_mtx, { lens_uv.x, lens_uv.y, 0.0f });
		ray.direction = NMath::Normalize(focal_point - ray.origin);
		ray.t_max = std::numeric_limits<F32>::max();
	}

	return ray;
}
