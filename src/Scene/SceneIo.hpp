
#pragma once

#include "Scene/SceneContext.hpp"

namespace NSceneIo
{
	void Import(const std::string& filename, SSceneStorage& scene_storage);
};
