
#pragma once

struct SShaderGlobals;
struct SVector2f;
struct SVector3f;

#include "Foundation/Common.hpp"
#include "Foundation/Math.hpp"
#include "Foundation/Shading.hpp"

class IBsdf
{
	public:
		virtual F32 Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf) = 0;
		virtual F32 Eval(const SShaderGlobals& sg, const F32x3& wi) = 0;

		virtual ~IBsdf(){};

	protected:

	private:
};
