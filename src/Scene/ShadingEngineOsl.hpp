
#pragma once

#include <unordered_map>

#include "OSL/genclosure.h"
#include "OSL/oslexec.h"
#include "OSL/oslclosure.h"
#include "OSL/shaderglobals.h"

#include "Foundation/Common.hpp"
#include "Foundation/Container.hpp"
#include "Foundation/Math.hpp"
#include "Foundation/Shading.hpp"
#include "Foundation/Sampling.hpp"
#include "Foundation/ISampler.hpp"

namespace NShadingEngineOsl
{
	inline F32x3 ToXrType(const OSL::Vec3& v)
	{
		return { v.x, v.y, v.z };
	}

	inline F32x4 ToXrType(const OSL::Color3& c)
	{
		return { c.x, c.y, c.z, 1.0f };
	}

	inline OSL::Vec2 ToOslType(const F32x2& v)
	{
		return { v.x, v.y };
	}

	inline OSL::Vec3 ToOslType(const F32x3& v)
	{
		return { v.x, v.y, v.z };
	}

};

class CShadingEngineOsl
{
	public:
		CShadingEngineOsl();
		~CShadingEngineOsl();

		void Eval(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, ISampler* sampler_ptr, std::vector<SSampleInfo>& sample_info_buffer);
		void Sample(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, ISampler* sampler_ptr, std::vector<SSampleInfo>& sample_info_buffer);

	protected:

	private:
		OSL::ErrorHandler m_error_handler;
		OSL::ShadingSystem* m_osl_system_ptr;
		TTrackedArray<OSL::ShaderGroupRef> m_osl_shader_networks;
};
