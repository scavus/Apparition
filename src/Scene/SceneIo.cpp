
#include <algorithm>
#include <fstream>

#include "SceneIo.hpp"

namespace WsfData
{
	struct SWsfHeader
	{
		U32 mesh_count;
		U32 area_light_count;
	};
}

namespace NSceneIo
{
	struct SVector2Import
	{
		F32 x, y;
	};

	struct SVector3Import
	{
		F32 x, y, z;
	};

	struct SVector4Import
	{
		F32 x, y, z, w;
	};

	struct SVertexImport
	{
		SVector3Import pos;
		SVector3Import normal;
		SVector3Import tangent;
		SVector3Import binormal;
		SVector2Import texcoord;
	};

	struct STriangleImport
	{
		SVertexImport vtx[3];
		U32 mesh_id;
	};

	struct SMatrix4x4Import
	{
		SVector4Import r0;
		SVector4Import r1;
		SVector4Import r2;
		SVector4Import r3;
	};

	enum ECameraTypeImport
	{
		CT_PINHOLE,
		CT_THINLENS
	};

	struct SPinholeCameraPropertiesImport
	{
		F32 fov;
	};

	struct SThinlensCameraPropertiesImport
	{
		F32 aperture_size;
		F32 focal_length;
		F32 fov;
	};

	struct SCameraImport
	{
		ECameraTypeImport type;

		union UProperties
		{
			SPinholeCameraPropertiesImport pinhole;
			SThinlensCameraPropertiesImport thinlens;

		} properties;

		SMatrix4x4Import camera_to_world_mtx;
	};

	struct SMeshImport
	{
		SMatrix4x4Import obj_to_world_mtx;
		SMatrix4x4Import world_to_obj_mtx;
		U32 material_id;
		U32 polygon_count;
	};

	struct SAreaLightImport
	{
		U32 type;
		SMatrix4x4Import obj_to_world_mtx;
		SMatrix4x4Import world_to_obj_mtx;
		F32 intensity;
		F32 exposure;
		SVector4Import color;
	};

	const ELightType LIGHT_TYPE_MAP[] =
	{
		ELightType::LIGHT_TYPE_DISK,
		ELightType::LIGHT_TYPE_SPHERE,
		ELightType::LIGHT_TYPE_QUAD,
	};

	SVector2f ConvertImportedType(const SVector2Import& v)
	{
		SVector2f ov;
		ov.x = v.x;
		ov.y = v.y;

		return ov;
	}

	SVector3f ConvertImportedType(const SVector3Import& v)
	{
		SVector3f ov;
		ov.x = v.x;
		ov.y = v.y;
		ov.z = v.z;

		return ov;
	}

	SVector4f ConvertImportedType(const SVector4Import& v)
	{
		SVector4f ov;
		ov.x = v.x;
		ov.y = v.y;
		ov.z = v.z;
		ov.w = v.w;

		return ov;
	}

	SMatrix4x4 ConvertImportedType(const SMatrix4x4Import& m)
	{
		SMatrix4x4 om;
		om.r0 = ConvertImportedType(m.r0);
		om.r1 = ConvertImportedType(m.r1);
		om.r2 = ConvertImportedType(m.r2);
		om.r3 = ConvertImportedType(m.r3);

		return om;
	}

	void Import(const std::string& filename, SSceneStorage& scene_storage)
	{
		std::ifstream f(filename, std::ios::in | std::ios::binary);

		WsfData::SWsfHeader header;
		f.read((char*)&header, sizeof(WsfData::SWsfHeader));

		SCamera camera;
		SCameraImport camera_imported;
		f.read((char*)&camera_imported, sizeof(SCameraImport));
		camera.camera_to_world_mtx = ConvertImportedType(camera_imported.camera_to_world_mtx);
		camera.type = static_cast<ECameraType>(camera_imported.type);
		camera.properties.pinhole.fov = camera_imported.properties.pinhole.fov;
		camera.properties.thinlens.fov = camera_imported.properties.thinlens.fov;
		camera.properties.thinlens.aperture_size = camera_imported.properties.thinlens.aperture_size;
		camera.properties.thinlens.focal_length = camera_imported.properties.thinlens.focal_length;
		scene_storage.camera_storage->camera = camera;

		std::vector<STriangle> polygon_buffer;

		for (U32 i = 0; i < header.mesh_count; i++)
		{
			SMesh mesh;
			SMeshImport mesh_imported;
			f.read((char*)&mesh_imported, sizeof(SMeshImport));
			mesh.obj_to_world_mtx = ConvertImportedType(mesh_imported.obj_to_world_mtx);
			mesh.world_to_obj_mtx = ConvertImportedType(mesh_imported.world_to_obj_mtx);
			// mesh.material_id.idx = mesh_imported.material_id;
			mesh.polygon_count = mesh_imported.polygon_count;
			scene_storage.mesh_storage->mesh_arr.Append(mesh);

			std::vector<STriangle> polygons;
			polygons.reserve(mesh.polygon_count);

			for (U32 j = 0; j < mesh.polygon_count; j++)
			{
				STriangle polygon;
				STriangleImport polygon_imported;
				f.read((char*)&polygon_imported, sizeof(STriangleImport));

				for (U32 k = 0; k < 3; k++)
				{
					polygon.vtx[k].pos = ConvertImportedType(polygon_imported.vtx[k].pos);
					polygon.vtx[k].normal = ConvertImportedType(polygon_imported.vtx[k].normal);
					polygon.vtx[k].tangent = ConvertImportedType(polygon_imported.vtx[k].tangent);
					polygon.vtx[k].binormal = ConvertImportedType(polygon_imported.vtx[k].binormal);
					polygon.vtx[k].texcoord = ConvertImportedType(polygon_imported.vtx[k].texcoord);
				}

				polygon.mesh_id = polygon_imported.mesh_id;

				polygons.push_back(polygon);
			}

			polygon_buffer.insert(std::end(polygon_buffer), std::begin(polygons), std::end(polygons));
		}

		std::swap(scene_storage.mesh_geom_storage->mesh_geom, polygon_buffer);

		for (U32 i = 0; i < header.area_light_count; i++)
		{
			SAreaLight area_light;
			SAreaLightImport area_light_imported;
			f.read((char*)&area_light_imported, sizeof(SAreaLightImport));

			area_light.type = LIGHT_TYPE_MAP[area_light_imported.type];
			area_light.obj_to_world_mtx = ConvertImportedType(area_light_imported.obj_to_world_mtx);
			area_light.world_to_obj_mtx = ConvertImportedType(area_light_imported.world_to_obj_mtx);
			area_light.intensity = area_light_imported.intensity;
			area_light.exposure = area_light_imported.exposure;
			area_light.color = ConvertImportedType(area_light_imported.color);
			scene_storage.area_light_storage->area_lights_arr.Append(area_light);
		}
		
		f.close();
	}
}
