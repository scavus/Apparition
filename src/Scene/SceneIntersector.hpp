
#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/IIntersector.hpp"
#include "Foundation/Geometry.hpp"

class CSceneIntersector
{
	public:
		CSceneIntersector() {};
		~CSceneIntersector() {};

		void SetMeshIntersector(IIntersector* intersector_ptr) { m_mesh_intersector_ptr = intersector_ptr; };
		void SetLightIntersector(IIntersector* intersector_ptr) { m_light_intersector_ptr = intersector_ptr; };

		void QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data);
		void QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info);
		void QueryOcclusion();

	protected:

	private:
		IIntersector* m_mesh_intersector_ptr;
		IIntersector* m_light_intersector_ptr;
};
