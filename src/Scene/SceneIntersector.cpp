
#include "SceneIntersector.hpp"

#include <iostream>

void CSceneIntersector::QueryIntersection(const SSceneContext* scene_ctx_ptr, SIntersectorQueryData& query_data)
{
	if (m_mesh_intersector_ptr)
	{
		m_mesh_intersector_ptr->QueryIntersection(scene_ctx_ptr, query_data);
	}

	if (m_light_intersector_ptr)
	{
		m_light_intersector_ptr->QueryIntersection(scene_ctx_ptr, query_data);
	}
}

void CSceneIntersector::QueryIntersection1(const SSceneContext* scene_ctx_ptr, SRay& ray, SIntersectionData& hit_info)
{
	if (m_mesh_intersector_ptr)
	{
		m_mesh_intersector_ptr->QueryIntersection1(scene_ctx_ptr, ray, hit_info);
	}

	if (m_light_intersector_ptr)
	{
		m_light_intersector_ptr->QueryIntersection1(scene_ctx_ptr, ray, hit_info);
	}
}

void CSceneIntersector::QueryOcclusion()
{
}
