
#pragma once

#include <vector>

#include "Foundation/Camera.hpp"
#include "Foundation/Geometry.hpp"
#include "Foundation/Settings.hpp"
#include "Foundation/WorkManager.hpp"
#include "Foundation/ISampler.hpp"

struct SSceneContext;

struct SCameraContainer
{
	SCamera camera;
};

class CCameraManager
{
	public:
		using SStorage = SCameraContainer;

		CCameraManager(){};
		virtual ~CCameraManager(){};

		CCameraManager(SStorage* storage_ptr, SSceneContext* scene_ctx_ptr)
		{
			m_storage_ptr = storage_ptr;
			m_scene_ctx_ptr = scene_ctx_ptr;
		};

		SCamera& GetCameraRef();
		SCamera& GetCameraConstRef();

		void SetCamera(SCamera& camera);
		void Update();
		void GenerateRays(ISampler* sampler_ptr, const SRenderSettings& render_settings, const STileInfo& tile, std::vector<SRay>& ray_buffer);
		SRay GenerateRay(const F32x2& uv, const F32x2& pixel_coord, const U32 render_width, const U32 render_height);

	protected:

	private:
		SSceneContext* m_scene_ctx_ptr;
		SStorage* m_storage_ptr;
};
