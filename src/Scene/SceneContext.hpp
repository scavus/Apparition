
#pragma once

#include "Scene/CameraManager.hpp"
#include "Scene/MeshManager.hpp"
#include "Scene/LightManager.hpp"
#include "Scene/ShaderManager.hpp"
#include "Scene/ShadingEngineOsl.hpp"

struct SSceneContext
{
	CCameraManager* camera_manager_ptr;
	CMeshManager* mesh_manager_ptr;
	CMeshGeomManager* mesh_geom_manager_ptr;
	CLightManager* light_manager_ptr;
	// CShaderManager* shader_manager_ptr;
	CShadingEngineOsl* shader_manager_ptr;
};

struct SSceneStorage
{
	CCameraManager::SStorage* camera_storage;
	CMeshManager::SStorage* mesh_storage;
	CMeshGeomManager::SStorage* mesh_geom_storage;
	CLightManager::SStorage* area_light_storage;
	CShaderManager::SStorage* shader_storage;
	/*
	CAreaLightManager::SStorage* area_light_storage;
	CEnvLightManager::SStorage* env_light_storage;
	CTexture2dManager::SStorage* texture2d_storage;
	CDeferredShadingSystem::SStorage* shader_storage;
	*/
};

namespace NSceneContext
{
	inline void Update(SSceneContext* scene_ctx_ptr)
	{
		scene_ctx_ptr->camera_manager_ptr->Update();
		scene_ctx_ptr->mesh_manager_ptr->Update();
		scene_ctx_ptr->mesh_geom_manager_ptr->Update();
		scene_ctx_ptr->light_manager_ptr->Update();
	}
}
