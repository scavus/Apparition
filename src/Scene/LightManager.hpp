
#pragma once

#include "Foundation/Common.hpp"
#include "Foundation/Container.hpp"
#include "Foundation/Light.hpp"
#include "Foundation/ISampler.hpp"

struct SSceneContext;

using SAreaLightId = SResourceId;

struct SAreaLightContainer
{
	TTrackedArray<SAreaLight> area_lights_arr;
	std::vector<SAreaLight> transformed_area_lights_arr;
};

class CLightManager
{
	public:
		using SStorage = SAreaLightContainer;

		struct SMetadata
		{
			U32 light_count;
		};

		CLightManager(){};

		virtual ~CLightManager()
		{
			m_storage_ptr->area_lights_arr.Clear();
			m_storage_ptr->transformed_area_lights_arr.clear();
		};

		CLightManager(SStorage* storage_ptr, SSceneContext* scene_ctx_ptr)
		{
			m_storage_ptr = storage_ptr;
			m_storage_ptr->area_lights_arr.update_flag = true;
			m_scene_ctx_ptr = scene_ctx_ptr;
		};

		const std::vector<SAreaLight>& GetAreaLightsArrayConstRef() const { return m_storage_ptr->transformed_area_lights_arr; };

		SAreaLightId CreateAreaLight(SAreaLight& area_light);
		void SetAreaLight(SAreaLightId area_light_id, SAreaLight& area_light);

		void DestroyAreaLight(SAreaLightId resource_id);

		void Update();

		void Eval(const U32* input_stream, const U32 input_stream_length, const SShaderGlobals* sg_buffer, const SIntersectionData* hit_info_buffer, ISampler* sampler_ptr, SSampleInfo* sample_info_buffer);
		void Sample(const U32* input_stream, const U32 input_stream_length, const SShaderGlobals* sg_buffer, ISampler* sampler_ptr, SSampleInfo* sample_info_buffer);

		SMetadata GetMetadata()
		{
			SMetadata metadata;
			metadata.light_count = m_storage_ptr->area_lights_arr.data.size();

			return metadata;
		};

	protected:

	private:
		SSceneContext* m_scene_ctx_ptr;
		SStorage* m_storage_ptr;
};
