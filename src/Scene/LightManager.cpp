
#include "LightManager.hpp"

#include "Scene/SceneContext.hpp"

namespace NLightManager
{
	void Transform(SAreaLightContainer* area_light_container_ptr)
	{
		for (SAreaLight& area_light : area_light_container_ptr->area_lights_arr.data)
		{
			switch (area_light.type)
			{
				case ELightType::LIGHT_TYPE_DISK:
				{
					SDisk& disk = area_light.shape.disk;

					SDisk pt_disk;

					pt_disk.center = NMath::TransformPos(area_light.obj_to_world_mtx, { 0.0f, 0.0f, 0.0f });
					pt_disk.normal = NMath::Normalize(NMath::TransformDir(area_light.obj_to_world_mtx, { 0.0f, 0.0f, -1.0f }));
					pt_disk.radius = 1.0f;

					SAreaLight pt_area_light;
					pt_area_light = area_light;
					pt_area_light.shape.disk = pt_disk;

					area_light_container_ptr->transformed_area_lights_arr.push_back(pt_area_light);

					break;
				}
				/*
				case ELightType::LIGHT_TYPE_SPHERE:
				{
					SSphere& Sphere = LightSpheres[area_light.shape_id];

					SSphere PtSphere;

					PtSphere.center = (area_light.obj_to_world_mtx * Math::Vector4(Sphere.center, 1.0f)).xyz();
					// PtSphere.radius =

					PtLightSpheres.push_back(PtSphere);

					break;
				}
				*/
				case ELightType::LIGHT_TYPE_QUAD:
				{
					SQuad pt_quad;

					pt_quad.base = NMath::TransformPos(area_light.obj_to_world_mtx, { -0.5f, 0.0f, 0.5f });
					// pt_quad.u = MathUtils::TransformDir(area_light.obj_to_world_mtx, MathUtils::MakeVec3(1.0f, 0.0f, 0.0f));
					// pt_quad.v = MathUtils::TransformDir(area_light.obj_to_world_mtx, MathUtils::MakeVec3(0.0f, 0.0f, -1.0f));
					pt_quad.u = NMath::TransformDir(area_light.obj_to_world_mtx, { 1.0f, 0.0f, 0.0f });
					pt_quad.v = NMath::TransformDir(area_light.obj_to_world_mtx, { 0.0f, 0.0f, 1.0f });

					pt_quad.normal = NMath::Normalize(NMath::Cross(pt_quad.u, pt_quad.v));
					// pt_quad.normal = WrVector3f{ 0.0f, 1.0f, 0.0f };

					pt_quad.inv_length_sqr_u = 1.0f / (pt_quad.u.x * pt_quad.u.x + pt_quad.u.y * pt_quad.u.y + pt_quad.u.z * pt_quad.u.z);
					pt_quad.inv_length_sqr_v = 1.0f / (pt_quad.v.x * pt_quad.v.x + pt_quad.v.y * pt_quad.v.y + pt_quad.v.z * pt_quad.v.z);

					SAreaLight pt_area_light;
					pt_area_light = area_light;
					pt_area_light.shape.quad = pt_quad;
					area_light_container_ptr->transformed_area_lights_arr.push_back(pt_area_light);
					// area_light_container_ptr->transformed_area_light_quads_arr.Append(pt_quad);

					break;
				}
				/*
				case ELightType::LIGHT_TYPE_TUBE:
				{

					break;
				}
				*/
				default:
					break;
			}
		}
	}
}

SAreaLightId CLightManager::CreateAreaLight(SAreaLight& area_light)
{
	SAreaLightId id;
	id = m_storage_ptr->area_lights_arr.Append(area_light);

	m_storage_ptr->area_lights_arr.update_flag = true;

	return id;
}

void CLightManager::Update()
{
	if (m_storage_ptr->area_lights_arr.update_flag)
	{
		NLightManager::Transform(m_storage_ptr);

		m_storage_ptr->area_lights_arr.update_flag = false;
	}
}

void CLightManager::DestroyAreaLight(SAreaLightId resource_id)
{

}

void CLightManager::SetAreaLight(SAreaLightId area_light_id, SAreaLight& area_light)
{
	m_storage_ptr->area_lights_arr[area_light_id] = std::move(area_light);
	m_storage_ptr->area_lights_arr.update_flag = true;
}

void CLightManager::Eval(const U32* input_stream, const U32 input_stream_length, const SShaderGlobals* sg_buffer, const SIntersectionData* hit_info_buffer, ISampler* sampler_ptr, SSampleInfo* sample_info_buffer)
{
	for (U32 i = 0; i < input_stream_length; i++)
	{
		U32 sample_idx = input_stream[i];
		// SShaderGlobals sg = sg_buffer[i];
		SIntersectionData hit_info = hit_info_buffer[sample_idx];
		SSampleInfo& sample_info = sample_info_buffer[sample_idx];

		SAreaLight light = m_storage_ptr->transformed_area_lights_arr[hit_info.occluder_id];

		F32x4 l = light.color * light.intensity /* std::powf(2.0f, light.exposure)*/;
		sample_info.throughput = sample_info.throughput * l;
	}
}

using FnLightSample = void(*) (const SAreaLight& light, const SShaderGlobals& sg, const F32 u0, const F32 u1, SSampleInfo& light_sample);

const FnLightSample LIGHT_SAMPLE_FN_TABLE[] = 
{
	SampleLightQuad,
	SampleLightDisk,
	SampleLightSphere
};

void CLightManager::Sample(const U32* input_stream, const U32 input_stream_length, const SShaderGlobals* sg_buffer, ISampler* sampler_ptr, SSampleInfo* sample_info_buffer)
{
	for (U32 stream_idx = 0; stream_idx < input_stream_length; stream_idx++)
	{
		U32 sample_idx = input_stream[stream_idx];
		SShaderGlobals sg = sg_buffer[sample_idx];
		SSampleInfo& sample_info = sample_info_buffer[sample_idx];
		U32 u = sampler_ptr->GetSubsampler(sample_idx)->SampleU32();
		// U32 light_id = SampleLightIdUniform(u, m_storage_ptr->light_arr);
		// U32 selected_light_id = SampleLightIdUniform(rng_ptr, scene_metadata_ptr);

		F32x2 uv = sampler_ptr->GetSubsampler(sample_idx)->SampleF32x2();
		SAreaLight light = m_storage_ptr->transformed_area_lights_arr[0];

		LIGHT_SAMPLE_FN_TABLE[light.type](light, sg, uv.x, uv.y, sample_info);
		sample_info.pdf *= 1.0f / ((F32)m_storage_ptr->transformed_area_lights_arr.size());
		sample_info.throughput = sample_info.throughput * (1.0f / sample_info.pdf);
	}
}


