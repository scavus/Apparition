
#include "ShaderNodeLambert.hpp"

void CShaderNodeLambert::Eval(const SShaderGlobals& sg, SRandomNumberGenerator* rng_ptr, SSampleInfo& sample_info)
{
	F32 cos_theta = fabs(NMath::Dot(sg.normal, sample_info.direction));
	F32x4 throughput = NMath::INV_PI * m_albedo_color * cos_theta;
	sample_info.throughput = sample_info.throughput * throughput;
}

void CShaderNodeLambert::Sample(const SShaderGlobals& sg, SRandomNumberGenerator* rng_ptr, SSampleInfo& sample_info)
{
	F32x2 uv;
	uv.x = GenerateF32(rng_ptr);
	uv.y = GenerateF32(rng_ptr);

	F32x3 sample_dir = SampleHemisphereCosineWeighted(uv.x, uv.y);
	sample_dir = NMath::Normalize(NMath::TransformDir(sg.tangent_to_world_mtx, sample_dir));

	F32 cos_theta = fabs(NMath::Dot(sg.normal, sample_dir));
	F32 sample_pdf = PdfHemisphereCosineWeighted(cos_theta);

	sample_info.direction = sample_dir;
	sample_info.pdf = sample_pdf;

	F32x4 throughput = NMath::INV_PI * m_albedo_color * cos_theta * (1.0f / sample_pdf);
	sample_info.throughput = sample_info.throughput * throughput;
}
