
#include "ShadingEngineOsl.hpp"

#include "Scene/IBsdf.hpp"
#include "Scene/OslClosures.hpp"

enum EClosureId
{
	EMISSION_ID = 1,
	BACKGROUND_ID,
	DIFFUSE_ID,
	OREN_NAYAR_ID,
	TRANSLUCENT_ID,
	PHONG_ID,
	WARD_ID,
	MICROFACET_BECKMANN_ID,
	REFLECTION_ID,
	FRESNEL_REFLECTION_ID,
	REFRACTION_ID,
	TRANSPARENT_ID,
};

static const U32 MAX_CLOSURE_PARAM_COUNT = 32;

struct SBuiltinClosure
{
	const char* name;
	U32 id;
	OSL::ClosureParam params[MAX_CLOSURE_PARAM_COUNT];
};

struct SBsdfEmptyParams {};

OSL_NAMESPACE_ENTER
static const SBuiltinClosure BUILTIN_CLOSURES[] =
{
	{ "emission"   , EMISSION_ID, 	{ CLOSURE_FINISH_PARAM(SBsdfEmptyParams) } },

	{ "background" , BACKGROUND_ID, { CLOSURE_FINISH_PARAM(SBsdfEmptyParams) } },

	{ "diffuse"    , DIFFUSE_ID, 	{ CLOSURE_VECTOR_PARAM(CBsdfDiffuse::SParams, normal),
									  CLOSURE_FINISH_PARAM(CBsdfDiffuse::SParams) }
	},

	{ "reflection" , REFLECTION_ID, { CLOSURE_VECTOR_PARAM(CBsdfReflection::SParams, normal),
									  CLOSURE_FLOAT_PARAM(CBsdfReflection::SParams, eta),
									  CLOSURE_FINISH_PARAM(CBsdfReflection::SParams) }
	},

	{ "refraction" , REFRACTION_ID, { CLOSURE_VECTOR_PARAM(CBsdfRefraction::SParams, normal),
									  CLOSURE_FLOAT_PARAM(CBsdfRefraction::SParams, eta),
									  CLOSURE_FINISH_PARAM(CBsdfRefraction::SParams) }
	},

	{ "microfacet_beckmann" , MICROFACET_BECKMANN_ID, { CLOSURE_VECTOR_PARAM(CBsdfMicrofacetBeckmann::SParams, normal),
														CLOSURE_FLOAT_PARAM(CBsdfMicrofacetBeckmann::SParams, alpha),
														CLOSURE_FLOAT_PARAM(CBsdfMicrofacetBeckmann::SParams, eta),
														CLOSURE_FINISH_PARAM(CBsdfMicrofacetBeckmann::SParams) }
	},

	{ NULL, 0, {} }
};
OSL_NAMESPACE_EXIT


struct SBsdfArrayComponent
{
	OSL::Color3 weight;
	IBsdf* bsdf;
	F32 pdf;
};

class CBsdfArray
{
	public:
		CBsdfArray() { m_active_component_count = 0; };
		~CBsdfArray()
		{
			for (U32 i = 0; i < m_active_component_count; i++)
			{
				delete m_components[i].bsdf;
			}
		};

		void Eval(const SShaderGlobals& sg, SSampleInfo& sample_info);

		void Sample(const SShaderGlobals& sg, const F32x2& uv, SSampleInfo& sample_info);

		void AddComponent(const OSL::Color3& component_weight, IBsdf* component_ptr);

		void Clear()
		{
			for (U32 i = 0; i < m_active_component_count; i++)
			{
				delete m_components[i].bsdf;
			}

			m_active_component_count = 0;
		}

	protected:

	private:
		static const U32 MAX_BSDF_COUNT = 4;

		SBsdfArrayComponent m_components[MAX_BSDF_COUNT];
		U32 m_active_component_count;
};

void CBsdfArray::Eval(const SShaderGlobals& sg, SSampleInfo& sample_info)
{
	OSL::Color3 fr = { 0.0f, 0.0f, 0.0f };

	for (U32 i = 0; i < m_active_component_count; i++)
	{
		fr += m_components[i].weight * m_components[i].bsdf->Eval(sg, sample_info.direction);
	}

	sample_info.throughput = sample_info.throughput * F32x4{ fr.x, fr.y, fr.z, 1.0f };
}

void CBsdfArray::Sample(const SShaderGlobals& sg, const F32x2& uv, SSampleInfo& sample_info)
{
	OSL::Color3 fr = { 0.0f, 0.0f, 0.0f };

	/* Select a random component */
	U32 component_id = static_cast<U32>(ceilf(uv.x * static_cast<F32>(m_active_component_count))) - 1;

	fr += m_components[component_id].weight * m_components[component_id].bsdf->Sample(sg, uv, sample_info.direction, sample_info.pdf);

	for (U32 i = 0; i < m_active_component_count; i++)
	{
		if (i == component_id)
		{
			continue;
		}

		fr += m_components[i].weight * m_components[i].bsdf->Eval(sg, sample_info.direction);
	}

	sample_info.pdf /= static_cast<F32>(m_active_component_count);
}

void CBsdfArray::AddComponent(const OSL::Color3& component_weight, IBsdf* component_ptr)
{
	if (m_active_component_count >= MAX_BSDF_COUNT)
	{
		return;
	}

	m_components[m_active_component_count].weight = component_weight;
	m_components[m_active_component_count].bsdf = component_ptr;
	m_active_component_count++;
}

struct SShadingResult
{
	F32x4 throughput;
	CBsdfArray bsdf_array;
};

namespace NShadingEngineOsl
{
	void RegisterClosures(OSL::ShadingSystem* shadingsys)
	{
		for (U32 i = 0; OSL::BUILTIN_CLOSURES[i].name; i++)
		{
			shadingsys->register_closure(
				OSL::BUILTIN_CLOSURES[i].name,
				OSL::BUILTIN_CLOSURES[i].id,
				OSL::BUILTIN_CLOSURES[i].params,
				NULL, NULL);
		}
	}

	void ProcessClosureNetwork_R(SShadingResult& shading_result, const OSL::ClosureColor* closure_ptr, OSL::Color3& weight)
	{
		switch (closure_ptr->id)
		{
			case OSL::ClosureColor::ADD:
			{
				ProcessClosureNetwork_R(shading_result, closure_ptr->as_add()->closureA, weight);
				ProcessClosureNetwork_R(shading_result, closure_ptr->as_add()->closureB, weight);
				break;
			}
			case OSL::ClosureColor::MUL:
			{
				OSL::Color3 current_weight = weight * closure_ptr->as_mul()->weight;
				ProcessClosureNetwork_R(shading_result, closure_ptr->as_mul(), current_weight);
				break;
			}
			default:
			{
				const OSL::ClosureComponent* closure_component_ptr = closure_ptr->as_comp();
				OSL::Color3 current_weight = weight * closure_component_ptr->w;

				switch (closure_component_ptr->id)
				{
					case EClosureId::DIFFUSE_ID:
					{
						shading_result.bsdf_array.AddComponent(current_weight, new CBsdfDiffuse{ *closure_component_ptr->as<CBsdfDiffuse::SParams>() });
						break;
					}
					case EClosureId::REFLECTION_ID:
					{
						shading_result.bsdf_array.AddComponent(current_weight, new CBsdfReflection{ *closure_component_ptr->as<CBsdfReflection::SParams>() });
						break;
					}
					case EClosureId::REFRACTION_ID:
					{
						shading_result.bsdf_array.AddComponent(current_weight, new CBsdfRefraction{ *closure_component_ptr->as<CBsdfRefraction::SParams>() });
						break;
					}
					case EClosureId::MICROFACET_BECKMANN_ID:
					{
						shading_result.bsdf_array.AddComponent(current_weight, new CBsdfMicrofacetBeckmann{ *closure_component_ptr->as<CBsdfMicrofacetBeckmann::SParams>() });
						break;
					}
				}

				break;
			}
		}
	}

	void FilterInputStream(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, std::vector<std::vector<U32>>& stream_array)
	{
		for (auto i : input_stream)
		{
			U32 shader_id = sg_buffer[i].material_id;
			stream_array[shader_id].push_back(i);
		}
	}
}

/*
class CNullRendererServices : public OSL::RendererServices
{
	public:
		CNullRendererServices() {};
		~CNullRendererServices() {}

		virtual int supports(OSL::string_view feature) const { return 0; };
		virtual bool get_matrix(OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
			OSL::TransformationPtr xform,
			float time)
		{
			return false;
		};
		virtual bool get_matrix(OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
			OSL::ustring from, float time)
		{
			return false;
		};
		virtual bool get_matrix(OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
			OSL::TransformationPtr xform) {
			return false;
		};
		virtual bool get_matrix(OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
			OSL::ustring from)
		{
			return false;
		};
		virtual bool get_inverse_matrix(OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
			OSL::ustring to, float time)
		{
			return false;
		};

		virtual bool get_array_attribute(OSL::ShaderGlobals *sg, bool derivatives,
			OSL::ustring object, OSL::TypeDesc type, OSL::ustring name,
			int index, void *val)
		{
			return false;
		};
		virtual bool get_attribute(OSL::ShaderGlobals *sg, bool derivatives, OSL::ustring object,
			OSL::TypeDesc type, OSL::ustring name, void *val)
		{
			return false;
		};
		virtual bool get_userdata(bool derivatives, OSL::ustring name, OSL::TypeDesc type,
			OSL::ShaderGlobals *sg, void *val)
		{
			return false;
		};

	private:
};
*/

/*
class CNullRendererServices : public OSL::RendererServices
{
	public:

	    CNullRendererServices () {};
	    ~CNullRendererServices () {}

	    virtual int supports (OSL::string_view feature) const {return 0;};
	    virtual bool get_matrix (OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
	                             OSL::TransformationPtr xform,
	                             float time) {return false;};
	    virtual bool get_matrix (OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
	                             OSL::ustring from, float time) {return false;};
	    virtual bool get_matrix (OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
	                             OSL::TransformationPtr xform) {return false;};
	    virtual bool get_matrix (OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
	                             OSL::ustring from) {return false;};
	    virtual bool get_inverse_matrix (OSL::ShaderGlobals *sg, OSL::Matrix44 &result,
	                                     OSL::ustring to, float time) {return false;};

	    virtual bool get_array_attribute (OSL::ShaderGlobals *sg, bool derivatives,
	                                      OSL::ustring object, OSL::TypeDesc type, OSL::ustring name,
	                                      int index, void *val ) {return false;};
	    virtual bool get_attribute (OSL::ShaderGlobals *sg, bool derivatives, OSL::ustring object,
	                                OSL::TypeDesc type, OSL::ustring name, void *val) {return false;};
	    virtual bool get_userdata (bool derivatives, OSL::ustring name, OSL::TypeDesc type,
	                               OSL::ShaderGlobals *sg, void *val) {return false;};
	private:
};

static CNullRendererServices null_rs;
*/

CShadingEngineOsl::CShadingEngineOsl()
{
	m_osl_system_ptr = new OSL::ShadingSystem{ nullptr, NULL, &m_error_handler };

	NShadingEngineOsl::RegisterClosures(m_osl_system_ptr);

	m_osl_system_ptr->attribute("searchpath:shader", "data/shaders/");

	OSL::ShaderGroupRef default_shader_network = m_osl_system_ptr->ShaderGroupBegin("default");
	{
		OSL::Color3 Cs = { 0.5f, 0.5f, 0.5f };
		m_osl_system_ptr->Parameter("Cs", OSL::TypeDesc::TypeColor, &Cs);
		m_osl_system_ptr->Shader("surface", "matte", "layer0");
	}
	m_osl_system_ptr->ShaderGroupEnd();

	m_osl_shader_networks.Append(default_shader_network);
};

CShadingEngineOsl::~CShadingEngineOsl()
{
	m_osl_shader_networks.Clear();
	delete m_osl_system_ptr;
};

void CShadingEngineOsl::Eval(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, ISampler* sampler_ptr, std::vector<SSampleInfo>& sample_info_buffer)
{
	std::vector<std::vector<U32>> stream_array;
	stream_array.resize(m_osl_shader_networks.data.size());
	NShadingEngineOsl::FilterInputStream(input_stream, sg_buffer, stream_array);

	OSL::PerThreadInfo* thread_local_data = m_osl_system_ptr->create_thread_info();
	OSL::ShadingContext* shading_ctx_ptr = m_osl_system_ptr->get_context(thread_local_data);

	for (U32 shader_id = 0; shader_id < m_osl_shader_networks.data.size(); shader_id++)
	{
		for (auto sample_id : stream_array[shader_id])
		{
			const SShaderGlobals& sg = sg_buffer[sample_id];

			OSL::ShaderGlobals osl_sg;
			osl_sg.P = NShadingEngineOsl::ToOslType(sg.pos);
			osl_sg.I = NShadingEngineOsl::ToOslType(sg.wo);
			osl_sg.N = NShadingEngineOsl::ToOslType(sg.normal);
			osl_sg.u = sg.texcoord.x;
			osl_sg.v = sg.texcoord.y;

			m_osl_system_ptr->execute(shading_ctx_ptr, *m_osl_shader_networks.data[shader_id], osl_sg);

			SShadingResult shading_result;
			OSL::Color3 weight = { 1.0f, 1.0f, 1.0f };
			NShadingEngineOsl::ProcessClosureNetwork_R(shading_result, osl_sg.Ci, weight);

			SSampleInfo& sample_info = sample_info_buffer[sample_id];
			shading_result.bsdf_array.Eval(sg, sample_info);
			shading_result.bsdf_array.Clear();
		}
	}

	m_osl_system_ptr->release_context(shading_ctx_ptr);
	m_osl_system_ptr->destroy_thread_info(thread_local_data);
	stream_array.clear();
}

void CShadingEngineOsl::Sample(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, ISampler* sampler_ptr, std::vector<SSampleInfo>& sample_info_buffer)
{
	std::vector<std::vector<U32>> stream_array;
	stream_array.resize(m_osl_shader_networks.data.size());
	NShadingEngineOsl::FilterInputStream(input_stream, sg_buffer, stream_array);

	OSL::PerThreadInfo* thread_local_data = m_osl_system_ptr->create_thread_info();
	OSL::ShadingContext* shading_ctx_ptr = m_osl_system_ptr->get_context(thread_local_data);

	for (U32 shader_id = 0; shader_id < m_osl_shader_networks.data.size(); shader_id++)
	{
		for (auto sample_id : stream_array[shader_id])
		{
			const SShaderGlobals& sg = sg_buffer[sample_id];

			OSL::ShaderGlobals osl_sg;
			osl_sg.P = NShadingEngineOsl::ToOslType(sg.pos);
			osl_sg.I = NShadingEngineOsl::ToOslType(sg.wo);
			osl_sg.N = NShadingEngineOsl::ToOslType(sg.normal);
			osl_sg.u = sg.texcoord.x;
			osl_sg.v = sg.texcoord.y;

			m_osl_system_ptr->execute(shading_ctx_ptr, *m_osl_shader_networks.data[shader_id], osl_sg);

			SShadingResult shading_result;
			OSL::Color3 weight = { 1.0f, 1.0f, 1.0f };
			NShadingEngineOsl::ProcessClosureNetwork_R(shading_result, osl_sg.Ci, weight);

			SSampleInfo& sample_info = sample_info_buffer[sample_id];

			F32x2 uv = sampler_ptr->GetSubsampler(sample_id)->SampleF32x2();
			shading_result.bsdf_array.Sample(sg, uv, sample_info);
			shading_result.bsdf_array.Clear();

		}
	}

	m_osl_system_ptr->release_context(shading_ctx_ptr);
	m_osl_system_ptr->destroy_thread_info(thread_local_data);
	stream_array.clear();
}
