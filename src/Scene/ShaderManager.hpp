
#pragma once

#include <vector>

#include "Foundation/Common.hpp"
#include "Foundation/Container.hpp"
#include "Foundation/IShaderNode.hpp"
#include "Scene/ShaderNodeLambert.hpp"

struct SSceneContext;

class CShaderNetwork
{
	public:
		CShaderNetwork() { m_node_ptr = new CShaderNodeLambert; };
		~CShaderNetwork() { };

		void Eval(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, SRandomNumberGenerator* rng_ptr, std::vector<SSampleInfo>& sample_info_buffer);
		void Sample(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, SRandomNumberGenerator* rng_ptr, std::vector<SSampleInfo>& sample_info_buffer);

	private:
		IShaderNode* m_node_ptr;
};

struct SShaderContainer
{
	TTrackedArray<CShaderNetwork> shader_networks;
};

class CShaderManager
{
	public:
		using SStorage = SShaderContainer;

		CShaderManager() {};
		
		CShaderManager(SStorage* storage_ptr, SSceneContext* scene_ctx_ptr)
		{
			m_storage_ptr = storage_ptr;
			m_scene_ctx_ptr = scene_ctx_ptr;

			CShaderNetwork default_shader_network;
			m_storage_ptr->shader_networks.Append(default_shader_network);
		};

		~CShaderManager() {};


		void Eval(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, SRandomNumberGenerator* rng_ptr, std::vector<SSampleInfo>& sample_info_buffer);
		void Sample(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, SRandomNumberGenerator* rng_ptr, std::vector<SSampleInfo>& sample_info_buffer);

	protected:

	private:
		// void FilterInputStream(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, std::vector<std::vector<U32>>& stream_array);

		SSceneContext* m_scene_ctx_ptr;
		SStorage* m_storage_ptr;

};
