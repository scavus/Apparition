
#pragma once

#include "Foundation/Common.hpp"
#include "Scene/IBsdf.hpp"

class CBsdfDiffuse : public IBsdf
{
	public:
		struct SParams
		{
			F32x3 normal;
		};

		CBsdfDiffuse(const SParams& params) { m_params = params; };
		virtual ~CBsdfDiffuse() {};

		virtual F32 Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf) override;
		virtual F32 Eval(const SShaderGlobals& sg, const F32x3& wi) override;

	protected:

	private:
		SParams m_params;
};

class CBsdfReflection : public IBsdf
{
	public:
		struct SParams
		{
			F32x3 normal;
			F32 eta;
		};

		CBsdfReflection(const SParams& params) { m_params = params; };
		virtual ~CBsdfReflection() {};

		virtual F32 Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf) override;
		virtual F32 Eval(const SShaderGlobals& sg, const F32x3& wi) override;

	protected:

	private:
		SParams m_params;
};

class CBsdfRefraction : public IBsdf
{
	public:
		struct SParams
		{
			F32x3 normal;
			F32 eta;
		};

		CBsdfRefraction(const SParams& params) { m_params = params; };
		virtual ~CBsdfRefraction() {};

		virtual F32 Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf) override;
		virtual F32 Eval(const SShaderGlobals& sg, const F32x3& wi) override;

	protected:

	private:
		SParams m_params;
};

class CBsdfMicrofacetBeckmann : public IBsdf
{
	public:
		struct SParams
		{
			F32x3 normal;
			F32 alpha;
			F32 eta;
		};

		CBsdfMicrofacetBeckmann(const SParams& params) { m_params = params; };
		virtual ~CBsdfMicrofacetBeckmann() {};

		virtual F32 Sample(const SShaderGlobals& sg, const F32x2& uv, F32x3& wi, F32& pdf) override;
		virtual F32 Eval(const SShaderGlobals& sg, const F32x3& wi) override;

	protected:

	private:
		SParams m_params;
};
