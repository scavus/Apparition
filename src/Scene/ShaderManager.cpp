
#include "ShaderManager.hpp"

#include "Scene/SceneContext.hpp"

void CShaderNetwork::Eval(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, SRandomNumberGenerator* rng_ptr, std::vector<SSampleInfo>& sample_info_buffer)
{
	for (auto i : input_stream)
	{
		const SShaderGlobals& sg = sg_buffer[i];
		SSampleInfo& sample_info = sample_info_buffer[i];

		m_node_ptr->Eval(sg, rng_ptr, sample_info);
	}
}

void CShaderNetwork::Sample(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, SRandomNumberGenerator* rng_ptr, std::vector<SSampleInfo>& sample_info_buffer)
{
	for (auto i : input_stream)
	{
		const SShaderGlobals& sg = sg_buffer[i];
		SSampleInfo& sample_info = sample_info_buffer[i];

		m_node_ptr->Sample(sg, rng_ptr, sample_info);
	}
}

namespace NShaderManager
{
	void FilterInputStream(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, std::vector<std::vector<U32>>& stream_array)
	{
		for (auto i : input_stream)
		{
			U32 shader_id = sg_buffer[i].material_id;
			stream_array[shader_id].push_back(i);
		}
	}
}

void CShaderManager::Eval(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, SRandomNumberGenerator* rng_ptr, std::vector<SSampleInfo>& sample_info_buffer)
{
	std::vector<std::vector<U32>> stream_array;
	stream_array.resize(m_storage_ptr->shader_networks.data.size());

	NShaderManager::FilterInputStream(input_stream, sg_buffer, stream_array);

	for (U32 shader_id = 0; shader_id < m_storage_ptr->shader_networks.data.size(); shader_id++)
	{
		m_storage_ptr->shader_networks.data[shader_id].Eval(stream_array[shader_id], sg_buffer, rng_ptr, sample_info_buffer);
	}

	stream_array.clear();
}

void CShaderManager::Sample(const std::vector<U32>& input_stream, const std::vector<SShaderGlobals>& sg_buffer, SRandomNumberGenerator* rng_ptr, std::vector<SSampleInfo>& sample_info_buffer)
{
	std::vector<std::vector<U32>> stream_array;
	stream_array.resize(m_storage_ptr->shader_networks.data.size());

	NShaderManager::FilterInputStream(input_stream, sg_buffer, stream_array);

	for (U32 shader_id = 0; shader_id < m_storage_ptr->shader_networks.data.size(); shader_id++)
	{
		m_storage_ptr->shader_networks.data[shader_id].Sample(stream_array[shader_id], sg_buffer, rng_ptr, sample_info_buffer);
	}

	stream_array.clear();
}
