
#include "MeshManager.hpp"

#include "Scene/SceneContext.hpp"

namespace NMeshGeomManager
{
	void Transform(const std::vector<SMesh>& meshes, const std::vector<STriangle>& primitives, std::vector<STriangle>& transformed_primitives)
	{
		for (const STriangle& triangle : primitives)
		{
			STriangle pt_triangle;

			const SMatrix4x4& obj_to_world_mtx = meshes[triangle.mesh_id].obj_to_world_mtx;

			pt_triangle.vtx[0].pos = NMath::TransformPos(obj_to_world_mtx, triangle.vtx[0].pos);
			pt_triangle.vtx[1].pos = NMath::TransformPos(obj_to_world_mtx, triangle.vtx[1].pos);
			pt_triangle.vtx[2].pos = NMath::TransformPos(obj_to_world_mtx, triangle.vtx[2].pos);

			pt_triangle.vtx[0].normal = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[0].normal) );
			pt_triangle.vtx[1].normal = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[1].normal) );
			pt_triangle.vtx[2].normal = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[2].normal) );

			pt_triangle.vtx[0].tangent = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[0].tangent) );
			pt_triangle.vtx[1].tangent = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[1].tangent) );
			pt_triangle.vtx[2].tangent = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[2].tangent) );

			pt_triangle.vtx[0].binormal = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[0].binormal) );
			pt_triangle.vtx[1].binormal = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[1].binormal) );
			pt_triangle.vtx[2].binormal = NMath::Normalize( NMath::TransformDir(obj_to_world_mtx, triangle.vtx[2].binormal) );

			pt_triangle.vtx[0].texcoord = triangle.vtx[0].texcoord;
			pt_triangle.vtx[1].texcoord = triangle.vtx[1].texcoord;
			pt_triangle.vtx[2].texcoord = triangle.vtx[2].texcoord;

			pt_triangle.mesh_id = triangle.mesh_id;

			transformed_primitives.push_back(pt_triangle);
		}
	}
};

void CMeshGeomManager::Update()
{
	if (m_storage_ptr->transformed_mesh_geom_update_flag)
	{
		auto mesh_arr = _scene_ctx_ptr->mesh_manager_ptr->GetMeshArrayConstRef();
		NMeshGeomManager::Transform(mesh_arr.data, m_storage_ptr->mesh_geom, m_storage_ptr->transformed_geom);
		m_storage_ptr->transformed_mesh_geom_update_flag = false;
	}
}

void CMeshManager::Update()
{
	if (m_storage_ptr->mesh_arr.update_flag)
	{
		_scene_ctx_ptr->mesh_geom_manager_ptr->Update();
		m_storage_ptr->mesh_arr.update_flag = false;
	}
}

void CMeshManager::SetMesh(SMeshId resource_id, SMesh& resource)
{
	SMesh& mesh = m_storage_ptr->mesh_arr[resource_id];
	// _scene_ctx_ptr->deferred_shading_system_ptr->UnbindMaterial(&mesh.material_id);

	m_storage_ptr->mesh_arr[resource_id] = std::move(resource);
	// _scene_ctx_ptr->deferred_shading_system_ptr->BindMaterial(&mesh.material_id);

	m_storage_ptr->mesh_arr.update_flag = true;
}

/*
void CMeshManager::SetMeshMaterial(SMeshId mesh_id, SMaterialId mtl_id)
{
	m_storage_ptr->mesh_arr[mesh_id].material_id = mtl_id;
}
*/