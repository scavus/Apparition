# About
This is a toy project I've initially started developing by porting some of the code from my other GPU-based renderer [Wisard](https://gitlab.com/scavus/Wisard) to run on the CPU.

My main purpose was to explore and experiment with data-oriented, SIMD-friendly rendering by utilizing [ispc](https://ispc.github.io/), and a shading engine using [OpenShadingLanguage](https://github.com/imageworks/OpenShadingLanguage/) etc...
